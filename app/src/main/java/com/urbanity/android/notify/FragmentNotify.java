package com.urbanity.android.notify;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.urbanity.android.R;
import com.urbanity.android.bases.BaseFragment;

/**
 * Created by DRM on 20/05/17.
 */

public class FragmentNotify extends BaseFragment {

    ImageView imgNotify;
    NotifyType type;

    TextView txTitle, txSubTitle;

    public static FragmentNotify getInstance( NotifyType type){
        FragmentNotify fragment=new FragmentNotify();
        Bundle arg=new Bundle();
        arg.putSerializable("type",type);
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notify,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        imgNotify=(ImageView)findViewById(R.id.img_notify);
        txSubTitle=(TextView)findViewById(R.id.tx_sub_title);
        txTitle=(TextView)findViewById(R.id.tx_title);

        type= (NotifyType) getArguments().getSerializable("type");
        switch (type){
            case ERROR_NETWORK:
                txTitle.setText("No encontramos conexión a Internet.");
                txSubTitle.setText("Asegúrate de estar conectado a WiFi o tener datos para poder utilizar tu tarjeta.");
                imgNotify.setBackgroundResource(R.drawable.ic_network);
                break;

            case ERROR_NEWS:
                txTitle.setText("¡Oops! Hubo un Error.");
                txSubTitle.setText("No pudimos obtener las noticias en este momento, por favor intenta más tarde.");
                imgNotify.setBackgroundResource(R.drawable.ic_urban_black);
                break;

            default:

                break;
        }


    }

}
