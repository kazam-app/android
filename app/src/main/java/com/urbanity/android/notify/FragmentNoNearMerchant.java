package com.urbanity.android.notify;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseActivity;
import com.urbanity.android.bases.BaseFragment;

import static com.urbanity.android.analytics.EventsFB.SCREEN_MERCHANT_LIST_NO_NEARBY_STORES;

/**
 * Created by drm on 11/10/17.
 */

public class FragmentNoNearMerchant extends BaseFragment {


    public static FragmentNoNearMerchant getInstance(){
        return new FragmentNoNearMerchant();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_empty_near,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //TODO mandar a contants
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_MERCHANT_LIST_NO_NEARBY_STORES);
    }
}
