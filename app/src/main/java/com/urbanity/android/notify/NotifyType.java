package com.urbanity.android.notify;

/**
 * Created by DRM on 20/05/17.
 */

public enum NotifyType {

    ERROR_NEWS,
    ERROR_NETWORK,


    ERROR_LOCATION,
    ERROR_EMPTY_REWARD,


    ERROR_GENERIC
}
