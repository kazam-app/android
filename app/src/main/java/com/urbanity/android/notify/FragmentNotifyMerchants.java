package com.urbanity.android.notify;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.utils.UrbanDevice;

/**
 * Created by DRM on 20/05/17.
 */

public class FragmentNotifyMerchants extends BaseFragment {

    ImageView imgNotifyMerchants;
    NotifyType type;

    TextView txTitleMerchants, txMessageMerchants;

    private static final int READ_LOCATION_PERMISSIONS_REQUEST = 1;
    Button bt_active_location;


    public static FragmentNotifyMerchants getInstance(NotifyType type){
        FragmentNotifyMerchants fragment=new FragmentNotifyMerchants();
        Bundle arg=new Bundle();
        arg.putSerializable("type",type);
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notify_merchants,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        imgNotifyMerchants=(ImageView)findViewById(R.id.img_notify_merchants);
        txTitleMerchants=(TextView)findViewById(R.id.tx_title_merchants);
        txMessageMerchants=(TextView)findViewById(R.id.tx_message_merchants);
        bt_active_location =(Button)findViewById(R.id.bt_active_location);

        type= (NotifyType) getArguments().getSerializable("type");
        switch (type){
            case ERROR_LOCATION:
//TODO mandar a contants
                FacebookAnalytics.FireBaseScreenTracking(getActivity(),"Merchant List - Near Me [Location Ask]");
                imgNotifyMerchants.setImageResource(R.drawable.ic_merchant_location);
            //    imgNotifyMerchants.setBackgroundResource(R.drawable.ic_merchant_location);
                txTitleMerchants.setText("Tiendas cerca de ti");
                txMessageMerchants.setText("Activa tu ubicación para poder encontrar tiendas cerca de ti");
                bt_active_location.setBackgroundResource(R.drawable.selector_button);
                bt_active_location.setTextColor(getResources().getColor(android.R.color.white));
                bt_active_location.setVisibility(View.VISIBLE);
                bt_active_location.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(checkPermissions()){
                            validateLocationEnabled();
                        }

                    }

                });

                break;

            case ERROR_EMPTY_REWARD:

                imgNotifyMerchants.setImageResource(R.drawable.ic_reward_empty);
                txTitleMerchants.setText("Tus Premios");
                txMessageMerchants.setText("Cuando completes una tarjeta, tus premios aparecerán aquí.");

                break;

            default:

                break;
        }


    }

    public void validateLocationEnabled(){
        if(UrbanDevice.isLocationEnabled(getActivity())){

            if(mListener!=null){
                mListener.onChangeListener();
            }
        }else{
            UrbanDevice.ActiveLocation(getActivity());
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == READ_LOCATION_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if(mListener!=null){
                    mListener.onChangeListener();
                }
            } else {
                // showRationale = false if user clicks Never Ask Again, otherwise true
                boolean showRationale = shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION);

                if (showRationale) {
                    // do something here to handle degraded mode
                } else {
                    Toast.makeText(getActivity(), "Permiso de Ubicación denegado", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public boolean checkPermissions() {
        boolean resultP=true;
        // 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid
        // checking the build version since Context.checkSelfPermission(...) is only available
        // in Marshmallow
        // 2) Always check for permission (even if permission has already been granted)
        // since the user can revoke permissions at any time through Settings
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            resultP= false;
            // The permission is NOT already granted.
            // Check if the user has been asked about this permission already and denied
            // it. If so, we want to give more explanation about why the permission is needed.
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show our own UI to explain to the user why we need to read the contacts
                // before actually requesting the permission and showing the default UI
            }

            // Fire off an async request to actually get the permission
            // This will show the standard permission request dialog UI
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, READ_LOCATION_PERMISSIONS_REQUEST);
        }
        return resultP;
    }


    public static OnChangeListener mListener;
    public static void setListener(OnChangeListener listener){
        mListener=listener;
    }
    public interface OnChangeListener{
        void onChangeListener();
    }
}
