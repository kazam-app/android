package com.urbanity.android.notify;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;

import static com.urbanity.android.analytics.EventsFB.SCREEN_MERCHANT_PROFILE_NO_CARD;

/**
 * Created by DRM on 02/08/17.
 */

public class FragmentEmptyPunchCard extends BaseFragment {

    TextView txError;

    public static String NAMEMERCHANT="NAMEMERCHANT";
    public static FragmentEmptyPunchCard getInstance(String merchantName){
        FragmentEmptyPunchCard fragment=new FragmentEmptyPunchCard();
        Bundle arg=new Bundle();
        arg.putString(NAMEMERCHANT,merchantName);
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_empty_card,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //TODO mandar a contants
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_MERCHANT_PROFILE_NO_CARD);
        txError=(TextView)findViewById(R.id.tx_error_desc);
        txError.setText(String.valueOf("¡Oops! "+ getArguments().getString(NAMEMERCHANT) +" aún no ha creado su tarjeta de sellos. Cuando lo haga aparecerá aquí."));
    }
}
