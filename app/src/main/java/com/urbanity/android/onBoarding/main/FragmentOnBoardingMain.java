package com.urbanity.android.onBoarding.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.onBoarding.OnBoardingType;

import static com.urbanity.android.analytics.EventsFB.EVENT_ENTER_STORE;
import static com.urbanity.android.analytics.EventsFB.EVENT_GET_PRIZES;
import static com.urbanity.android.analytics.EventsFB.EVENT_HOW_WORK;
import static com.urbanity.android.analytics.EventsFB.EVENT_IO_GET_PRIZE;
import static com.urbanity.android.analytics.EventsFB.EVENT_IO_VALIDATE;
import static com.urbanity.android.analytics.EventsFB.EVENT_PUNCH_CARD;
import static com.urbanity.android.analytics.EventsFB.EVENT_SCREEN_ENTER_STORE;
import static com.urbanity.android.analytics.EventsFB.EVENT_SCREEN_HOW_WORK;
import static com.urbanity.android.analytics.EventsFB.EVENT_SCREEN_IO_GET_PRIZE;
import static com.urbanity.android.analytics.EventsFB.EVENT_SCREEN_IO_VALIDATE;
import static com.urbanity.android.analytics.EventsFB.EVENT_SCREEN_PRIZE;
import static com.urbanity.android.analytics.EventsFB.EVENT_SCREEN_STAMP_CARD;
import static com.urbanity.android.analytics.EventsFB.EVENT_SCREEN_WELCOME;
import static com.urbanity.android.analytics.EventsFB.EVENT_SCREEN_YOUR_CARDS;
import static com.urbanity.android.analytics.EventsFB.EVENT_STAMP_CARD;
import static com.urbanity.android.analytics.EventsFB.EVENT_WELCOME;

/**
 * Created by DRM on 27/05/17.
 */

public class FragmentOnBoardingMain extends BaseFragment {

    TextView txOnBoardingTitle, txOnBoardingSubTitle1, txOnBoardingSubTitle2;
    Button btOnBoarding;
    ImageView imgOnBoarding , tx_skip;
    ImageView imgBackground;
    OnBoardingType type;


    private final  static  String POSITION="POSITION";
    public static FragmentOnBoardingMain getInstance(OnBoardingType type){
        FragmentOnBoardingMain fragment=new FragmentOnBoardingMain();
        Bundle arg=new Bundle();
        arg.putSerializable(POSITION,type);
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_onboarding_main,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        txOnBoardingTitle=(TextView)findViewById(R.id.tx_on_boarding_title);
        txOnBoardingSubTitle1=(TextView)findViewById(R.id.tx_on_boarding_subtitle1);
        txOnBoardingSubTitle2=(TextView)findViewById(R.id.tx_on_boarding_subtitle2);
        btOnBoarding=(Button)findViewById(R.id.bt_on_boarding);
        tx_skip=(ImageView) findViewById(R.id.tx_skip);
        imgOnBoarding=(ImageView)findViewById(R.id.img_main_on_boarding);
        imgBackground=(ImageView)findViewById(R.id.img_back_on_boarding);
        type=(OnBoardingType) getArguments().getSerializable(POSITION);
        tx_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            }
        });



        switch (type){
            case OBMAIN1:
                FacebookAnalytics.FireBaseScreenTracking(getActivity(),EVENT_SCREEN_WELCOME);
                FacebookAnalytics.FBSimpleEvent(getActivity(),EVENT_WELCOME);

                txOnBoardingTitle.setText(String.valueOf(getResources().getString(R.string.tittleOnBoarding1)));
                txOnBoardingSubTitle1.setText(String.valueOf(getResources().getString(R.string.msg1OnBoarding1)));
                txOnBoardingSubTitle2.setText(String.valueOf(getResources().getString(R.string.msg2OnBoarding1)));
                btOnBoarding.setText(String.valueOf(getResources().getString(R.string.btOnBoarding1)));
                imgOnBoarding.setBackgroundResource(R.drawable.logo_kazam_full);

                btOnBoarding.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mPageListener!=null){
                            mPageListener.changeNextPage();
                        }
                    }
                });

                break;

            case OBMAIN2:
                FacebookAnalytics.FireBaseScreenTracking(getActivity(),EVENT_SCREEN_ENTER_STORE);
                FacebookAnalytics.FBSimpleEvent(getActivity(),EVENT_ENTER_STORE);


                txOnBoardingTitle.setText(String.valueOf(getResources().getString(R.string.tittleOnBoarding2)));
                txOnBoardingSubTitle1.setText(String.valueOf(getResources().getString(R.string.msg1OnBoarding2)));
                txOnBoardingSubTitle2.setText(String.valueOf(getResources().getString(R.string.msg2OnBoarding2)));
                btOnBoarding.setText(String.valueOf(getResources().getString(R.string.btOnBoarding2)));
                imgOnBoarding.setBackgroundResource(R.drawable.main_onboarding2);

                btOnBoarding.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mPageListener!=null){
                            mPageListener.changeNextPage();
                        }
                    }
                });


                break;

            case OBMAIN3:
                FacebookAnalytics.FireBaseScreenTracking(getActivity(),EVENT_SCREEN_YOUR_CARDS);
                FacebookAnalytics.FBSimpleEvent(getActivity(),EVENT_PUNCH_CARD);



                txOnBoardingTitle.setText(String.valueOf(getResources().getString(R.string.tittleOnBoarding3)));
                txOnBoardingSubTitle1.setText(String.valueOf(getResources().getString(R.string.msg1OnBoarding3)));
                txOnBoardingSubTitle2.setText(String.valueOf(getResources().getString(R.string.msg2OnBoarding3)));
                btOnBoarding.setText(String.valueOf(getResources().getString(R.string.btOnBoarding3)));

                imgOnBoarding.setBackgroundResource(R.drawable.main_onboarding3);

               // imgBackground.setBackgroundResource(R.drawable.splash_background);

                btOnBoarding.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mPageListener!=null){
                            mPageListener.changeNextPage();
                        }
                    }
                });




                break;



            case  OBMAIN4:
                FacebookAnalytics.FireBaseScreenTracking(getActivity(),EVENT_SCREEN_PRIZE);
                FacebookAnalytics.FBSimpleEvent(getActivity(),EVENT_GET_PRIZES);




                txOnBoardingTitle.setText(String.valueOf(getResources().getString(R.string.tittleOnBoarding4)));
                txOnBoardingSubTitle1.setText(String.valueOf(getResources().getString(R.string.msg1OnBoarding4)));
                txOnBoardingSubTitle2.setText(String.valueOf(getResources().getString(R.string.msg2OnBoarding4)));
                btOnBoarding.setText(String.valueOf(getResources().getString(R.string.btOnBoarding4)));

                imgOnBoarding.setBackgroundResource(R.drawable.main_onboarding4);




                btOnBoarding.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mPageListener!=null){
                            startActivity(new Intent(getActivity(), MainActivity.class));
                            getActivity().finish();
                        }
                    }
                });

                break;


            //**************Description- App


            case OBDESC1:

                FacebookAnalytics.FireBaseScreenTracking(getActivity(),EVENT_SCREEN_HOW_WORK);
                FacebookAnalytics.FBSimpleEvent(getActivity(),EVENT_HOW_WORK);




                txOnBoardingTitle.setText(String.valueOf(getResources().getString(R.string.tittleOnBoardingDesc1)));
                txOnBoardingSubTitle1.setText(String.valueOf(getResources().getString(R.string.msg1OnBoardingDesc1)));
                txOnBoardingSubTitle2.setText(String.valueOf(getResources().getString(R.string.msg2OnBoardingDesc1)));
                btOnBoarding.setText(String.valueOf(getResources().getString(R.string.buttonNext)));
                imgOnBoarding.setBackgroundResource(R.drawable.onboardingeneral);
                imgBackground.setBackgroundResource(R.drawable.onboarding_back);

                btOnBoarding.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mPageListener!=null){
                            mPageListener.changeNextPage();
                        }
                    }
                });

                break;

            case OBDESC2:

                FacebookAnalytics.FireBaseScreenTracking(getActivity(),EVENT_SCREEN_STAMP_CARD);
                FacebookAnalytics.FBSimpleEvent(getActivity(),EVENT_STAMP_CARD);





                txOnBoardingTitle.setText(String.valueOf(getResources().getString(R.string.tittleOnBoardingDesc2)));
                txOnBoardingSubTitle1.setText(String.valueOf(getResources().getString(R.string.msg1OnBoardingDesc2)));
                txOnBoardingSubTitle2.setText(String.valueOf(getResources().getString(R.string.msg2OnBoardingDesc2)));
                btOnBoarding.setText(String.valueOf(getResources().getString(R.string.buttonNext)));


                imgOnBoarding.setBackgroundResource(R.drawable.onboarding_desc2);
                imgBackground.setBackgroundResource(R.drawable.onboarding_back);

                btOnBoarding.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mPageListener!=null){
                            mPageListener.changeNextPage();
                        }
                    }
                });

                break;

            case OBDESC3:

                FacebookAnalytics.FireBaseScreenTracking(getActivity(),EVENT_SCREEN_IO_VALIDATE);
                FacebookAnalytics.FBSimpleEvent(getActivity(),EVENT_IO_VALIDATE);





                txOnBoardingTitle.setText(String.valueOf(getResources().getString(R.string.tittleOnBoardingDesc3)));
                txOnBoardingSubTitle1.setText(String.valueOf(getResources().getString(R.string.msg1OnBoardingDesc3)));
                txOnBoardingSubTitle2.setText(String.valueOf(getResources().getString(R.string.msg2OnBoardingDesc3)));
                btOnBoarding.setText(String.valueOf(getResources().getString(R.string.buttonNext)));


                imgOnBoarding.setBackgroundResource(R.drawable.onboarding_desc3);
                imgBackground.setBackgroundResource(R.drawable.onboarding_back);


                btOnBoarding.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mPageListener!=null){
                            mPageListener.changeNextPage();
                        }
                    }
                });


                break;

            case OBDESC4:
                FacebookAnalytics.FireBaseScreenTracking(getActivity(),EVENT_SCREEN_IO_GET_PRIZE);
                FacebookAnalytics.FBSimpleEvent(getActivity(),EVENT_IO_GET_PRIZE);



                txOnBoardingTitle.setText(String.valueOf(getResources().getString(R.string.tittleOnBoardingDesc4)));
                txOnBoardingSubTitle1.setText(String.valueOf(getResources().getString(R.string.msg1OnBoardingDesc4)));
                txOnBoardingSubTitle2.setText(String.valueOf(getResources().getString(R.string.msg2OnBoardingDesc4)));
                btOnBoarding.setText(String.valueOf(getResources().getString(R.string.bt_finish_desc)));


                btOnBoarding.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getActivity(),MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        getActivity().finish();
                        }
                });

                imgOnBoarding.setBackgroundResource(R.drawable.onboarding_desc4);
                imgBackground.setBackgroundResource(R.drawable.onboarding_back);

                break;

        }
    }




    public static void setPageListener(OnNextPageListener pageListener){
        mPageListener=pageListener;
    }
    public static OnNextPageListener mPageListener;
    public interface OnNextPageListener{
        void changeNextPage();
    }

}
