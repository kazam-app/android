package com.urbanity.android.onBoarding;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.urbanity.android.R;
import com.urbanity.android.customsViews.cristalViewPager.CrystalViewPager;
import com.urbanity.android.onBoarding.main.FragmentOnBoardingMain;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by DRM on 20/05/17.
 */

public class OnBoardingActivity extends AppCompatActivity implements FragmentOnBoardingMain.OnNextPageListener {


    public static final String BOARDINGTYPE="BOARDINGTYPE";


    CrystalViewPager viewPager;
    FragmentAdapter adapter;


    CirclePageIndicator indicator;
   // TextView tx_skeep;
   RelativeLayout content_on_boarding_activity;

  //  Button bt_start;
    OnBoardingType type;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        Bundle arg=getIntent().getExtras();
        if(arg!=null){
            type= (OnBoardingType) arg.getSerializable(BOARDINGTYPE);
        }
        viewPager= (CrystalViewPager) findViewById(R.id.viewpager);
        adapter= new FragmentAdapter(getSupportFragmentManager());
     //   tx_skeep=(TextView)findViewById(R.id.tx_skeep);
     //   bt_start=(Button)findViewById(R.id.bt_start);
        indicator=(CirclePageIndicator)findViewById(R.id.indicator);

        content_on_boarding_activity=(RelativeLayout)findViewById(R.id.content_on_boarding_activity);
        content_on_boarding_activity.setBackgroundResource(R.drawable.splash_background);



        switch (type){

            case MAIN:
                adapter.addFragment(FragmentOnBoardingMain.getInstance(OnBoardingType.OBMAIN1));
                adapter.addFragment(FragmentOnBoardingMain.getInstance(OnBoardingType.OBMAIN2));
                adapter.addFragment(FragmentOnBoardingMain.getInstance(OnBoardingType.OBMAIN3));
                adapter.addFragment(FragmentOnBoardingMain.getInstance(OnBoardingType.OBMAIN4));
                break;

            case APPDESC:

                adapter.addFragment(FragmentOnBoardingMain.getInstance(OnBoardingType.OBDESC1));
                adapter.addFragment(FragmentOnBoardingMain.getInstance(OnBoardingType.OBDESC2));
                adapter.addFragment(FragmentOnBoardingMain.getInstance(OnBoardingType.OBDESC3));
                adapter.addFragment(FragmentOnBoardingMain.getInstance(OnBoardingType.OBDESC4));


                break;
        }

        FragmentOnBoardingMain.setPageListener(this);

        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        adapter.notifyDataSetChanged();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageSelected(int position) {
                Log.e("Position",String.valueOf(position));
            }
            @Override
            public void onPageScrollStateChanged(int state) {} });


    }


    @Override
    public void changeNextPage() {
        viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
    }
}
