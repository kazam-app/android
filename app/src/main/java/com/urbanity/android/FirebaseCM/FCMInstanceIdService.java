package com.urbanity.android.FirebaseCM;

import android.util.Log;

import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by DRM on 11/11/16.
 */

public class FCMInstanceIdService extends FirebaseInstanceIdService  {
    private static final String TAG = "FirebaseIDService";

    @Override
    public void onTokenRefresh() {

        Log.e(TAG, "Refreshed token: " + FirebaseInstanceId.getInstance().getToken());

        // TODO: Implement this method to send any registration to your app's servers.
        try {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            AppEventsLogger.setPushNotificationsRegistrationId(refreshedToken);
        } catch (Exception e) {
            Log.e("test", "Failed to complete token refresh", e);
        }
    }



}
