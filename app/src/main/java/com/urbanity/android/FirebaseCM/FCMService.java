package com.urbanity.android.FirebaseCM;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.facebook.notifications.NotificationsManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.urbanity.android.R;
import com.urbanity.android.main.MainActivity;

import java.util.Map;


public class FCMService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.

        Bundle data = new Bundle();
        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
            data.putString(entry.getKey(), entry.getValue());
            Log.e("FB_BUNDLE ", entry.getKey()  + " - "+ entry.getValue()  );
        }
        if(data.getString("fb_push_payload") != null){

            if(data.getString("body") !=null){
                sendNotification(data.getString("body"));
                return;
            }


            NotificationsManager.presentNotification(this,data,new Intent(getApplicationContext(), MainActivity.class) );


            return;


        }

        if (remoteMessage != null) {
            String message = "";
            try {
                message = remoteMessage.getNotification().getBody();
                if (message != null) {
                    sendNotification(message);
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sendNotification(String message) {

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 1001,  resultIntent,  PendingIntent.FLAG_UPDATE_CURRENT );



        NotificationCompat.Builder notificationBuilde = new NotificationCompat.Builder(this)
                .setSmallIcon(getNotificationIcon())
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message));

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationBuilde.setContentIntent(resultPendingIntent);
            notificationManager.notify(0 /* ID of notification */, notificationBuilde.build());



    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_stat_onesignal_default : R.drawable.ic_stat_onesignal_default;
    }
}
