package com.urbanity.android.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;

/**
 * Created by drm on 28/09/17.
 */

public class OnClearFromRecentService  extends Service {

        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            Log.e("ClearFromRecentService", "Service Started");
            return START_NOT_STICKY;
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            Log.e("ClearFromRecentService", "Service Destroyed");
        }

        @Override
        public void onTaskRemoved(Intent rootIntent) {
            Log.e("ClearFromRecentService", "END");
            FacebookAnalytics.FBSimpleEvent(this, EventsFB.EVENT_APP_CLOSE);
            stopSelf();
        }
}
