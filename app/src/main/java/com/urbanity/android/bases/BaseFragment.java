package com.urbanity.android.bases;

/**
 * Created by lenovo on 16/05/2017.
 */


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.urbanity.android.R;

/**
 * Created by DRM on 30/09/16.
 */

public class BaseFragment extends Fragment {

    public View findViewById(int resource){
        try{return getView().findViewById(resource);}catch (Exception e){return null;}
    }

    public void addFragmentToBackStack(Fragment fragment, String tag, int container){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down);//, R.anim.pop_enter, R.anim.pop_exit);
        transaction.addToBackStack(tag);
        transaction.replace(container, fragment, tag).commit();
    }

    public void addFragmentToBackStackNoAnimation(Fragment fragment, String tag, int container){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
      //  transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down);//, R.anim.pop_enter, R.anim.pop_exit);
        transaction.addToBackStack(tag);
        transaction.replace(container, fragment, tag).commit();
    }


    public void addFragmentToBackStackContact(Fragment fragment, String tag, int container){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.contact_in_up, R.anim.contact_out_up,R.anim.contact_in_down, R.anim.contact_out_down);
        transaction.addToBackStack(tag);
        transaction.replace(container, fragment, tag).commit();
    }



    public void replaceFragment(Fragment fragment, int container){
        if(fragment!=null ){
           try{
               FragmentTransaction transaction = getFragmentManager().beginTransaction();
               transaction.replace(container, fragment).commit();
           }catch (Exception e){}
        }

    }


    public void removeFragment(Fragment fragment){
        FragmentManager manager = getActivity().getSupportFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove(fragment);
        trans.commit();
        manager.popBackStack();
    }







    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }



}

