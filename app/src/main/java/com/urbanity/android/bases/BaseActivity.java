package com.urbanity.android.bases;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.urbanity.android.R;
import com.urbanity.android.legal.ActivityLegalFAQ;
import com.urbanity.android.redeem.ActivityRedeemMerchant;
import com.urbanity.android.reward.RewardActivity;
import com.urbanity.android.rulesReward.ActivityRulesReward;
import com.urbanity.android.trademarks.TrademarkActivity;
import com.urbanity.android.trademarks.perfil.TradeMarkPerfilActivity;
import com.urbanity.android.user.PerfilUserActivity;
import com.urbanity.android.user.notifications.ActivityNotifications;
import com.urbanity.android.user.update.ActivityChangeMail;
import com.urbanity.android.user.update.ActivityUpdatePassword;
import com.urbanity.android.user.update.ActivityUpdateUser;

/**
 * Created by DRM on 06/01/17.
 */

public class BaseActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolbarTitle;
    String title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    protected void onStart() {
        super.onStart();
        toolbar = (Toolbar)findViewById(R.id.toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null){

            if(this instanceof PerfilUserActivity
                    || this instanceof TradeMarkPerfilActivity
                    || this instanceof ActivityRedeemMerchant
                    || this instanceof ActivityNotifications
                    || this instanceof ActivityUpdatePassword
                    || this instanceof ActivityChangeMail
                    || this instanceof ActivityLegalFAQ
                    || this instanceof ActivityRulesReward
                    || this instanceof ActivityUpdateUser
                    ){

                getSupportActionBar().setDisplayShowCustomEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
            }else {
                getSupportActionBar().setDisplayShowCustomEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setHomeButtonEnabled(false);
            }



        }

        if (toolbar!=null){
            for (int i = 0; i < toolbar.getChildCount(); ++i) {
                View child = toolbar.getChildAt(i);

                // assuming that the title is the first instance of TextView
                // you can also check if the title string matches
                if (child instanceof TextView) {
                    toolbarTitle = (TextView)child;
                    if(title!=null && toolbar!=null){
                        toolbarTitle.setText(title);
                    }
                    break;
                }
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
             //   if (!(BaseActivity.this instanceof MainActivity))
                    onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setTitleToolbar(String title){
        this.title=title;

    }

    public Toolbar getToolbar(){
        return toolbar;
    }
}
