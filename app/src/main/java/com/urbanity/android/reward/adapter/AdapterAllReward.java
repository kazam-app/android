package com.urbanity.android.reward.adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.urbanity.android.R;
import com.urbanity.android.customsViews.circleImage.CircleImageView;
import com.urbanity.android.model.PunchCard;
import com.urbanity.android.reward.RewardType;
import com.urbanity.android.trademarks.adapter.AdapterPunchCard;

import java.util.List;

/**
 * Created by DRM on 21/07/17.
 */

public class AdapterAllReward extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private final int PUNCHCARDS = 0;
    List<PunchCard> pCards;
    private int itemSelected = -1;

    private int lastPosition = -1;
    RewardType type;


    public AdapterAllReward(Context context, List<PunchCard> pCards, RewardType type, AllPunchCardListener listener) {
        this.context = context;
        this.pCards = pCards;
        this.type=type;
        this.listener=listener;
    }


    private AllPunchCardListener listener;
    public interface AllPunchCardListener {
        void onItemSelected(PunchCard card, int position);
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View headerView;
        switch (viewType) {

            case PUNCHCARDS:
                headerView = inflater.inflate(R.layout.row_reward, viewGroup, false);
                viewHolder = new ViewHolderItemMenu(headerView);
                break;

        }

        return viewHolder;
    }


    private class ViewHolderItemMenu extends RecyclerView.ViewHolder {
        CircleImageView imgRowReward;
        TextView txRowNameMerchant, txRowMerchantPrize;
        Button btRowButton;
        ImageView imgRowArrow;
        RecyclerView rvRowReward;

        private ViewHolderItemMenu(View itemView) {
            super(itemView);
            imgRowReward=(CircleImageView)itemView.findViewById(R.id.img_row_reward);
            txRowNameMerchant=(TextView)itemView.findViewById(R.id.tx_row_name_merchant);
            txRowMerchantPrize=(TextView)itemView.findViewById(R.id.tx_row_merchant_prize);
            btRowButton=(Button)itemView.findViewById(R.id.bt_row_button);
            imgRowArrow=(ImageView)itemView.findViewById(R.id.img_row_arrow);
            rvRowReward=(RecyclerView)itemView.findViewById(R.id.rv_row_reward);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolderItemMenu aforeHolder;
        //  setAnimation(holder.itemView, position);
        switch (holder.getItemViewType()) {

            case PUNCHCARDS:
                aforeHolder = (ViewHolderItemMenu) holder;
                configureViewHolderItemMenu(aforeHolder, position);
                break;
        }


    }


    private void configureViewHolderItemMenu(final ViewHolderItemMenu holder, final int position) {
        PunchCard card = pCards.get(position);
        Picasso.with(context).load(card.getMerchant_image_url()).placeholder(context.getResources().getDrawable(R.drawable.ic_mark)).error(context.getResources().getDrawable(R.drawable.ic_mark)).into(holder.imgRowReward);

        holder.txRowNameMerchant.setText(String.valueOf(card.getMerchant_name()));
        holder.txRowMerchantPrize.setText(String.valueOf("Premio: "+card.getPrize()));
        holder.rvRowReward.setLayoutManager(new GridLayoutManager(context,5));

        AdapterPunchCard adapterPunchCard=new AdapterPunchCard(context,card.getPunch_count(),card.getPunch_limit(),0,true);


        holder.rvRowReward.setAdapter(adapterPunchCard);
        adapterPunchCard.notifyDataSetChanged();

        switch (type){
            case REWARD:
                holder.btRowButton.setVisibility(View.VISIBLE);
                holder.imgRowArrow.setVisibility(View.GONE);
            break;

            case ALLREWARD:
                holder.btRowButton.setVisibility(View.GONE);
                holder.imgRowArrow.setVisibility(View.VISIBLE);
                break;
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemSelected(pCards.get(position), position);
                }
            }
        });


        adapterPunchCard.setListener(new AdapterPunchCard.OnItemListener() {
            @Override
            public void onItemSelected() {
                if (listener != null) {
                    listener.onItemSelected(pCards.get(position), position);
                }
            }
        });


        holder.txRowMerchantPrize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemSelected(pCards.get(position), position);
                }
            }
        });

        holder.txRowNameMerchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemSelected(pCards.get(position), position);
                }
            }
        });


        holder.rvRowReward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemSelected(pCards.get(position), position);
                }
            }
        });







        holder.imgRowReward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemSelected(pCards.get(position), position);
                }
            }
        });



        holder.btRowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemSelected(pCards.get(position), position);
                }
            }
        });

        holder.imgRowArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onItemSelected(pCards.get(position), position);
                }
            }
        });
    }


    @Override
    public int getItemViewType(int position) {

        if (pCards.get(position) != null) {
            return PUNCHCARDS;
        }
        return position;
    }

    @Override
    public int getItemCount() {
        return pCards.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}