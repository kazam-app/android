package com.urbanity.android.reward.presenter;

import android.content.Context;

import com.urbanity.android.model.PunchCard;
import com.urbanity.android.reward.OnRewardFinishListener;
import com.urbanity.android.reward.RewardView;
import com.urbanity.android.reward.interactor.RewardInteractor;
import com.urbanity.android.reward.interactor.RewardInteractorImp;
import com.urbanity.android.utils.UrbanDevice;

import java.util.ArrayList;

/**
 * Created by DRM on 21/07/17.
 */

public class RewardPresenterImp implements RewardPresenter, OnRewardFinishListener {

    RewardView rewardView;
    RewardInteractor interactor;
    public RewardPresenterImp(RewardView rewardView){
        this.rewardView=rewardView;
        this.interactor=new RewardInteractorImp();
    }

    @Override
    public void validateGetAllReward(Context context) {
        if(UrbanDevice.isConnected(context)){
            rewardView.ShowProgress();
            interactor.getAllRewards(context,this);
        }
    }

    @Override
    public void validateReadyReward(Context context) {
        if(UrbanDevice.isConnected(context)){
            rewardView.ShowProgress();
            interactor.getReadyReward(context,this);
        }
    }

    @Override
    public void onError(String error) {
        rewardView.DismissProgress();
        rewardView.OnError(error);
    }

    @Override
    public void onSuccess(ArrayList<PunchCard> pCards) {
        rewardView.DismissProgress();
        rewardView.OnSuccess(pCards);
    }

    @Override
    public void onSuccessReady(ArrayList<PunchCard> pCards) {
        rewardView.DismissProgress();
        rewardView.OnSuccessReady(pCards);
    }

    @Override
    public void onSessionExpired() {
        rewardView.DismissProgress();
        rewardView.onSessionExpired();

    }
}
