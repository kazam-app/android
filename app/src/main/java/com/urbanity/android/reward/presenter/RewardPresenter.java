package com.urbanity.android.reward.presenter;

import android.content.Context;

/**
 * Created by DRM on 21/07/17.
 */

public interface RewardPresenter {
    void validateGetAllReward(Context context);
    void validateReadyReward(Context context);
}
