package com.urbanity.android.reward;

import com.urbanity.android.model.PunchCard;

import java.util.ArrayList;

/**
 * Created by DRM on 21/07/17.
 */

public interface OnRewardFinishListener {
    void onError(String error);
    void onSuccess(ArrayList<PunchCard> pCards);
    void onSuccessReady(ArrayList<PunchCard> pCards);

    void onSessionExpired();
}
