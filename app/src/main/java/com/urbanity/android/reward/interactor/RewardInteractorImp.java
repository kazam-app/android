package com.urbanity.android.reward.interactor;

import android.content.Context;

import com.urbanity.android.model.PunchCard;
import com.urbanity.android.model.UrbanResponseError;
import com.urbanity.android.network.ErrorUtils;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.reward.OnRewardFinishListener;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.utils.UrbanConstants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DRM on 21/07/17.
 */

public class RewardInteractorImp implements RewardInteractor {
    @Override
    public void getAllRewards(Context context, final OnRewardFinishListener listener) {
        Call<ArrayList<PunchCard>>getAllRewards= RetrofitService.getUrbanService().getAllPunchCard(UrbanDB.getInstance(context).getUrbanUser().getUrbanToken());
        getAllRewards.enqueue(new Callback<ArrayList<PunchCard>>() {
            @Override
            public void onResponse(Call<ArrayList<PunchCard>> call, Response<ArrayList<PunchCard>> response) {
                if(response.code()== UrbanConstants.ERRORLOGOUT){
                    listener.onSessionExpired();
                    return;
                }


                if(response.body()!=null){
                    listener.onSuccess(response.body());
                }else {
                    UrbanResponseError error= ErrorUtils.parseError(response);
                    listener.onError(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PunchCard>> call, Throwable t) {
                listener.onError("Ocurrio un error");
            }
        });
    }

    @Override
    public void getReadyReward(Context context, final OnRewardFinishListener listener) {
        Call<ArrayList<PunchCard>>getAllRewards= RetrofitService.getUrbanService().getPunchCardReady(UrbanDB.getInstance(context).getUrbanUser().getUrbanToken());
        getAllRewards.enqueue(new Callback<ArrayList<PunchCard>>() {
            @Override
            public void onResponse(Call<ArrayList<PunchCard>> call, Response<ArrayList<PunchCard>> response) {

                if(response.code()== UrbanConstants.ERRORLOGOUT){
                    listener.onSessionExpired();
                    return;
                }

                if(response.body()!=null){
                    listener.onSuccessReady(response.body());
                }else {
                    UrbanResponseError error= ErrorUtils.parseError(response);
                    listener.onError(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PunchCard>> call, Throwable t) {
                listener.onError("Ocurrio un error");
            }
        });
    }
}
