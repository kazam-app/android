package com.urbanity.android.reward;

import com.urbanity.android.model.PunchCard;

import java.util.ArrayList;

/**
 * Created by DRM on 21/07/17.
 */

public interface RewardView {
    void ShowProgress();
    void DismissProgress();

    void OnError(String error);
    void OnSuccess(ArrayList<PunchCard> punchCards);
    void OnSuccessReady(ArrayList<PunchCard> punchCards);
    void onSessionExpired();

}
