package com.urbanity.android.reward;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.urbanity.android.R;
import com.urbanity.android.bases.BaseActivity;
import com.urbanity.android.reward.fragment.FragmentReward;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.trademarks.TrademarkActivity;
import com.urbanity.android.trademarks.adapter.ViewPagerAdapterTabs;
import com.urbanity.android.trademarks.fragment.FragmentPunchNoAccount;

/**
 * Created by DRM on 24/05/17.
 */

public class RewardActivity extends BaseActivity{

    ViewPagerAdapterTabs adapter;

    ViewPager pager;
    TabLayout tabs;

    public  static CallbackManager callbackManager;
    public static LoginManager loginManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);
        callbackManager = CallbackManager.Factory.create();
        loginManager=LoginManager.getInstance();

        setTitleToolbar("Sellos y Premios");
        setUpTabs();



        findViewById(R.id.ly_news).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RewardActivity.this,TrademarkActivity.class));
                RewardActivity.this.finish();
            }
        });

        findViewById(R.id.ly_perfil).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(RewardActivity.this, "Rewards", Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.img_main_urban).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null){
                    mListener.changeFragmentEventMain();
                }
                finish();
            }
        });


    }

    public static ListenerEvent mListener;
    public static void setListener(ListenerEvent listener){
        mListener=listener;
    }
    public  interface ListenerEvent{
        void changeFragmentEventMain();
    }

    void setUpTabs(){
        adapter=new ViewPagerAdapterTabs(getSupportFragmentManager());

        if(UrbanPrefs.getInstance(RewardActivity.this).isLoged()){
            adapter.addFragment(FragmentReward.getInstance(RewardType.ALLREWARD),"Todos");
            adapter.addFragment(FragmentReward.getInstance(RewardType.REWARD),"Premios");
        }else {
            adapter.addFragment(FragmentPunchNoAccount.getInstance(),"Todos");
            adapter.addFragment(FragmentPunchNoAccount.getInstance(),"Premios");
        }


        pager=(ViewPager)findViewById(R.id.pager);
        pager.setAdapter(adapter);
        tabs=(TabLayout)findViewById(R.id.tabs);
        tabs.setupWithViewPager(pager);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

/*    private void setupTabIcons() {
        tabs.getTabAt(0).setIcon(R.mipmap.ic_launcher);
        tabs.getTabAt(1).setIcon(R.mipmap.ic_launcher);
    }*/
}
