package com.urbanity.android.reward.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.urbanity.android.R;
import com.urbanity.android.UrbanApplication;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.customsViews.customDialog.SimpleCustomDialog;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.model.PunchCard;
import com.urbanity.android.model.TypeDialog;
import com.urbanity.android.notify.FragmentNotifyMerchants;
import com.urbanity.android.notify.NotifyType;
import com.urbanity.android.reward.RewardType;
import com.urbanity.android.reward.RewardView;
import com.urbanity.android.reward.adapter.AdapterAllReward;
import com.urbanity.android.reward.presenter.RewardPresenter;
import com.urbanity.android.reward.presenter.RewardPresenterImp;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.trademarks.TrademarkActivity;
import com.urbanity.android.trademarks.perfil.TradeMarkPerfilActivity;
import com.urbanity.android.utils.UrbanUtils;

import java.util.ArrayList;

/**
 * Created by DRM on 12/06/16.
 */

public class FragmentReward extends BaseFragment implements RewardView {

    public static String TYPEREWARD="type";
    RecyclerView rvRewardPCards;
    ProgressDialog pDialog;
    RewardType type;
    RewardPresenter presenter;
    AdapterAllReward adapter;
    ProgressBar progressBar;



    public static FragmentReward getInstance(RewardType type){
        FragmentReward fragment=new FragmentReward();
        Bundle arg=new Bundle();
        arg.putSerializable(TYPEREWARD,type);
        fragment.setArguments(arg);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_trade_marks,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pDialog= UrbanUtils.getDialogUrban(getActivity(),getActivity().getResources().getString(R.string.app_name),"Cargando tiendas");
        rvRewardPCards=(RecyclerView)findViewById(R.id.rv_reward_pcards);
        rvRewardPCards.setLayoutManager(new LinearLayoutManager(getActivity()));
        type=(RewardType)getArguments().getSerializable(TYPEREWARD);
        presenter=new RewardPresenterImp(this);
        progressBar=(ProgressBar)findViewById(R.id.progress_bar);








        TrademarkActivity.setListener(new TrademarkActivity.ListenerEvent() {

            @Override
            public void changeFragmentEventMain() {
                if(getActivity()!=null){
                    getActivity().finish();
                }
            }

        });

    }

    @Override
    public void onResume() {
        super.onResume();
        switch (type){
            case REWARD:
//TODO mandar a contants
                FacebookAnalytics.FireBaseScreenTracking(getActivity(),"Punch Cards - Rewards");
                presenter.validateReadyReward(getActivity());

                break;

            case ALLREWARD:
//TODO mandar a contants
                FacebookAnalytics.FireBaseScreenTracking(getActivity(),"Punch Cards - All");
                presenter.validateGetAllReward(getActivity());

                break;
        }
    }

    @Override
    public void ShowProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void DismissProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void OnError(String error) {

    }

    @Override
    public void OnSuccess(ArrayList<PunchCard> punchCards) {

                adapter=new AdapterAllReward(getActivity(), punchCards, type ,  new AdapterAllReward.AllPunchCardListener() {
                    @Override
                    public void onItemSelected(PunchCard card, int position) {
                        launchNextActivity(card,position);
                    }
                });
                rvRewardPCards.setAdapter(adapter);
                adapter.notifyDataSetChanged();
    }

    @Override
    public void OnSuccessReady(ArrayList<PunchCard> punchCards) {
        if(punchCards.size()>0){
            adapter=new AdapterAllReward(getActivity(), punchCards, type, new AdapterAllReward.AllPunchCardListener() {
                @Override
                public void onItemSelected(PunchCard card, int position) {
                    launchNextActivity(card,position);
                }
            });
            rvRewardPCards.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }else {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //TODO mandar a contantss
                    FacebookAnalytics.FireBaseScreenTracking(getActivity(),"Punch Cards - Rewards [Empty]");
                    findViewById(R.id.content_state_merchants).setVisibility(View.VISIBLE);
                    getChildFragmentManager().beginTransaction().replace(R.id.content_state_merchants, FragmentNotifyMerchants.getInstance(NotifyType.ERROR_EMPTY_REWARD)).commit();

                }
            });
        }
    }

    @Override
    public void onSessionExpired() {
        SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), getResources().getString(R.string.error_session), getResources().getString(R.string.error_session_message), getResources().getString(R.string.buttonAccept), TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                UrbanApplication.getInstance().clearApplicationData();
                UrbanDB.getInstance(getActivity()).CloseSession();
                UrbanPrefs.getInstance(getActivity()).ClearPrefs();
                UrbanPrefs.getInstance(getActivity()).setIsLoged(false);
                UrbanPrefs.getInstance(getActivity()).setRunAppFirst(false);
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void launchNextActivity(PunchCard card, int position){
        Merchants mark=new Merchants();
        mark.setCategory(card.getCategory());
        mark.setColor(card.getMerchant_color());
        mark.setId(card.getMerchant_id());
        mark.setName(card.getMerchant_name());
        mark.setImage_url(card.getMerchant_image_url());
        //mark.setBudget_rating(getArguments().getString(BUDGETRATING));
        startActivity(new Intent(getActivity(), TradeMarkPerfilActivity.class).putExtra("merchant",mark).putExtra("position",position));
    }
}
