package com.urbanity.android.reward.interactor;

import android.content.Context;

import com.urbanity.android.reward.OnRewardFinishListener;

/**
 * Created by DRM on 21/07/17.
 */

public interface RewardInteractor {
    void getAllRewards(Context context, OnRewardFinishListener listener);
    void getReadyReward(Context context, OnRewardFinishListener listener);

}
