package com.urbanity.android.trademarks.viewInterface;

import com.urbanity.android.model.MerchantNear;
import com.urbanity.android.model.Merchants;

import java.util.ArrayList;

/**
 * Created by DRM on 24/05/17.
 */

public interface TradeMarkFinishListener {
    void onError(String error);
    void onSuccess(ArrayList<Merchants>marks);
    void onEmptyMerchants();



    void onSuccessNearMerchants(ArrayList<MerchantNear> nears);
}
