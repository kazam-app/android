package com.urbanity.android.trademarks.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.reward.RewardActivity;
import com.urbanity.android.trademarks.perfil.TradeMarkPerfilActivity;
import com.urbanity.android.user.PerfilUserActivity;
import com.urbanity.android.utils.UrbanConstants;

import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by DRM on 13/06/17.
 */

public class FragmentPunchNoAccount extends BaseFragment {

    LoginButton btFacebookLogin;

    UrbanUser user;
    public static FragmentPunchNoAccount getInstance(){
        return new FragmentPunchNoAccount();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_punch_no_account,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //TODO mandar a contants
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),"Merchant Profile - Punch Cards [No Account]");

        btFacebookLogin=(LoginButton)findViewById(R.id.bt_facebook_login);
        try {
            if(getActivity() instanceof TradeMarkPerfilActivity ){
                if(TradeMarkPerfilActivity.loginManager!=null){
                    TradeMarkPerfilActivity.loginManager.logOut();
                }
            }else {
                if(RewardActivity.loginManager!=null){
                    RewardActivity.loginManager.logOut();
                }
            }



        }catch (Exception ignore){}

        btFacebookLogin.setReadPermissions(Arrays.asList("public_profile, email"));
        btFacebookLogin.registerCallback(getActivity() instanceof TradeMarkPerfilActivity ? TradeMarkPerfilActivity.callbackManager : RewardActivity.callbackManager , new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult){
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        try {
                                            user=new UrbanUser();
                                            user.setFb_id(object.getString("id"));
                                            user.setName(object.getString("first_name"));
                                            user.setLast_names(object.getString("last_name"));
                                            user.setBirthdate(""); // user.setBirthdate(object.getString("birthday"));
                                            user.setGender(object.getString("gender"));
                                            user.setEmail(object.getString("email")!=null ?  object.getString("email") : "");


                                            AccessToken token = AccessToken.getCurrentAccessToken();
                                            if (token != null) {
                                                user.setFb_expires_at(String.valueOf(token.getExpires()));
                                                user.setFb_token(token.getToken());
                                            }

                                            startActivity(new Intent(getActivity(), PerfilUserActivity.class).putExtra(PerfilUserActivity.TYPE, UrbanConstants.REGISTER_FB).putExtra(PerfilUserActivity.USER,user));

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id ,first_name , email, gender, last_name");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }



                    @Override
                    public void onCancel() {
                        Log.e("onCancel","onCancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.e("onError",""+error.getMessage());
                    }
                });


















        findViewById(R.id.bt_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), PerfilUserActivity.class).putExtra(PerfilUserActivity.TYPE, UrbanConstants.REGISTER));
            }
        });
    }
}
