package com.urbanity.android.trademarks.fragment;

import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.interaction.fragment.FragmentSuccessResponse;
import com.urbanity.android.model.ItemMerchant;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.notify.FragmentNotifyMerchants;
import com.urbanity.android.notify.NotifyType;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.trademarks.adapter.AdapterMarks;
import com.urbanity.android.trademarks.adapter.AdapterMerchants;
import com.urbanity.android.trademarks.markType.TradeMarkType;
import com.urbanity.android.utils.UrbanDevice;
import com.urbanity.android.utils.UrbanGPSTracker;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DRM on 19/07/17.
 */

public class FragmentMerchantID extends BaseFragment {

    Merchants merchant;
    RecyclerView rv_merchant;
    AdapterMerchants adapter;


    LocationManager lmanager;
    UrbanGPSTracker mGPS;

    double lat, lng;

    public static FragmentMerchantID getInstance(Merchants merchant){
        FragmentMerchantID fragment=new FragmentMerchantID();
        Bundle arg=new Bundle();
        arg.putSerializable(FragmentSuccessResponse.MERCHANT,merchant);
        fragment.setArguments(arg);
        return fragment;

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.urban_recyclerview,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lmanager  = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);


        //TODO mandar a contants
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),"Merchant Profile - Shops");


        findViewById(R.id.swipe_data).setEnabled(false);
        merchant= (Merchants) getArguments().getSerializable(FragmentSuccessResponse.MERCHANT);
        rv_merchant=(RecyclerView)findViewById(R.id.urban_rv);
        rv_merchant.setLayoutManager(new LinearLayoutManager(getActivity()));


        mGPS= new UrbanGPSTracker(getActivity());
        if(mGPS.canGetLocation ){
            mGPS.getLocation();
            lat=mGPS.getLatitude();
            lng=mGPS.getLongitude();
        }



        Call<ArrayList<ItemMerchant>>getMerchants= RetrofitService.getUrbanService().getMerchantByID(UrbanDB.getInstance(getActivity()).getUrbanUser().getUrbanToken(),merchant.getId(), lat,lng
        );


        getMerchants.enqueue(new Callback<ArrayList<ItemMerchant>>() {
            @Override
            public void onResponse(Call<ArrayList<ItemMerchant>> call, Response<ArrayList<ItemMerchant>> response) {
                if(response.body()!=null){
                    adapter=new AdapterMerchants(getActivity(),response.body());
                    rv_merchant.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ItemMerchant>> call, Throwable t) {

            }
        });

    }
}
