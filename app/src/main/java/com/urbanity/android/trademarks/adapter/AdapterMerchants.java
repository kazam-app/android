package com.urbanity.android.trademarks.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.urbanity.android.R;
import com.urbanity.android.model.ItemMerchant;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.trademarks.markType.TradeMarkType;

import java.util.List;

/**
 * Created by lenovo on 16/05/2017.
 */

public class AdapterMerchants extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private final int MARK=0;
    private List<ItemMerchant> merchants;
    private int itemSelected=-1;

    private int lastPosition = -1;


    public AdapterMerchants(Context context, List<ItemMerchant> merchants){
        this.context=context;
        this.merchants=merchants;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View headerView;
        switch (viewType){

            case MARK:
                headerView=inflater.inflate(R.layout.row_mark,viewGroup,false);
                viewHolder=new ViewHolderItemMenu(headerView);
                break;

        }

        return viewHolder;
    }





    private class ViewHolderItemMenu extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txMarkName, txMarkAddress, txMarkCategory, txMarkPrice_on,txMarkPrice_off, txMarkDistance;
        ImageView imgRowMerchant;

        private ViewHolderItemMenu(View itemView){
            super(itemView);

            txMarkName =(TextView) itemView.findViewById(R.id.tx_row_mark_name);
            txMarkAddress =(TextView) itemView.findViewById(R.id.tx_row_mark_address);
            txMarkCategory =(TextView) itemView.findViewById(R.id.tx_row_mark_category);
            txMarkPrice_on =(TextView) itemView.findViewById(R.id.tx_row_mark_price_on);
            txMarkPrice_off =(TextView) itemView.findViewById(R.id.tx_row_mark_price_off);
            txMarkDistance =(TextView) itemView.findViewById(R.id.tx_row_mark_distance);
            imgRowMerchant=(ImageView)itemView.findViewById(R.id.img_row_merchant);

                itemView.setOnClickListener(this);



        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onMerchantItemSelected(merchants.get(getAdapterPosition()));
            }
            notifyItemChanged(getAdapterPosition());
            itemSelected = getLayoutPosition();
            notifyItemChanged(getAdapterPosition());
            notifyDataSetChanged();
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolderItemMenu aforeHolder;
      //  setAnimation(holder.itemView, position);

        switch (holder.getItemViewType()){

            case MARK:
                aforeHolder =(ViewHolderItemMenu) holder;
                configureViewHolderItemMenu(aforeHolder,position);
                break;
        }



    }

    private MerchantListener listener;
    public interface MerchantListener {
        void onMerchantItemSelected(ItemMerchant merchant);
    }

    public void setListener(MerchantListener listener) {
        this.listener = listener;
    }



    private void configureViewHolderItemMenu(final ViewHolderItemMenu holder, final int position) {
        ItemMerchant merchant=merchants.get(position);

        Picasso.with(context).load(merchant.getMerchant_image_url()).placeholder(context.getResources().getDrawable(R.drawable.ic_mark)).error(context.getResources().getDrawable(R.drawable.ic_mark)).into(holder.imgRowMerchant);
        holder.txMarkName.setText(merchant.getName());
        holder.txMarkCategory.setText(merchant.getCategory());

        holder.txMarkCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);


        switch (merchant.getMerchant_budget_rating()){
            case 1:
                holder.txMarkPrice_on.setText(String.valueOf("$"));
                holder.txMarkPrice_off.setText(String.valueOf("$$$$"));
                break;

            case 2:
                holder.txMarkPrice_on.setText(String.valueOf("$$"));
                holder.txMarkPrice_off.setText(String.valueOf("$$$"));
                break;

            case 3:
                holder.txMarkPrice_on.setText(String.valueOf("$$$"));
                holder.txMarkPrice_off.setText(String.valueOf("$$"));
                break;

            case 4:
                holder.txMarkPrice_on.setText(String.valueOf("$$$$"));
                holder.txMarkPrice_off.setText(String.valueOf("$"));
                break;

            case 5:
                holder.txMarkPrice_on.setText(String.valueOf("$$$$$"));
                holder.txMarkPrice_off.setText(String.valueOf(""));
                break;

            default:
                holder.txMarkPrice_on.setText(String.valueOf(""));
                holder.txMarkPrice_off.setText(String.valueOf("$$$$$"));
                break;
        }


        holder.txMarkDistance.setText(String.valueOf(merchant.getDistance()));
        holder.txMarkAddress.setText(String.valueOf(merchant.getShop_cluster()));


    }


    @Override
    public  int getItemViewType(int position) {

        if(merchants.get(position) != null){
            return MARK;
        }
        return position;
    }

    @Override
    public int getItemCount() {
        return merchants.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}










