package com.urbanity.android.trademarks.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.urbanity.android.R;
import com.urbanity.android.redeem.fragment.FragmentRedeemBluetooth;

/**
 * Created by lenovo on 16/05/2017.
 */

public class AdapterPunchCard extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;

    private int punch_limit;
    private int punch_count;
    private int itemSelected=-1;
    int size;
    boolean isReward;


    private int lastPosition = -1;


    public AdapterPunchCard(Context context, int punch_count, int punch_limit, int size, boolean isReward){
        this.context=context;
        this.punch_count=punch_count;
        this.punch_limit=punch_limit;
        this.size=size;
        this.isReward=isReward;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View headerView;

        if (size==0){//Small
            headerView=inflater.inflate(R.layout.row_punch_card_small,viewGroup,false);
        }else {//large
            headerView=inflater.inflate(R.layout.row_punch_card_big,viewGroup,false);
        }
                viewHolder=new ViewHolderItemMenu(headerView);

        return viewHolder;
    }





    private class ViewHolderItemMenu extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView img_row_punch_card;

        private ViewHolderItemMenu(View itemView){
            super(itemView);
            img_row_punch_card=(ImageView)itemView.findViewById(R.id.img_row_punch_card);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            notifyItemChanged(getAdapterPosition());
            itemSelected = getLayoutPosition();
            notifyItemChanged(getAdapterPosition());
            notifyDataSetChanged();

           if(mListener!=null){
               mListener.onItemSelected();
           }
        }
    }

    public  OnItemListener mListener;
    public  void setListener(OnItemListener listener){
        this.mListener=listener;
    }
    public interface OnItemListener{
        void onItemSelected();
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolderItemMenu aforeHolder;


        //Validar si es necesario la animacion
        if(FragmentRedeemBluetooth.SETANIMATION){
            if(position+1==punch_count){
                setAnimation(holder.itemView, position);
                FragmentRedeemBluetooth.SETANIMATION=false;
            }


        }


                aforeHolder =(ViewHolderItemMenu) holder;
                configureViewHolderItemMenu(aforeHolder,position);
    }



    private void configureViewHolderItemMenu(final ViewHolderItemMenu holder, final int position) {

        if(punch_limit>10&&!isReward){
           holder.img_row_punch_card.getLayoutParams().width=100;
           holder.img_row_punch_card.getLayoutParams().height=100;
        }
        if(position+1<=punch_count){
            holder.img_row_punch_card.setBackgroundResource(R.drawable.punch_card_full);
        }else {
            holder.img_row_punch_card.setBackgroundResource(R.drawable.punch_card_empty);
        }
        
    }


    @Override
    public  int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return punch_limit;
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.anim_punch_card);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}










