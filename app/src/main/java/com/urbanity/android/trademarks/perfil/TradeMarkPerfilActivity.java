package com.urbanity.android.trademarks.perfil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;
import com.urbanity.android.R;
import com.urbanity.android.bases.BaseActivity;
import com.urbanity.android.broadcastReceiver.GpsLocationReceiver;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.notify.FragmentNotifyMerchants;
import com.urbanity.android.notify.NotifyType;
import com.urbanity.android.reward.RewardActivity;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.trademarks.TrademarkActivity;
import com.urbanity.android.trademarks.adapter.ViewPagerAdapterTabs;
import com.urbanity.android.trademarks.fragment.FragmentMerchantID;
import com.urbanity.android.trademarks.fragment.FragmentPunchNoAccount;
import com.urbanity.android.trademarks.fragment.FragmentTradeMarks;
import com.urbanity.android.trademarks.markType.TradeMarkType;
import com.urbanity.android.trademarks.perfil.fragment.SealMarkFragment;
import com.urbanity.android.user.register.FragmentActiveLocation;
import com.urbanity.android.utils.UrbanDevice;
import com.urbanity.android.utils.UrbanGPSTracker;
import com.urbanity.android.utils.UrbanUtils;

import org.w3c.dom.Text;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by DRM on 24/05/17.
 */

public class TradeMarkPerfilActivity extends BaseActivity {

    ViewPagerAdapterTabs adapter;
    ViewPager pager;
    TabLayout tabs;
    Merchants mark;
    TextView txMarkName, txMarkCategory, txMarkPriceOn, txMarkPriceOff;
    ImageView img_perfil_mark;
    int position=0;


    public  static CallbackManager callbackManager;
    public static LoginManager loginManager;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trademark_perfil);
        setTitleToolbar("Perfil");



        callbackManager = CallbackManager.Factory.create();
        loginManager=LoginManager.getInstance();
        txMarkName=(TextView)findViewById(R.id.tx_mark_name);
        txMarkCategory=(TextView)findViewById(R.id.tx_mark_category);
        img_perfil_mark=(ImageView)findViewById(R.id.img_perfil_mark);
        txMarkPriceOn=(TextView)findViewById(R.id.tx_mark_price_on);
        txMarkPriceOff=(TextView)findViewById(R.id.tx_mark_price_off);


        Bundle arg=getIntent().getExtras();
        if(arg!=null){
            mark= (Merchants) arg.getSerializable("merchant");
            position=arg.getInt("position");

            txMarkName.setText(mark.getName());
            txMarkCategory.setText(mark.getCategory());


            Picasso.with(this).load(mark.getImage_url()).placeholder(getResources().getDrawable(R.drawable.ic_mark)).error(getResources().getDrawable(R.drawable.ic_mark)).into(img_perfil_mark);

            setUpTabs();

            if(mark.getColor()!=null && mark.getColor().length()>5 && !mark.getColor().equals("null")){
                Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
                toolbar.setBackgroundColor(Color.parseColor(mark.getColor()));
                LinearLayout content_detail_merchant =(LinearLayout)findViewById(R.id.content_detail_merchant);
                content_detail_merchant.setBackgroundColor(Color.parseColor(mark.getColor()));
                tabs.setBackgroundColor(Color.parseColor(mark.getColor()));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    getWindow().setStatusBarColor(Color.parseColor(mark.getColor()));
                }
            }

            int rating;
            try{
                rating= Integer.parseInt(mark.getBudget_rating());
            }catch (Exception e){
                rating=3;
            }
            switch (rating){
                case 0:
                    txMarkPriceOn.setText("");
                    txMarkPriceOff.setText("$$$$$");
                    break;

                case 1:
                    txMarkPriceOn.setText("$");
                    txMarkPriceOff.setText("$$$$");
                    break;

                case 2:
                    txMarkPriceOn.setText("$$");
                    txMarkPriceOff.setText("$$$");
                    break;

                case 3:
                    txMarkPriceOn.setText("$$$");
                    txMarkPriceOff.setText("$$");
                    break;

                case 4:
                    txMarkPriceOn.setText("$$$$");
                    txMarkPriceOff.setText("$");
                    break;

                case 5:
                    txMarkPriceOn.setText("$$$$$");
                    txMarkPriceOff.setText("");
                    break;

            }

        }

        findViewById(R.id.ly_news).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(TradeMarkPerfilActivity.this, "Marks", Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.ly_perfil).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TradeMarkPerfilActivity.this,RewardActivity.class));
                TradeMarkPerfilActivity.this.finish();
            }
        });


        findViewById(R.id.img_main_urban).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                TrademarkActivity.changeMain();

            }
        });


        GpsLocationReceiver.setNetworkListener(new GpsLocationReceiver.OnLocationListener() {
            @Override
            public void onLocationEnabled(boolean state) {

                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {

                        try{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    Log.e("Location","hacer algo");
                                }

                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                };
                Timer timer = new Timer();
                timer.schedule(task, 600);

            }
        });


    }




    void setUpTabs(){
        adapter=new ViewPagerAdapterTabs(getSupportFragmentManager());
        if(UrbanPrefs.getInstance(this).isLoged()){
               adapter.addFragment(SealMarkFragment.getInstance(mark,position),"Sellos y premios");
               adapter.addFragment(FragmentMerchantID.getInstance(mark),"Tiendas");
        }else{
            adapter.addFragment(FragmentPunchNoAccount.getInstance(),"Sellos y premios");
            adapter.addFragment(FragmentPunchNoAccount.getInstance(),"Tiendas");

        }



        pager=(ViewPager)findViewById(R.id.pager);
        pager.setAdapter(adapter);
        tabs=(TabLayout)findViewById(R.id.tabs);
        tabs.setupWithViewPager(pager);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
