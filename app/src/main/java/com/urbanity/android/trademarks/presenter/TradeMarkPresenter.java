package com.urbanity.android.trademarks.presenter;

import android.content.Context;

/**
 * Created by DRM on 24/05/17.
 */

public interface TradeMarkPresenter {
    void validateGetTradeMark(Context context);

    void validateGetTradeMark(Context context,double lan, double lng);
}
