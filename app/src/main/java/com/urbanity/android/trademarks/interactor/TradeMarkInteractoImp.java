package com.urbanity.android.trademarks.interactor;

import android.content.Context;
import android.util.Log;

import com.urbanity.android.model.MerchantNear;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.trademarks.viewInterface.TradeMarkFinishListener;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DRM on 24/05/17.
 */

public class TradeMarkInteractoImp implements TradeMarkInteractor {

    @Override
    public void getTradeMarks(Context context, final TradeMarkFinishListener listener) {

        Call<ArrayList<Merchants>> getMerchantslist= RetrofitService.getUrbanService().getMerchantsList();

        getMerchantslist.enqueue(new Callback<ArrayList<Merchants>>() {
            @Override
            public void onResponse(Call<ArrayList<Merchants>> call, Response<ArrayList<Merchants>> response) {
               if(response.body()!=null && response.body().size()>0){
                   listener.onSuccess(response.body());
                   return;
               }
                   listener.onEmptyMerchants();
            }

            @Override
            public void onFailure(Call<ArrayList<Merchants>> call, Throwable t) {
                listener.onError("Algo salio mal, intentalo mas tarde.");
            }
        });
    }

    @Override
    public void getTradeMarks(Context context, final double lat, double lng, final TradeMarkFinishListener listener) {
        Call<ArrayList<MerchantNear>>getMerchantsNear=RetrofitService.getUrbanService().getNearMerchantsList(lat,lng);
        getMerchantsNear.enqueue(new Callback<ArrayList<MerchantNear>>() {
            @Override
            public void onResponse(Call<ArrayList<MerchantNear>> call, Response<ArrayList<MerchantNear>> response) {
                if(response.body()!= null && response.body().size()>0){
                    listener.onSuccessNearMerchants(response.body());
                }else {
                    listener.onEmptyMerchants();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MerchantNear>> call, Throwable t) {
                listener.onError("Ocurrio un error en el servicio");
            }
        });


    }
}
