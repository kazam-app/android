package com.urbanity.android.trademarks.fragment;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.broadcastReceiver.GpsLocationReceiver;
import com.urbanity.android.model.MerchantNear;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.notify.FragmentNoNearMerchant;
import com.urbanity.android.notify.FragmentNotifyMerchants;
import com.urbanity.android.notify.NotifyType;
import com.urbanity.android.reward.RewardActivity;
import com.urbanity.android.trademarks.adapter.AdapterMarks;
import com.urbanity.android.trademarks.adapter.AdapterNears;
import com.urbanity.android.trademarks.markType.TradeMarkType;
import com.urbanity.android.trademarks.perfil.TradeMarkPerfilActivity;
import com.urbanity.android.trademarks.presenter.TradeMarkPresenter;
import com.urbanity.android.trademarks.presenter.TradeMarkPresenterImp;
import com.urbanity.android.trademarks.viewInterface.TradeMarkView;
import com.urbanity.android.utils.UrbanDevice;
import com.urbanity.android.utils.UrbanGPSTracker;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by DRM on 24/05/17.
 */

public class
FragmentTradeMarks extends BaseFragment implements TradeMarkView {


    TradeMarkPresenter presenter;
    ProgressBar progressBar;
    AdapterMarks adapter;
    AdapterNears adapterNears;

    RecyclerView rvMarks;
    TradeMarkType type;

    LocationManager lmanager;
    UrbanGPSTracker mGPS;



    public static  FragmentTradeMarks getInstance(TradeMarkType type){
        FragmentTradeMarks fragment=new FragmentTradeMarks();
        Bundle arg=new Bundle();
        arg.putSerializable("type",type);
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_trade_marks,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        progressBar= (ProgressBar)findViewById(R.id.progress_bar);
        rvMarks=(RecyclerView)findViewById(R.id.rv_reward_pcards);
        presenter=new TradeMarkPresenterImp(this);
        type= (TradeMarkType) getArguments().getSerializable("type");
        lmanager  = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);




        switch (type){
            case ALL:
                //TODO mandar a contants
                FacebookAnalytics.FireBaseScreenTracking(getActivity(),"Merchant List - All");


                findViewById(R.id.content_state_merchants).setVisibility(View.GONE);
                presenter.validateGetTradeMark(getActivity());
                break;

            case CLOSEOFME:
//TODO mandar a contants
                FacebookAnalytics.FireBaseScreenTracking(getActivity(),"Merchant List - Near Me");


                validateLocation();

                break;
        }



        GpsLocationReceiver.setNetworkListener(new GpsLocationReceiver.OnLocationListener() {
            @Override
            public void onLocationEnabled(boolean state) {

                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {

                        try{
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    validateLocation();

                                }
                            });
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                };
                Timer timer = new Timer();
                timer.schedule(task, 600);

            }
        });

        FragmentNotifyMerchants.setListener(new FragmentNotifyMerchants.OnChangeListener() {
            @Override
            public void onChangeListener() {
               try{
                   validateLocation();
               }catch (Exception e){
                   e.printStackTrace();
               }
            }
        });










    }


    void validateLocation(){
        mGPS= new UrbanGPSTracker(getActivity());

       try{
           getActivity().runOnUiThread(new Runnable() {
               @Override
               public void run() {


                   if(!UrbanDevice.isLocationEnabled(getActivity()) || !UrbanDevice.checkLocationPermission(getActivity())){
                       findViewById(R.id.content_state_merchants).setVisibility(View.VISIBLE);
                       getChildFragmentManager().beginTransaction().replace(R.id.content_state_merchants, FragmentNotifyMerchants.getInstance(NotifyType.ERROR_LOCATION)).commit();
                       rvMarks.setVisibility(View.GONE);
                       return;
                   }

                   if(mGPS.canGetLocation ){
                       rvMarks.setVisibility(View.VISIBLE);
                       findViewById(R.id.content_state_merchants).setVisibility(View.GONE);
                       mGPS.getLocation();
                       presenter.validateGetTradeMark(getActivity(),mGPS.getLatitude(),mGPS.getLongitude());
                   }
               }
           });
       }catch (Exception e){

       }
    }



    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onErrorNetWork() {

    }

    @Override
    public void onEmptyMerchants() {
        if(findViewById(R.id.content_state_merchants)!=null){
            findViewById(R.id.content_state_merchants).setVisibility(View.VISIBLE);
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {

                        if(getActivity()!=null){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    getChildFragmentManager().beginTransaction().replace(R.id.content_state_merchants, FragmentNoNearMerchant.getInstance()).commit();
                                }
                            });
                        }


                    }
                };
                Timer timer = new Timer();
                timer.schedule(task,200);


            }
        });
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onSuccess(ArrayList<Merchants> items) {

        adapter=new AdapterMarks(getActivity(),items, type);
        rvMarks.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMarks.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        adapter.setListener(new AdapterMarks.MarkItemListener() {
            @Override
            public void onMarkItemSelected(Merchants mark) {
                startActivity(new Intent(getActivity(), TradeMarkPerfilActivity.class).putExtra("merchant",mark));
            }
        });

    }

    @Override
    public void onSuccessNear(ArrayList<MerchantNear> nears) {

        adapterNears=new AdapterNears(getActivity(),nears, type);
        rvMarks.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMarks.setAdapter(adapterNears);
        adapterNears.notifyDataSetChanged();
        adapterNears.setListener(new AdapterNears.MarkItemListener() {
            @Override
            public void onMarkItemSelected(MerchantNear mark) {

                Merchants m=new Merchants();
                m.setName(mark.getMerchant_name());
                m.setCategory(mark.getCategory());
                m.setId(mark.getMerchant_id());
                m.setImage_url(mark.getMerchant_image_url());
                m.setBudget_rating(mark.getMerchant_budget_rating());
                m.setColor(mark.getMerchant_color());
                startActivity(new Intent(getActivity(), TradeMarkPerfilActivity.class).putExtra("merchant",m));

            }
        });

    }
}
