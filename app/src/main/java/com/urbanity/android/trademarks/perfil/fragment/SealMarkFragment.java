package com.urbanity.android.trademarks.perfil.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.urbanity.android.R;
import com.urbanity.android.UrbanApplication;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.customsViews.cristalViewPager.CrystalViewPager;
import com.urbanity.android.customsViews.customDialog.SimpleCustomDialog;
import com.urbanity.android.interaction.beacon.punchCard.PunchCardView;
import com.urbanity.android.interaction.beacon.punchCard.presenter.PunchCardPresenter;
import com.urbanity.android.interaction.beacon.punchCard.presenter.PunchCardPresenterImp;
import com.urbanity.android.interaction.fragment.FragmentSuccessResponse;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.model.ItemRedeemResponse;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.model.PunchCard;
import com.urbanity.android.model.TypeDialog;
import com.urbanity.android.notify.FragmentEmptyPunchCard;
import com.urbanity.android.onBoarding.FragmentAdapter;
import com.urbanity.android.redeem.fragment.FragmentPunchCardExchange;
import com.urbanity.android.redeem.fragment.FragmentRedeemBluetooth;
import com.urbanity.android.redeem.fragment.FragmentScanQR;
import com.urbanity.android.redeem.presenter.RedeemPresenterImp;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.utils.UrbanDevice;
import com.urbanity.android.utils.UrbanUtils;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by DRM on 24/05/17.
 */

public class SealMarkFragment extends BaseFragment implements PunchCardView,FragmentRedeemBluetooth.OnClearAdapterListener {

    CrystalViewPager viewPager;

    CirclePageIndicator indicator;
    Merchants merchant;
    PunchCardPresenter presenter;
    ProgressDialog pDialog;
    public static String POSITION="POSITION";
    int position=0;
    FragmentAdapter adapter;

    LinearLayout contentPager;
    LinearLayout.LayoutParams params;
    Point screenSize;



    public static SealMarkFragment getInstance(Merchants merchant, int position){
        SealMarkFragment fragment=new SealMarkFragment();
        Bundle arg=new Bundle();
        arg.putSerializable(FragmentSuccessResponse.MERCHANT,merchant);
        arg.putInt(POSITION,position);
        fragment.setArguments(arg);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_seal_mark,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //TODO mandar a contants
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),"Merchant Profile - Punch Cards");


        pDialog= UrbanUtils.getDialogUrban(getActivity(),null,"Un momento por favor");
        merchant=(Merchants)getArguments().getSerializable(FragmentSuccessResponse.MERCHANT);
        FragmentRedeemBluetooth.setListener(this);
        viewPager= (CrystalViewPager) findViewById(R.id.viewpager);
        indicator=(CirclePageIndicator)findViewById(R.id.indicator);
        presenter=new PunchCardPresenterImp(this);
        contentPager = (LinearLayout)findViewById(R.id.content_pager);
        screenSize =UrbanDevice.getSizeScreen(getActivity());


        position=getArguments().getInt(POSITION);
        findCards();

        FragmentScanQR.setListenerRefress(new FragmentScanQR.OnRefresCardsListener() {
            @Override
            public void OnRefresh() {
                ClearAdapterCard();
                findCards();
            }
        });


        CardMarkFragment.setListenerSize(new CardMarkFragment.ChangeSizeListener() {

            @Override
            public void onFullCard(final int position) {
                TimerTask task1 = new TimerTask() {
                    @Override
                    public void run() {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                viewPager.setCurrentItem(position+1);

                            }
                        });

                    }
                };
                Timer timer1 = new Timer();
                timer1.schedule(task1, 1000);
            }
        });

    }

    public void findCards(){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                presenter.validatePunchCardMerchant(getActivity(),merchant.getId());
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void ShowProgress() {
        pDialog.show();
    }

    @Override
    public void HideProgress() {
        pDialog.dismiss();
    }

    @Override
    public void onEmptyPunchCard() {

        FragmentAdapter adapter = new FragmentAdapter(getChildFragmentManager());
        adapter.addFragment(FragmentEmptyPunchCard.getInstance(merchant.getName()));

        if(viewPager!=null){
            viewPager.setAdapter(adapter);
            indicator.setViewPager(viewPager);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onSuccess(ArrayList<PunchCard> items) {

        adapter = new FragmentAdapter(getChildFragmentManager());

        for (int i=0; i<items.size(); i++){
            adapter.addFragment(CardMarkFragment.getInstance(items.get(i),merchant,"profile", i , items.get(i).getPunch_count()));
        }


        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Validar si hay recompenzas en cache
                        //Validar si hay recompenzas en cache
                        if(UrbanDB.getInstance(getActivity()).getUrbanCards(merchant.getId()).size()>0){
                            for(ItemRedeemResponse item: UrbanDB.getInstance(getActivity()).getUrbanCards(merchant.getId())){
                                adapter.addFragment(FragmentPunchCardExchange.getInstance(item));
                            }
                            adapter.notifyDataSetChanged();
                        }

                        FragmentPunchCardExchange.setListener(new FragmentPunchCardExchange.OnDeleteCardListener() {
                            @Override
                            public void DeleteCard(final Fragment fragment) {
                             /*   getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        /*if(viewPager!=null){
                                            adapter.removeFragment(getFragmentManager(),fragment);
                                            viewPager.setAdapter(adapter);
                                            indicator=(CirclePageIndicator)findViewById(R.id.indicator);
                                            indicator.setViewPager(viewPager);
                                            adapter.notifyDataSetChanged();
                                            indicator.notifyDataSetChanged();
                                        }

                                    }
                                });*/
                            }
                        });



                    }
                });

            }
        };
        Timer timer = new Timer();
        timer.schedule(task, 100);

        if(viewPager!=null){
            viewPager.setAdapter(adapter);
            indicator.setViewPager(viewPager);
            adapter.notifyDataSetChanged();
        }
        if(viewPager!=null){
            viewPager.setCurrentItem(adapter.getCount()-1);
        }
            viewPager.setCurrentItem(position);






    }








    @Override
    public void onError(String error) {
        Toast.makeText(getActivity(), ""+error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorNetwork() {

    }

    @Override
    public void onSessionExpired() {
        SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), "Error en sesión", "Su sesión ha expirado, es necesario que vuelva a iniciar sesión", "Aceptar", TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                UrbanApplication.getInstance().clearApplicationData();
                UrbanDB.getInstance(getActivity()).CloseSession();
                UrbanPrefs.getInstance(getActivity()).ClearPrefs();
                UrbanPrefs.getInstance(getActivity()).setIsLoged(false);
                UrbanPrefs.getInstance(getActivity()).setRunAppFirst(false);
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void ClearAdapter() {
        ClearAdapterCard();
        findCards();
    }

    public void ClearAdapterCard(){
        try {
            List<Fragment> fragments = getChildFragmentManager().getFragments();
            if (fragments != null) {
                FragmentTransaction ft = getChildFragmentManager().beginTransaction();
                for (Fragment f : fragments) {
                    //You can perform additional check to remove some (not all) fragments:
                    if (f instanceof CardMarkFragment) {
                        ft.remove(f);
                    }
                }
                ft.commitAllowingStateLoss();
            }
        }catch (Exception e){e.printStackTrace();}
    }
}
