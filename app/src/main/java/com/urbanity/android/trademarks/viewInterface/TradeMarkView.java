package com.urbanity.android.trademarks.viewInterface;

import com.urbanity.android.model.MerchantNear;
import com.urbanity.android.model.Merchants;

import java.util.ArrayList;

/**
 * Created by DRM on 24/05/17.
 */

public interface TradeMarkView {
    void showProgress();
    void hideProgress();
    void onErrorNetWork();
    void onEmptyMerchants();

    void onError(String error);
    void onSuccess(ArrayList<Merchants>items);


    void onSuccessNear(ArrayList<MerchantNear> nears);

}
