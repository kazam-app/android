package com.urbanity.android.trademarks.perfil.fragment;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.urbanity.android.R;
import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.customsViews.customDialog.SimpleCustomDialog;
import com.urbanity.android.interaction.fragment.FragmentSuccessResponse;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.model.PunchCard;
import com.urbanity.android.model.TypeDialog;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.redeem.ActivityRedeemMerchant;
import com.urbanity.android.redeem.TypeRedeem;
import com.urbanity.android.rulesReward.ActivityRulesReward;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.trademarks.adapter.AdapterPunchCard;
import com.urbanity.android.utils.UrbanDevice;

import java.util.HashMap;

/**
 * Created by DRM on 24/05/17.
 */

public class CardMarkFragment extends BaseFragment{

    RecyclerView rvRewardSeal;
    public static String CARD="CARD";
    public static String TYPE_REDEEM="TYPE_REDEEM";
    TextView tx_rules_reward, tx_terms_reward;
    PunchCard card;
    Button bt_canje, bt_sello;
    Merchants merchant;
    public static String SCREENSOURCE="screen_source";
    AdapterPunchCard adapter;
    UrbanUser user;

    LinearLayout card_urban;

    public static int POSITIONCARD =0;

    public static final String POSITIONINFRAGMENT =  "POSITIONINFRAGMENT";
    public static final String COUNTPUNCHCARD =  "COUNTPUNCHCARD";

    public static CardMarkFragment getInstance(PunchCard card, Merchants merchant, String screen_source , int positionInFragment, int countPunchCard){
        CardMarkFragment fragment=new CardMarkFragment();
        Bundle arg=new Bundle();
        arg.putSerializable(CARD,card);
        arg.putSerializable(FragmentSuccessResponse.MERCHANT,merchant);
        arg.putString(SCREENSOURCE,screen_source);
        arg.putInt(POSITIONINFRAGMENT,positionInFragment);
        arg.putInt(COUNTPUNCHCARD,countPunchCard);
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.mark_card_fragment,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tx_rules_reward=(TextView)findViewById(R.id.tx_rules_reward);

        tx_terms_reward=(TextView)findViewById(R.id.tx_terms_reward);

        rvRewardSeal=(RecyclerView)findViewById(R.id.rv_reward_punch_card);
        rvRewardSeal.setLayoutManager(new GridLayoutManager(getActivity(),5));
        bt_canje=(Button) findViewById(R.id.bt_canje);
        bt_sello=(Button) findViewById(R.id.bt_sello);
        user= UrbanDB.getInstance(getActivity()).getUrbanUser();
        card=(PunchCard)getArguments().getSerializable(CARD);
        merchant=(Merchants)getArguments().getSerializable(FragmentSuccessResponse.MERCHANT);

        card_urban =(LinearLayout)findViewById(R.id.card_urban);


        bt_canje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String,String> params=new HashMap<>();
                params.put(EventsFB.PARAM_PUNCHCARD_USER_ID,user.getUrbanId());
                params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_NAME,merchant.getName());
                params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_ID,merchant.getId());
                params.put(EventsFB.PARAM_PUNCHCARD_ID,card.getId());
                params.put(EventsFB.PARAM_PUNCHCARD_CATEGORY,merchant.getCategory());
                params.put(EventsFB.PARAM_SCREEN_SOURCE,getArguments().getString(SCREENSOURCE));

                FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_PUNCHCARD_TRY_REDEEM,params);



                if(card.getIdentity()!=1){
                    SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), "Completa tu Tarjeta", "Asegúrate de tener todos los sellos necesarios antes de canjear tu premio.", "Aceptar", TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
                        @Override
                        public void onAcceptClickListener(Button button, AlertDialog dialog) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                return;
                }

                startActivity(new Intent(getActivity(),ActivityRedeemMerchant.class).putExtra(CARD,card).putExtra(FragmentSuccessResponse.MERCHANT,merchant).putExtra(TYPE_REDEEM,TypeRedeem.EXCHANGE).putExtra("screen_source",getArguments().getString(SCREENSOURCE)));
            }
        });

        bt_sello.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                POSITIONCARD = getArguments().getInt(POSITIONINFRAGMENT);
                if(card.getValidation_date()!=null){
                    HashMap<String,String> params=new HashMap<>();
                    params.put(EventsFB.PARAM_PUNCHCARD_USER_ID,UrbanDB.getInstance(getActivity()).getUrbanUser().getUrbanId());
                    params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_NAME,merchant.getName());
                    params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_ID, merchant.getId());
                    params.put(EventsFB.PARAM_PUNCHCARD_ID , card.getId());
                    params.put(EventsFB.PARAM_PUNCHCARD_CATEGORY,merchant.getCategory());
                    params.put(EventsFB.PARAM_SCREEN_SOURCE,"interaction" );

                    FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_PUNCH_LIMIT_REACHED,params);
                    SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), "¡Oops! Llegaste al Límite", "Has llegado al límite de sellos que puedes obtener de esta marca hoy, vuelve mañana.", "¡OK!", TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
                        @Override
                        public void onAcceptClickListener(Button button, AlertDialog dialog) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    return;

                }



                HashMap<String,String> params=new HashMap<>();
                params.put(EventsFB.PARAM_PUNCHCARD_USER_ID,user.getUrbanId());
                params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_NAME,merchant.getName());
                params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_ID,merchant.getId());
                params.put(EventsFB.PARAM_PUNCHCARD_ID,card.getId());
                params.put(EventsFB.PARAM_PUNCHCARD_CATEGORY,merchant.getCategory());
                params.put(EventsFB.PARAM_SCREEN_SOURCE,getArguments().getString(SCREENSOURCE));


                FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_PUNCHCARD_TRY_PUNCH,params);


                if(card.getIdentity()==1){
                    SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), "Tarjeta Completa", "Ya has completado esta tarjeta.", "Aceptar", TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
                        @Override
                        public void onAcceptClickListener(Button button, AlertDialog dialog) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    return;
                }

                    startActivity(new Intent(getActivity(),  ActivityRedeemMerchant.class) .putExtra(CARD,card) .putExtra(FragmentSuccessResponse.MERCHANT,merchant) .putExtra(TYPE_REDEEM,TypeRedeem.SEAL).putExtra("screen_source",getArguments().getString(SCREENSOURCE)));

            }
        });



        tx_terms_reward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(),ActivityRulesReward.class).putExtra("date",card.getExpires_at()).putExtra("rules",card.getRules()).putExtra("term",card.getTerms()));            }
        });



        tx_rules_reward.setText(String.valueOf("Premio: "+ card.getPrize()));


        if(card.getIdentity()==1){
            bt_sello.setBackgroundResource(R.drawable.selector_cancel_left);
            bt_canje.setBackgroundResource(R.drawable.selector_green_right);
        }


        //TODO esta linea de codigo nos sirve para agregar punch cart limit
     //  card.setPunch_limit(7);


        adapter =new AdapterPunchCard(getActivity(),card.getPunch_count(),card.getPunch_limit(),1,false);
        rvRewardSeal.setAdapter(adapter);
        adapter.notifyDataSetChanged();





        if(sListener!=null){
            if(card.getPunch_limit()<=5){
                resizeLayout(0);
            }else if(card.getPunch_limit()>5 && card.getPunch_limit()<=10){
                resizeLayout(1);
            }else if(card.getPunch_limit()>10) {
                resizeLayout(2);
            }
        }





        if(getArguments().getInt(COUNTPUNCHCARD) == card.getPunch_limit()){
            sListener.onFullCard(POSITIONCARD);
        }
    }


    public static ChangeSizeListener sListener;
    public static void setListenerSize(ChangeSizeListener listener){
        sListener=listener;
    }
    public interface ChangeSizeListener{
        void onFullCard(int position);
    }


    public void resizeLayout(int value) {
        Point screenSize = UrbanDevice.getSizeScreen(getActivity());
        int extraHeight=0;
        if(card.getPrize().length()>30){
            extraHeight=25;
        }

        int size = 0;
        switch (value) {
            case 0:
                size = screenSize.y / 3;
                // size = 620;

                break;

            case 1:  size = (screenSize.y / 3 ) + 80 ;

            break;

            case 2:

                size = screenSize.y / 3 + (screenSize.y / 10);
                //size = 840;

                break;
        }

        card_urban.getLayoutParams().height = size+extraHeight;
    }



}