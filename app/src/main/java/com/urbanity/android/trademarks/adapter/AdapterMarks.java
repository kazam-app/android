package com.urbanity.android.trademarks.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.urbanity.android.R;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.trademarks.markType.TradeMarkType;

import java.util.List;

/**
 * Created by lenovo on 16/05/2017.
 */

public class AdapterMarks extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private final int MARK=0;
    private List<Merchants> mMarks;
    private int itemSelected=-1;
    private MarkItemListener listener;
    private int lastPosition = -1;
    TradeMarkType type;


    public AdapterMarks(Context context, List<Merchants> mMarks, TradeMarkType type){
        this.context=context;
        this.mMarks=mMarks;
        this.type=type;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View headerView;
        switch (viewType){

            case MARK:
                headerView=inflater.inflate(R.layout.row_mark,viewGroup,false);
                viewHolder=new ViewHolderItemMenu(headerView);
                break;

        }

        return viewHolder;
    }





    private class ViewHolderItemMenu extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txMarkName, txMarkAddress, txMarkCategory, txMarkPriceOn,txMarkPriceOf, txMarkDistance;
        ImageView imgRowMerchant;

        private ViewHolderItemMenu(View itemView){
            super(itemView);

            txMarkName =(TextView) itemView.findViewById(R.id.tx_row_mark_name);
            txMarkAddress =(TextView) itemView.findViewById(R.id.tx_row_mark_address);
            txMarkCategory =(TextView) itemView.findViewById(R.id.tx_row_mark_category);
            txMarkPriceOn =(TextView) itemView.findViewById(R.id.tx_row_mark_price_on);
            txMarkPriceOf=(TextView)itemView.findViewById(R.id.tx_row_mark_price_off);
            txMarkDistance =(TextView) itemView.findViewById(R.id.tx_row_mark_distance);
            imgRowMerchant=(ImageView)itemView.findViewById(R.id.img_row_merchant);





       /*     myBackground = (LinearLayout) itemView.findViewById(R.id.ly_afore);
            imgAfore=(ImageView)itemView.findViewById(R.id.img_afore);
            imgnewselect=(ImageView)itemView.findViewById(R.id.img_afore_select);*/

                itemView.setOnClickListener(this);

       //     myBackground.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onMarkItemSelected(mMarks.get(getAdapterPosition()));
            }
            notifyItemChanged(getAdapterPosition());
            itemSelected = getLayoutPosition();
            notifyItemChanged(getAdapterPosition());
            notifyDataSetChanged();
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolderItemMenu aforeHolder;
      //  setAnimation(holder.itemView, position);

        switch (holder.getItemViewType()){

            case MARK:
                aforeHolder =(ViewHolderItemMenu) holder;
                configureViewHolderItemMenu(aforeHolder,position);
                break;
        }



    }

    public interface MarkItemListener {
        void onMarkItemSelected(Merchants mark);
    }

    public void setListener(MarkItemListener listener) {
        this.listener = listener;
    }



    private void configureViewHolderItemMenu(final ViewHolderItemMenu holder, final int position) {
        Merchants mark=mMarks.get(position);

        if(type==TradeMarkType.ALL){
            holder.txMarkDistance.setVisibility(View.GONE);
            holder.txMarkAddress.setVisibility(View.GONE);
        }else {



        }

        Picasso.with(context).load(mark.getImage_url()).placeholder(context.getResources().getDrawable(R.drawable.ic_mark)).error(context.getResources().getDrawable(R.drawable.ic_mark)).into(holder.imgRowMerchant);
        holder.txMarkName.setText(mark.getName());
        holder.txMarkCategory.setText(mark.getCategory());

        holder.txMarkPriceOn.setTextColor(context.getResources().getColor(R.color.textBlackTittle));
        holder.txMarkPriceOf.setTextColor(context.getResources().getColor(R.color.colorSingMoney));


        int rating;
        try{
            rating =  Integer.parseInt(mark.getBudget_rating()) ;
        }catch (Exception e){
            rating=1;
        }

        switch (rating){
            case 0:
                holder.txMarkPriceOn.setText("");
                holder.txMarkPriceOf.setText("$$$$$");
                break;

            case 1:
                holder.txMarkPriceOn.setText("$");
                holder.txMarkPriceOf.setText("$$$$");
                break;

            case 2:
                holder.txMarkPriceOn.setText("$$");
                holder.txMarkPriceOf.setText("$$$");
                break;

            case 3:
                holder.txMarkPriceOn.setText("$$$");
                holder.txMarkPriceOf.setText("$$");
                break;

            case 4:
                holder.txMarkPriceOn.setText("$$$$");
                holder.txMarkPriceOf.setText("$");
                break;

            case 5:
                holder.txMarkPriceOn.setText("$$$$$");
                holder.txMarkPriceOf.setText("");
                break;
        }


    }


    @Override
    public  int getItemViewType(int position) {

        if(mMarks.get(position) != null){
            return MARK;
        }
        return position;
    }

    @Override
    public int getItemCount() {
        return mMarks.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}










