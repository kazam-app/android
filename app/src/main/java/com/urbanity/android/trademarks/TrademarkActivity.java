package com.urbanity.android.trademarks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseActivity;
import com.urbanity.android.broadcastReceiver.GpsLocationReceiver;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.reward.RewardActivity;
import com.urbanity.android.trademarks.adapter.ViewPagerAdapterTabs;
import com.urbanity.android.trademarks.fragment.FragmentTradeMarks;
import com.urbanity.android.trademarks.markType.TradeMarkType;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by DRM on 24/05/17.
 */

public class TrademarkActivity extends BaseActivity{

    ViewPagerAdapterTabs adapter;
    ViewPager pager;
    TabLayout tabs;
    public static Activity context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trademark);
        setTitleToolbar("Marcas");
        setUpTabs();
        context=this;

        findViewById(R.id.ly_news).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(TrademarkActivity.this, "Marks", Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.ly_perfil).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TrademarkActivity.this,RewardActivity.class));
                TrademarkActivity.this.finish();
            }
        });

        findViewById(R.id.img_main_urban).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeMain();
                TrademarkActivity.this.finish();
            }
        });


    }




    public static void changeMain(){
        if(mListener!=null){
            mListener.changeFragmentEventMain();
        }

    }






    public static ListenerEvent mListener;
    public static void setListener(ListenerEvent listener){
        mListener=listener;
    }
    public  interface ListenerEvent{
        void changeFragmentEventMain();
    }






    void setUpTabs(){

        adapter=new ViewPagerAdapterTabs(getSupportFragmentManager());
        adapter.addFragment(FragmentTradeMarks.getInstance(TradeMarkType.ALL),"Todas");
        adapter.addFragment(FragmentTradeMarks.getInstance(TradeMarkType.CLOSEOFME),"Cerca de mi");


        pager=(ViewPager)findViewById(R.id.pager);
        pager.setAdapter(adapter);
        tabs=(TabLayout)findViewById(R.id.tabs);
        tabs.setupWithViewPager(pager);

    }

    @Override
    protected void onPause() {
        super.onPause();
        MainActivity.canChangeFragment=true;
        mListener = null;
    }
}
