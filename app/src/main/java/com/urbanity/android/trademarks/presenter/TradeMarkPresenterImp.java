package com.urbanity.android.trademarks.presenter;

import android.content.Context;

import com.urbanity.android.model.ItemMark;
import com.urbanity.android.model.MerchantNear;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.trademarks.interactor.TradeMarkInteractoImp;
import com.urbanity.android.trademarks.interactor.TradeMarkInteractor;
import com.urbanity.android.trademarks.viewInterface.TradeMarkFinishListener;
import com.urbanity.android.trademarks.viewInterface.TradeMarkView;
import com.urbanity.android.utils.UrbanDevice;

import java.util.ArrayList;

/**
 * Created by DRM on 24/05/17.
 */

public class TradeMarkPresenterImp implements TradeMarkPresenter, TradeMarkFinishListener {

    TradeMarkView view;
    TradeMarkInteractor interactor;
    public TradeMarkPresenterImp(TradeMarkView view){
        this.view=view;
        this.interactor=new TradeMarkInteractoImp();
    }


    @Override
    public void validateGetTradeMark(Context context) {
        if(UrbanDevice.isConnected(context)){
            view.showProgress();
            interactor.getTradeMarks(context,this);
        }else {
            view.onErrorNetWork();
        }
    }


    @Override
    public void validateGetTradeMark(Context context, double lan, double lng) {
        if(UrbanDevice.isConnected(context)){
            view.showProgress();
            interactor.getTradeMarks(context,lan, lng, this);
        }else {
            view.onErrorNetWork();
        }
    }

    @Override
    public void onError(String error) {
        view.hideProgress();
        view.onError(error);
    }

    @Override
    public void onSuccess(ArrayList<Merchants> marks) {
        view.hideProgress();
        view.onSuccess(marks);
    }

    @Override
    public void onEmptyMerchants() {
        view.onEmptyMerchants();
        view.hideProgress();
    }




    @Override
    public void onSuccessNearMerchants(ArrayList<MerchantNear> nears) {
        view.hideProgress();
        view.onSuccessNear(nears);
    }
}
