package com.urbanity.android.trademarks.interactor;

import android.content.Context;

import com.urbanity.android.trademarks.viewInterface.TradeMarkFinishListener;

/**
 * Created by DRM on 24/05/17.
 */

public interface TradeMarkInteractor {
    void getTradeMarks(Context context, TradeMarkFinishListener listener);
    void getTradeMarks(Context context, double lat, double lng, TradeMarkFinishListener listener);
}
