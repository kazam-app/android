package com.urbanity.android.redeem.interactor;

import android.content.Context;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.model.ItemRedeem;
import com.urbanity.android.model.ItemRedeemResponse;
import com.urbanity.android.model.UrbanResponseError;
import com.urbanity.android.network.ErrorUtils;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.redeem.OnRedeemFinishListener;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.utils.UrbanUtils;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.urbanity.android.utils.UrbanConstants.ERRORLOGOUT;

/**
 * Created by DRM on 15/07/17.
 */

public class RedeemInteractorImp implements RedeemInteractor {


    @Override
    public void sendBeaconRedeem(Context context, String merchant_id, String card_id, IBeaconDevice beacon, final OnRedeemFinishListener listener) {
        Call<ItemRedeem> getPunchCard= RetrofitService.getUrbanService().getPunchBeacon(UrbanDB.getInstance(context).getUrbanUser().getUrbanToken(),merchant_id,card_id,String.valueOf(beacon.getProximityUUID()), String.valueOf(beacon.getMajor()),String.valueOf(beacon.getMinor()));
        getPunchCard.enqueue(new Callback<ItemRedeem>() {
            @Override
            public void onResponse(Call<ItemRedeem> call, Response<ItemRedeem> response) {
                if(response.code()==ERRORLOGOUT){
                    listener.onErrorLogout();
                    return;
                }


                if(response.code()==429){
                    listener.onErrorRedeemDate();
                    return;
                }
               if(response.body()!=null){
                   listener.onSuccess(response.body());
               }else {
                   UrbanResponseError error= ErrorUtils.parseError(response);
                   //No vienen  datos
                   listener.onError(error.getMessage());
               }

            }

            @Override
            public void onFailure(Call<ItemRedeem> call, Throwable t) {
               listener.onError("Ocurrio un error.");
            }
        });
    }

    @Override
    public void sendQRRedeem(Context context, String merchant_id, String card_id, String QRCode, final OnRedeemFinishListener listener) {
        Call<ItemRedeem>getPunchCard=RetrofitService.getUrbanService().getPunchQR(UrbanDB.getInstance(context).getUrbanUser().getUrbanToken(),merchant_id,card_id,QRCode);
        getPunchCard.enqueue(new Callback<ItemRedeem>() {
            @Override
            public void onResponse(Call<ItemRedeem> call, Response<ItemRedeem> response) {

                if(response.code()==ERRORLOGOUT){
                    listener.onErrorLogout();
                    return;
                }


                if(response.code()==429){
                    listener.onErrorRedeemDate();
                    return;
                }

                if(response.body()!=null){
                    listener.onSuccess(response.body());
                }else {
                    UrbanResponseError error= ErrorUtils.parseError(response);
                    //No vienen  datos
                    listener.onError(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ItemRedeem> call, Throwable t) {
                listener.onError("Ocurrio un error.");
            }
        });
    }

    @Override
    public void sendBeaconExchangeRedeem(final Context context, final String merchant_id, final String card_id, IBeaconDevice beacon, final OnRedeemFinishListener listener) {
        Call<ItemRedeemResponse>getExchange=RetrofitService.getUrbanService().postRedeemExchangeBeacon(UrbanDB.getInstance(context).getUrbanUser().getUrbanToken(),merchant_id,card_id,String.valueOf(beacon.getProximityUUID()), String.valueOf(beacon.getMajor()),String.valueOf(beacon.getMinor()));
        getExchange.enqueue(new Callback<ItemRedeemResponse>() {
            @Override
            public void onResponse(Call<ItemRedeemResponse> call, Response<ItemRedeemResponse> response) {

                if(response.code()==ERRORLOGOUT){
                    listener.onErrorLogout();
                    return;
                }

                if(response.body()!=null){


                    UrbanUtils.insertCardRedeemed(context,response.body(),merchant_id);

                    listener.onSuccessExchange(response.body());


                }else {
                    UrbanResponseError error=ErrorUtils.parseError(response);
                    listener.onError(error.getMessage());

                }
            }

            @Override
            public void onFailure(Call<ItemRedeemResponse> call, Throwable t) {
                listener.onError("Ocurrio un error");
            }
        });

    }

    @Override
    public void sendQRExchangeRedeem(final Context context, final String merchant_id, String card_id, String QRCode, final OnRedeemFinishListener listener) {
        Call<ItemRedeemResponse>getExchange=RetrofitService.getUrbanService().postRedeemExchangeQRBeacon(UrbanDB.getInstance(context).getUrbanUser().getUrbanToken(),merchant_id,card_id,QRCode);
        getExchange.enqueue(new Callback<ItemRedeemResponse>() {
            @Override
            public void onResponse(Call<ItemRedeemResponse> call, Response<ItemRedeemResponse> response) {

                if(response.code()==ERRORLOGOUT){
                    listener.onErrorLogout();
                    return;
                }

                if(response.body()!=null){
                    UrbanUtils.insertCardRedeemed(context,response.body(),merchant_id);
                    listener.onSuccessExchange(response.body());
                }else {
                    UrbanResponseError error=ErrorUtils.parseError(response);
                    listener.onError(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ItemRedeemResponse> call, Throwable t) {
                listener.onError("Ocurrio un error");
            }
        });
    }


}
