package com.urbanity.android.redeem.presenter;

import android.content.Context;

import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.urbanity.android.model.ItemRedeem;
import com.urbanity.android.model.ItemRedeemResponse;
import com.urbanity.android.redeem.OnRedeemFinishListener;
import com.urbanity.android.redeem.RedeemView;
import com.urbanity.android.redeem.interactor.RedeemInteractor;
import com.urbanity.android.redeem.interactor.RedeemInteractorImp;
import com.urbanity.android.utils.UrbanDevice;

/**
 * Created by DRM on 15/07/17.
 */

public class RedeemPresenterImp implements RedeemPresenter, OnRedeemFinishListener{

    private RedeemView redeemView;
    private RedeemInteractor interactor;

    public RedeemPresenterImp (RedeemView redeemView){
        this.redeemView=redeemView;
        this.interactor=new RedeemInteractorImp();
    }


    @Override
    public void validateRedeemBeacon(Context context, String merchant_id, String card_id, IBeaconDevice beacon) {
        if(UrbanDevice.isConnected(context)){
            redeemView.ShowProgress();
            interactor.sendBeaconRedeem(context,merchant_id,card_id,beacon,this);
        }else {
            redeemView.onErrorNetwork();
        }
    }

    @Override
    public void validateRedeemQR(Context context, String merchant_id, String card_id, String qrCode) {
        if(UrbanDevice.isConnected(context)){
            redeemView.ShowProgress();
            interactor.sendQRRedeem(context,  merchant_id,  card_id, qrCode,this);
        }else {
            redeemView.onErrorNetwork();
        }
    }


    @Override
    public void onError(String error) {
        redeemView.HideProgress();
        redeemView.onError(error);
    }

    @Override
    public void onSuccess(ItemRedeem item) {
        redeemView.HideProgress();
        redeemView.onSuccess(item);
    }

    @Override
    public void onSuccessExchange(ItemRedeemResponse itemRedeemResponse) {
        redeemView.HideProgress();
        redeemView.onSuccessRedeemExchange(itemRedeemResponse);
    }

    @Override
    public void onErrorRedeemDate() {
        redeemView.HideProgress();
        redeemView.onErrorRedeemDate();
    }

    @Override
    public void onErrorLogout() {
        redeemView.HideProgress();
        redeemView.onErrorLogout();
    }

    //******** EXCHANGE
    @Override
    public void validateRedeemExchangeBeacon(Context context, String merchant_id, String card_id, IBeaconDevice beacon) {
        if(UrbanDevice.isConnected(context)){
            redeemView.ShowProgress();
            interactor.sendBeaconExchangeRedeem(context,  merchant_id,  card_id, beacon,this);
        }else {
            redeemView.onErrorNetwork();
        }
    }

    @Override
    public void validateRedeemExchangeQR(Context context, String merchant_id, String card_id, String qrCode) {
        if(UrbanDevice.isConnected(context)){
            redeemView.ShowProgress();
            interactor.sendQRExchangeRedeem(context,  merchant_id,  card_id, qrCode , this);
        }else {
            redeemView.onErrorNetwork();
        }
    }
}
