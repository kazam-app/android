package com.urbanity.android.redeem.presenter;

import android.content.Context;

import com.kontakt.sdk.android.common.profile.IBeaconDevice;

/**
 * Created by DRM on 15/07/17.
 */

public interface RedeemPresenter {
    void validateRedeemBeacon(Context context, String merchant_id, String card_id, IBeaconDevice beacon);
    void validateRedeemQR(Context context,String merchant_id, String card_id,String qrCode );

    void validateRedeemExchangeBeacon(Context context, String merchant_id, String card_id, IBeaconDevice beacon);
    void validateRedeemExchangeQR(Context context,String merchant_id, String card_id,String qrCode );

}
