package com.urbanity.android.redeem.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.urbanity.android.R;
import com.urbanity.android.UrbanApplication;
import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.customsViews.customDialog.SimpleCustomDialog;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.model.ItemRedeem;
import com.urbanity.android.model.ItemRedeemResponse;
import com.urbanity.android.model.TypeDialog;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.redeem.ActivityRedeemMerchant;
import com.urbanity.android.redeem.RedeemView;
import com.urbanity.android.redeem.TypeRedeem;
import com.urbanity.android.redeem.presenter.RedeemPresenter;
import com.urbanity.android.redeem.presenter.RedeemPresenterImp;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.trademarks.perfil.fragment.CardMarkFragment;
import com.urbanity.android.utils.UrbanUtils;

import java.util.HashMap;

import static com.urbanity.android.analytics.EventsFB.SCREEN_VALIDATION_CAMERA;

/**
 * Created by DRM on 14/07/17.
 */

public class FragmentScanQR extends BaseFragment implements RedeemView {


    private IntentIntegrator qrScan;
    ProgressDialog pDialog;
    RedeemPresenter presenter;
    public static String MERCHANTID="MERCHANTID";
    public static String CARDID="CARDID";
    public static String MERCHANTNAME="MERCHANTNAME";
    public static String MERCHANTCATEGORY="MERCHANTCATEGORY";
    public static String SCREEN_SOURCE= "screen_source";
    TextView tx_scan_qr_error;
    TypeRedeem typeRedeem;
    UrbanUser user;

    public static FragmentScanQR getInstance(String merchant_id, String merchant_name, String card_id, String category,String screen_source , TypeRedeem typeRedeem){
        FragmentScanQR fragment=new FragmentScanQR();
        Bundle arg=new Bundle();
        arg.putString(MERCHANTID,merchant_id);
        arg.putString(CARDID,card_id);
        arg.putString(MERCHANTNAME,merchant_name);
        arg.putString(MERCHANTCATEGORY,category);
        arg.putString(SCREEN_SOURCE,screen_source);
        arg.putSerializable(CardMarkFragment.TYPE_REDEEM,typeRedeem);
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scan_qr, container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //TODO mandar a contants
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_VALIDATION_CAMERA);

        user= UrbanDB.getInstance(getActivity()).getUrbanUser();
        qrScan = new IntentIntegrator(getActivity());
        typeRedeem=(TypeRedeem)getArguments().getSerializable(CardMarkFragment.TYPE_REDEEM);

        pDialog= UrbanUtils.getDialogUrban(getActivity(),null,"Un momento por favor");
        tx_scan_qr_error=(TextView)findViewById(R.id.tx_scan_qr_error);
        presenter=new RedeemPresenterImp(this);
        findViewById(R.id.bt_redeem_scan_qr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tx_scan_qr_error.setVisibility(View.GONE);
                qrScan.initiateScan();

            }
        });
        ActivityRedeemMerchant.setListener(new ActivityRedeemMerchant.OnScanListener() {
            @Override
            public void scanQRListener(String code) {
                switch (typeRedeem){
                    case EXCHANGE:
                        presenter.validateRedeemExchangeQR(getActivity(),getArguments().getString(MERCHANTID), getArguments().getString(CARDID),code);
                        break;

                    case SEAL:
                        presenter.validateRedeemQR(getActivity(),getArguments().getString(MERCHANTID), getArguments().getString(CARDID),code);
                        break;

                }

            }
        });
    }


    @Override
    public void ShowProgress() {
        pDialog.show();
    }

    @Override
    public void HideProgress() {
        pDialog.dismiss();
    }

    @Override
    public void onError(String error) {
        tx_scan_qr_error.setVisibility(View.VISIBLE);
    }
    @Override
    public void onSuccess(ItemRedeem item) {
        FragmentRedeemBluetooth.SETANIMATION = true;

        HashMap<String,String> params=new HashMap<>();

        params.put( EventsFB.PARAM_PUNCHCARD_USER_ID ,user.getUrbanId());
        params.put( EventsFB.PARAM_PUNCHCARD_MERCHANT_NAME,getArguments().getString(MERCHANTNAME));
        params.put( EventsFB.PARAM_PUNCHCARD_MERCHANT_ID,getArguments().getString(MERCHANTID));
        params.put( EventsFB.PARAM_PUNCHCARD_ID ,item.getId());
        params.put( EventsFB.PARAM_VALIDATION_TYPE,"camera");
        params.put( EventsFB.PARAM_PUNCHCARD_CATEGORY,getArguments().getString(MERCHANTCATEGORY) );
        params.put( EventsFB.PARAM_SCREEN_SOURCE,getArguments().getString(SCREEN_SOURCE) );

        FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_PUNCHCARD_PUNCHED,params);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                getActivity().finish();
                if(mListener!=null){
                    mListener.OnRefresh();
                }
            }
        });
    }


    public static OnRefresCardsListener mListener;
    public static void setListenerRefress(OnRefresCardsListener listener){
        mListener=listener;
    }
    public interface OnRefresCardsListener{
        void OnRefresh();
    }



    @Override
    public void onSuccessRedeemExchange(ItemRedeemResponse item) {
        HashMap<String,String> params=new HashMap<>();
        params.put(EventsFB.PARAM_PUNCHCARD_USER_ID,user.getUrbanId());
        params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_NAME,getArguments().getString(MERCHANTNAME));
        params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_ID,getArguments().getString(MERCHANTID));
        params.put(EventsFB.PARAM_PUNCHCARD_ID,getArguments().getString(CARDID));
        params.put(EventsFB.PARAM_VALIDATION_TYPE,"camera");
        params.put(EventsFB.PARAM_PUNCHCARD_CATEGORY ,getArguments().getString(MERCHANTCATEGORY));
        params.put( EventsFB.PARAM_SCREEN_SOURCE,getArguments().getString(SCREEN_SOURCE) );

        FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_PUNCHCARD_REDEEMED,params);

        if(mListener!=null){
            mListener.OnRefresh();
        }
                getActivity().finish();
    }

    @Override
    public void onErrorNetwork() {

    }

    @Override
    public void onErrorRedeemDate() {

        HashMap<String,String> params=new HashMap<>();
        params.put(EventsFB.PARAM_PUNCHCARD_USER_ID,user.getUrbanId());
        params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_NAME,getArguments().getString(MERCHANTNAME));
        params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_ID, getArguments().getString(MERCHANTID));
        params.put(EventsFB.PARAM_PUNCHCARD_ID ,getArguments().getString(CARDID));
        params.put(EventsFB.PARAM_PUNCHCARD_CATEGORY,getArguments().getString(MERCHANTCATEGORY));
        params.put(EventsFB.PARAM_SCREEN_SOURCE,getArguments().getString(SCREEN_SOURCE) );
        FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_PUNCH_LIMIT_REACHED,params);


        SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), "¡Oops! Llegaste al Límite", "Has llegado al límite de sellos que puedes obtener de esta marca hoy, vuelve mañana.", "¡OK!", TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                dialog.dismiss();
                getActivity().onBackPressed();
            }
        });
        dialog.show();
    }

    @Override
    public void onErrorLogout() {
        SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), getResources().getString(R.string.error_session), getResources().getString(R.string.error_session_message), getResources().getString(R.string.buttonAccept), TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                UrbanApplication.getInstance().clearApplicationData();
                UrbanDB.getInstance(getActivity()).CloseSession();
                UrbanPrefs.getInstance(getActivity()).ClearPrefs();
                UrbanPrefs.getInstance(getActivity()).setIsLoged(false);
                UrbanPrefs.getInstance(getActivity()).setRunAppFirst(false);
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();

                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
