package com.urbanity.android.redeem.fragment;


import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.device.BeaconRegion;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory;
import com.kontakt.sdk.android.ble.manager.listeners.EddystoneListener;
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.EddystoneSpaceListener;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;
import com.kontakt.sdk.android.common.profile.IEddystoneDevice;
import com.kontakt.sdk.android.common.profile.IEddystoneNamespace;
import com.urbanity.android.R;
import com.urbanity.android.UrbanApplication;
import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.customsViews.customDialog.SimpleCustomDialog;
import com.urbanity.android.interaction.fragment.FragmentBluetoothDisable;
import com.urbanity.android.interaction.fragment.FragmentConnectionDisable;
import com.urbanity.android.interaction.fragment.FragmentInteractionStates;
import com.urbanity.android.interaction.fragment.FragmentLocationDisable;
import com.urbanity.android.interaction.fragment.FragmentSuccessResponse;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.model.ItemRedeem;
import com.urbanity.android.model.ItemRedeemResponse;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.model.PunchCard;
import com.urbanity.android.model.TypeDialog;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.redeem.ActivityRedeemMerchant;
import com.urbanity.android.redeem.RedeemView;
import com.urbanity.android.redeem.TypeRedeem;
import com.urbanity.android.redeem.presenter.RedeemPresenter;
import com.urbanity.android.redeem.presenter.RedeemPresenterImp;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.trademarks.perfil.fragment.CardMarkFragment;
import com.urbanity.android.utils.UrbanDevice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.urbanity.android.analytics.EventsFB.SCREEN_VALIDATION_BLUETOOTH;
import static com.urbanity.android.analytics.EventsFB.SCREEN_VALIDATION_BLUETOOTH_ERROR;

/**
 * Created by DRM on 14/07/17.
 */

public class FragmentRedeemBluetooth  extends BaseFragment implements  RedeemView{

    private final String TAG = "BeaconReferenceApp";
    boolean isShow=true;
    Merchants merchant;
    PunchCard card;
    RedeemPresenter presenter;
    TypeRedeem typeRedeem;
    private ProximityManager proximityManager;
    public static String SCREENSOUERCE="screen_source";
    UrbanUser user;
    public static boolean SETANIMATION = false;






    public static FragmentRedeemBluetooth getInstance(Merchants merchant, PunchCard card, String screen_source, TypeRedeem typeRedeem){
        FragmentRedeemBluetooth fragment=new FragmentRedeemBluetooth();
        Bundle arg=new Bundle();
        arg.putSerializable(FragmentSuccessResponse.MERCHANT,merchant);
        arg.putSerializable(CardMarkFragment.CARD,card);
        arg.putSerializable(CardMarkFragment.TYPE_REDEEM,typeRedeem);
        arg.putString(SCREENSOUERCE,screen_source);

        fragment.setArguments(arg);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_redeem_bluetooth,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //TODO mandar a contants
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_VALIDATION_BLUETOOTH);

        user= UrbanDB.getInstance(getActivity()).getUrbanUser();
        merchant=(Merchants)getArguments().getSerializable(FragmentSuccessResponse.MERCHANT);
        card=(PunchCard)getArguments().getSerializable(CardMarkFragment.CARD);
        typeRedeem=(TypeRedeem)getArguments().getSerializable(CardMarkFragment.TYPE_REDEEM);
        presenter=new RedeemPresenterImp(this);


        proximityManager = ProximityManagerFactory.create(getActivity());
        proximityManager.configuration()
                .monitoringEnabled(true)
                .monitoringSyncInterval(10)
                .deviceUpdateCallbackInterval(TimeUnit.SECONDS.toMillis(2));




        ArrayList<IBeaconRegion> regions=new ArrayList<>();

        if(FragmentInteractionStates.allRegions!=null){
            if(FragmentInteractionStates.allRegions.size()>0){

                for (int i=0; i<FragmentInteractionStates.allRegions.size(); i++){
                    IBeaconRegion region  = new BeaconRegion.Builder()
                            .identifier("My region")
                            .secureProximity(UUID.fromString(FragmentInteractionStates.allRegions.get(i).getUid()))
                            .build();
                    regions.add(region);
                }

                proximityManager.spaces().iBeaconRegions(regions);
            }
        }

        proximityManager.setIBeaconListener(new IBeaconListener() {
            @Override
            public void onIBeaconDiscovered(IBeaconDevice ibeacon, IBeaconRegion region) {
                Log.e("Sample", "IBeacon DATA" );
                Log.e("Sample", "IBeacon discovered Name: " + ibeacon.getName());
                Log.e("Sample", "IBeacon discovered Major: " + ibeacon.getMajor());
                Log.e("Sample", "IBeacon discovered Minor: " + ibeacon.getMinor());
                Log.e("Sample", "IBeacon discovered Proximity: " + ibeacon.getProximity());
                Log.e("Sample", "IBeacon discovered UniqueId: " + ibeacon.getUniqueId());
                Log.e("Sample", "IBeacon discovered ProximityUUID: " + ibeacon.getProximityUUID());
                Log.e("Sample", "IBeacon discovered Distance: " + ibeacon.getDistance());
            }

            @Override
            public void onIBeaconsUpdated(List<IBeaconDevice> collection, IBeaconRegion region) {
                if (collection.size() > 0) {
                    for (IBeaconDevice beacon : collection){
                        if(beacon.getDistance() <= 0.010){
                            Log.e(TAG, String.valueOf(beacon)+"\nThe first beacon I see is about "+beacon.getDistance()+" meters away.   ID3: "+beacon.getProximityUUID());// + "ID2" +beacon.getId2() + "ID3"+beacon.getId3() );
                            stopBeaconUnBind();
                            showBeacon(beacon);
                        }
                    }
                }


            }

            @Override
            public void onIBeaconLost(IBeaconDevice iBeacon, IBeaconRegion region) {

            }
        });


        proximityManager.setSpaceListener(new EddystoneSpaceListener() {
            @Override
            public void onNamespaceEntered(IEddystoneNamespace namespace) {
                //IBeacon region has been entered
            }

            @Override
            public void onNamespaceAbandoned(IEddystoneNamespace namespace) {
                //IBeacon region has been abandoned
            }
        });



        proximityManager.setEddystoneListener(new EddystoneListener() {
            @Override
            public void onEddystoneDiscovered(IEddystoneDevice eddystone, IEddystoneNamespace namespace) {
                //Profile discovered
            }

            @Override
            public void onEddystonesUpdated(List<IEddystoneDevice> eddystones, IEddystoneNamespace namespace) {
                //Profiles updated
            }

            @Override
            public void onEddystoneLost(IEddystoneDevice eddystone, IEddystoneNamespace namespace) {
                //Profile lost
            }
        });


        ActivityRedeemMerchant.setListenerView(new ActivityRedeemMerchant.OnChangeViewListener() {
            @Override
            public void onChangeView(final boolean state, int view) {
                switch (view){
                    case 1:
                        if(state){
                            findViewById(R.id.content_bluetooth).setVisibility(View.GONE);
                        }else {
                            findViewById(R.id.content_bluetooth).setVisibility(View.VISIBLE);
                            getChildFragmentManager().beginTransaction().replace(R.id.content_bluetooth, FragmentBluetoothDisable.getInstance()).commit();

                        }
                        break;

                    case 2:





                            TimerTask task = new TimerTask() {
                                @Override
                                public void run() {

                                    try{
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if(UrbanDevice.isLocationEnabled(getActivity())){
                                                    findViewById(R.id.content_bluetooth).setVisibility(View.GONE);
                                                    Log.e("Entra2","GONE");
                                                }else {
                                                    Log.e("Entra2","VISIBLE");

                                                    findViewById(R.id.content_bluetooth).setVisibility(View.VISIBLE);
                                                    getChildFragmentManager().beginTransaction().replace(R.id.content_bluetooth, FragmentLocationDisable.getInstance()).commit();
                                                }
                                            }
                                        });
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                }
                            };
                            Timer timer = new Timer();
                            timer.schedule(task, 2000);








                        break;


                    case 3:

                        if(state){
                                try{
                                    findViewById(R.id.content_bluetooth).setVisibility(View.GONE);
                                }catch (Exception e){

                                }
                        }else {
                            findViewById(R.id.content_bluetooth).setVisibility(View.VISIBLE);
                            getChildFragmentManager().beginTransaction().replace(R.id.content_bluetooth, FragmentConnectionDisable.getInstance(0)).commit();

                        }


                        break;
                }

            }
        });


        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null) {
            // Device does not support Bluetooth
            if (!mBluetoothAdapter.isEnabled()) {
                    findViewById(R.id.content_bluetooth).setVisibility(View.VISIBLE);
                    getChildFragmentManager().beginTransaction().replace(R.id.content_bluetooth, FragmentBluetoothDisable.getInstance()).commit();

            }else {
                findViewById(R.id.content_bluetooth).setVisibility(View.GONE);
            }
        }

    }


    @Override
    public void onStart() {
        super.onStart();
        startScanning();
    }

    @Override
    public void onStop() {
        proximityManager.stopScanning();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        proximityManager.disconnect();
        proximityManager = null;
        super.onDestroy();
        isShow=false;
    }

    private void startScanning() {
        proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                if(proximityManager!=null){
                    if(proximityManager.isConnected()){
                        try{
                            proximityManager.startScanning();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }

            }
        });
    }



    @Override
    public void onResume() {
        super.onResume();
        isShow=true;

    }

    @Override
    public void onPause() {
        super.onPause();
        isShow=false;
        stopBeaconUnBind();
    }

    public void stopBeaconUnBind(){
        if(proximityManager!=null){
            proximityManager.stopScanning();
        }
    }


    public void showBeacon(final IBeaconDevice beacon){
        if(isShow){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    switch (typeRedeem){
                        case EXCHANGE:
                            presenter.validateRedeemExchangeBeacon(getActivity(),merchant.getId(),card.getId(),beacon);
                            break;

                        case SEAL:
                            presenter.validateRedeemBeacon(getActivity(),merchant.getId(),card.getId(),beacon);
                            break;

                    }

                }

            });
        }
    }

    @Override
    public void ShowProgress() {
        Log.e("Progress","Show");
    }

    @Override
    public void HideProgress() {
        Log.e("Progress","Dismiss");
    }

    @Override
    public void onError(String error) {
        //TODO mandar a contants
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_VALIDATION_BLUETOOTH_ERROR);
    }




    @Override
    public void onSuccess(ItemRedeem item) {

        SETANIMATION = true;
        
        HashMap<String,String> params=new HashMap<>();
        params.put(EventsFB.PARAM_PUNCHCARD_USER_ID ,user.getUrbanId());
        params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_NAME ,merchant.getName());
        params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_ID ,merchant.getId());
        params.put(EventsFB.PARAM_PUNCHCARD_ID ,card.getId());
        params.put(EventsFB.PARAM_VALIDATION_TYPE,"bluetooth");
        params.put(EventsFB.PARAM_PUNCHCARD_CATEGORY,merchant.getCategory());
        params.put(EventsFB.PARAM_SCREEN_SOURCE,getArguments().getString(SCREENSOUERCE));
        FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_PUNCHCARD_PUNCHED,params);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getActivity().finish();
                if(mListener!=null){
                    mListener.ClearAdapter();
                }
            }
        });
    }


    public static OnClearAdapterListener mListener;
    public static void setListener(OnClearAdapterListener listener){
        mListener=listener;
    }
    public interface OnClearAdapterListener{
        void ClearAdapter();
    }

    @Override
    public void onSuccessRedeemExchange(ItemRedeemResponse itemRedeemResponse) {


        HashMap<String,String> params=new HashMap<>();
        params.put(EventsFB.PARAM_PUNCHCARD_USER_ID ,user.getUrbanId());
        params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_NAME ,merchant.getName());
        params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_ID ,merchant.getId());
        params.put(EventsFB.PARAM_PUNCHCARD_ID ,card.getId());
        params.put(EventsFB.PARAM_VALIDATION_TYPE,"bluetooth");
        params.put(EventsFB.PARAM_PUNCHCARD_CATEGORY,merchant.getCategory());
        params.put(EventsFB.PARAM_SCREEN_SOURCE,getArguments().getString(SCREENSOUERCE));

        FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_PUNCHCARD_REDEEMED,params);


        getActivity().finish();
        if(mListener!=null){
            mListener.ClearAdapter();
        }
    }

    @Override
    public void onErrorNetwork() {

    }

    @Override
    public void onErrorRedeemDate() {

        HashMap<String,String> params=new HashMap<>();
        params.put(EventsFB.PARAM_PUNCHCARD_USER_ID,UrbanDB.getInstance(getActivity()).getUrbanUser().getUrbanId());
        params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_NAME,merchant.getName());
        params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_ID, merchant.getId());
        params.put(EventsFB.PARAM_PUNCHCARD_ID , card.getId());
        params.put(EventsFB.PARAM_PUNCHCARD_CATEGORY,merchant.getCategory());
        params.put(EventsFB.PARAM_SCREEN_SOURCE,getArguments().getString(SCREENSOUERCE) );

        FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_PUNCH_LIMIT_REACHED,params);


        SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), "¡Oops! Llegaste al Límite", "Has llegado al límite de sellos que puedes obtener de esta marca hoy, vuelve mañana.", "¡OK!", TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                dialog.dismiss();
                getActivity().onBackPressed();
            }
        });
        dialog.show();

    }

    @Override
    public void onErrorLogout() {
        SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), getResources().getString(R.string.error_session), getResources().getString(R.string.error_session_message), getResources().getString(R.string.buttonAccept), TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                UrbanApplication.getInstance().clearApplicationData();
                UrbanDB.getInstance(getActivity()).CloseSession();
                UrbanPrefs.getInstance(getActivity()).ClearPrefs();
                UrbanPrefs.getInstance(getActivity()).setIsLoged(false);
                UrbanPrefs.getInstance(getActivity()).setRunAppFirst(false);
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();

                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
