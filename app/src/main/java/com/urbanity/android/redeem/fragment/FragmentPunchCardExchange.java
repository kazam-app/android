package com.urbanity.android.redeem.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.urbanity.android.R;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.model.ItemRedeemResponse;
import com.urbanity.android.rulesReward.ActivityRulesReward;
import com.urbanity.android.storage.UrbanDB;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by DRM on 16/07/17.
 */

public class FragmentPunchCardExchange extends BaseFragment {

    public static String ITEM="ITEM";
    TextView tx_rules_reward, tx_reward_code, tx_terms_reward, txTimer;
    ItemRedeemResponse item;
    CountDownTimer timer;
    long milliseconds=0;
    boolean runTimer=true;

    public static FragmentPunchCardExchange getInstance(ItemRedeemResponse itemRedeemResponse){
        FragmentPunchCardExchange fragment=new FragmentPunchCardExchange();
        Bundle arg=new Bundle();
        arg.putSerializable(ITEM,itemRedeemResponse);
        fragment.setArguments(arg);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_punch_card_exchange,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        item=(ItemRedeemResponse)getArguments().getSerializable(ITEM);
        tx_rules_reward=(TextView)findViewById(R.id.tx_rules_reward);
        tx_rules_reward.setText(String.valueOf("Premio: "+item.getPrize()));
        tx_reward_code=(TextView)findViewById(R.id.tx_reward_code);
        tx_reward_code.setText(String.valueOf(item.getRedeem_code()));
        tx_terms_reward=(TextView)findViewById(R.id.tx_terms_reward);
        txTimer=(TextView)findViewById(R.id.tx_CountDownTimer);
        tx_terms_reward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),ActivityRulesReward.class).putExtra("date",item.getExpires_at()).putExtra("rules",item.getRules()).putExtra("term",item.getTerms()));
            }
        });

        initTimer();
    }




    public void initTimer(){
        try{
            item=(ItemRedeemResponse)getArguments().getSerializable(ITEM);
            long date1=Long.parseLong( UrbanDB.getInstance(getActivity()).getUrbanCard(item.getIdCard()).getStartTime());
            long date2=Long.parseLong( UrbanDB.getInstance(getActivity()).getUrbanCard(item.getIdCard()).getEndTime());

            long diff = date2 - date1;
            long diffMinutes = diff / (60 * 1000) % 60;

            stopTimer();
            timer= new CountDownTimer(diff, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    milliseconds= millisUntilFinished;
                    txTimer.setText("");
                    runTimer = true;
                    txTimer.setText(String.valueOf("Esta tarjeta desaparecera en: \n"+
                            String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                            ))));
                }

                @Override
                public void onFinish() {
                    runTimer = false;
                    txTimer.setText(String.valueOf("El tiempo ha expirado"));
                    UrbanDB.getInstance(getActivity()).DeleteCard(item.getIdCard());
                    if(mListener!=null){
                        mListener.DeleteCard(FragmentPunchCardExchange.this);
                    }

                }
            }.start();
        }catch (Exception e){
                e.printStackTrace();
        }
    }



    @Override
    public void onStart() {
        super.onStart();
    }


    static OnDeleteCardListener mListener;
    public static void setListener(OnDeleteCardListener listener){
        mListener=listener;
    }
    public interface OnDeleteCardListener{
        void DeleteCard(Fragment fragment);
    }




   @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            onResume();
        }else {
            stopTimer();
        }
    }

    public void stopTimer(){
        if(timer!=null){
            timer.cancel();
            timer=null;
            UrbanDB.getInstance(getActivity()).UpdateTimeInCard(item.getIdCard(),String.valueOf(new Date().getTime()));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!runTimer){
            UrbanDB.getInstance(getActivity()).DeleteCard(item.getIdCard());
            if(mListener!=null){
                mListener.DeleteCard(FragmentPunchCardExchange.this);
            }
        }
        initTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopTimer();
    }



}
