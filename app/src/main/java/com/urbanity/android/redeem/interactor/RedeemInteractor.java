package com.urbanity.android.redeem.interactor;

import android.content.Context;

import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.urbanity.android.redeem.OnRedeemFinishListener;
/**
 * Created by DRM on 15/07/17.
 */

public interface RedeemInteractor {

    void sendBeaconRedeem(Context context, String merchant_id, String card_id, IBeaconDevice beacon, OnRedeemFinishListener listener);
    void sendQRRedeem(Context context,String merchant_id, String card_id, String QRCode, OnRedeemFinishListener listener);

    void sendBeaconExchangeRedeem(Context context,String merchant_id, String card_id, IBeaconDevice beacon, OnRedeemFinishListener listener);
    void sendQRExchangeRedeem(Context context,String merchant_id, String card_id, String QRCode, OnRedeemFinishListener listener);


}
