package com.urbanity.android.redeem;

import com.urbanity.android.model.ItemRedeem;
import com.urbanity.android.model.ItemRedeemResponse;

/**
 * Created by DRM on 15/07/17.
 */

public interface OnRedeemFinishListener {

    void onError(String error);
    void onSuccess(ItemRedeem item);
    void onSuccessExchange(ItemRedeemResponse itemRedeemResponse);
    void onErrorRedeemDate();


    void onErrorLogout();
}
