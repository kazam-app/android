package com.urbanity.android.redeem;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;
import com.urbanity.android.R;
import com.urbanity.android.UrbanApplication;
import com.urbanity.android.bases.BaseActivity;
import com.urbanity.android.broadcastReceiver.GpsLocationReceiver;
import com.urbanity.android.broadcastReceiver.NetworkChangeReceiver;
import com.urbanity.android.interaction.fragment.FragmentBluetoothDisable;
import com.urbanity.android.interaction.fragment.FragmentConnectionDisable;
import com.urbanity.android.interaction.fragment.FragmentLocationDisable;
import com.urbanity.android.interaction.fragment.FragmentSuccessResponse;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.model.PunchCard;
import com.urbanity.android.redeem.fragment.FragmentRedeemBluetooth;
import com.urbanity.android.redeem.fragment.FragmentScanQR;
import com.urbanity.android.trademarks.adapter.ViewPagerAdapterTabs;
import com.urbanity.android.trademarks.perfil.fragment.CardMarkFragment;
import com.urbanity.android.utils.UrbanDevice;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by DRM on 14/07/17.
 */

public class ActivityRedeemMerchant extends BaseActivity {

    ViewPagerAdapterTabs adapter;
    ViewPager pager;
    TabLayout tabs;


    PunchCard card;
    TextView txMarkName, txMarkCategory, tx_mark_price_on,tx_mark_price_off ;
    ImageView img_perfil_mark;
    Merchants merchant;
    TypeRedeem typeRedeem;
    String screen_source;

    private boolean canChange=false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_merchant);
        setTitleToolbar("");
        txMarkName=(TextView)findViewById(R.id.tx_mark_name);
        txMarkCategory=(TextView)findViewById(R.id.tx_mark_category);
        img_perfil_mark=(ImageView)findViewById(R.id.img_perfil_mark);
        tx_mark_price_on=(TextView)findViewById(R.id.tx_mark_price_on);
        tx_mark_price_off=(TextView)findViewById(R.id.tx_mark_price_off);

        adapter=new ViewPagerAdapterTabs(getSupportFragmentManager());
        pager=(ViewPager)findViewById(R.id.pager);
        tabs=(TabLayout)findViewById(R.id.tabs);
        pager.setOffscreenPageLimit(2);




        Bundle arg=getIntent().getExtras();
        if(arg!=null){
            card= (PunchCard) arg.getSerializable(CardMarkFragment.CARD);
            merchant=(Merchants)arg.getSerializable(FragmentSuccessResponse.MERCHANT);
            typeRedeem=(TypeRedeem)arg.getSerializable(CardMarkFragment.TYPE_REDEEM);

            screen_source=arg.getString("screen_source");

            txMarkName.setText(merchant.getName());
            txMarkCategory.setText(merchant.getCategory());

            int rating;
            try{
               rating = Integer.parseInt(merchant.getBudget_rating());
            }catch (Exception e){
                rating=3;
            }

            switch (rating){
                case 1:
                    tx_mark_price_on.setText("$");
                    tx_mark_price_off.setText("$$$$");
                    break;


                case 2:
                    tx_mark_price_on.setText("$$");
                    tx_mark_price_off.setText("$$$");
                    break;

                case 3:
                    tx_mark_price_on.setText("$$$");
                    tx_mark_price_off.setText("$$");
                    break;


                case 4:
                    tx_mark_price_on.setText("$$$$");
                    tx_mark_price_off.setText("$");
                    break;

                case 5:
                    tx_mark_price_on.setText("$$$$$");
                    tx_mark_price_off.setText("");
                    break;

                default:
                    tx_mark_price_on.setText("");
                    tx_mark_price_off.setText("$$$$$");
                    break;
            }

            Picasso.with(this).load(merchant.getImage_url()).placeholder(getResources().getDrawable(R.drawable.ic_mark)).error(getResources().getDrawable(R.drawable.ic_mark)).into(img_perfil_mark);



            if(merchant.getColor()!=null && merchant.getColor().length()>5 && !merchant.getColor().equals("null")){
                Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
                toolbar.setBackgroundColor(Color.parseColor(merchant.getColor()));
                LinearLayout content_detail_merchant =(LinearLayout)findViewById(R.id.content_detail_merchant);
                content_detail_merchant.setBackgroundColor(Color.parseColor(merchant.getColor()));
                tabs.setBackgroundColor(Color.parseColor(merchant.getColor()));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    getWindow().setStatusBarColor(Color.parseColor(merchant.getColor()));
                }
            }
        }
        setUpTabs();
    }


    public static OnChangeViewListener vListener;
    public static void setListenerView(OnChangeViewListener listener){
        vListener=listener;
    }

    public interface OnChangeViewListener{
        void onChangeView(boolean state, int view);
    }


    void setUpTabs(){
        adapter.notifyDataSetChanged();
        adapter.addFragment(FragmentRedeemBluetooth.getInstance(merchant,card,screen_source,typeRedeem),"Bluetooth");
        adapter.addFragment(FragmentScanQR.getInstance(merchant.getId(),merchant.getName(), card.getId(),merchant.getCategory(),screen_source,typeRedeem),"Escanear Código");
        pager.setAdapter(adapter);
        tabs.setupWithViewPager(pager);
        adapter.notifyDataSetChanged();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Instance of FragmentInteractionStates
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "No se encontraron resultados", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data
                try {
                    //converting the data to json
                    //   JSONObject obj = new JSONObject(result.getContents());
                    //setting values to textviews
                    //  Toast.makeText(this, String.valueOf(obj.getString("name")), Toast.LENGTH_LONG).show();
                    if(mListener!=null) mListener.scanQRListener(result.getContents());

                } catch (Exception e) {
                    e.printStackTrace();
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public static OnScanListener mListener;
    public static void setListener(OnScanListener listener){
        mListener=listener;
    }
    public interface OnScanListener{
        void scanQRListener(String code);
    }
    @Override
    protected void onPause() {
        super.onPause();
        canChange=false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        canChange=true;
        updateScreen();
    }


    public void updateScreen(){
        UrbanApplication.setNetworkListener(new UrbanApplication.OnBluetoothListener() {
            @Override
            public void onBluetoothEnabled(boolean state) {
                if(canChange){
                    if(vListener!=null){
                        vListener.onChangeView(state,1);
                    }

                }

            }
        });

        GpsLocationReceiver.setNetworkListener(new GpsLocationReceiver.OnLocationListener() {
            @Override
            public void onLocationEnabled(final boolean state) {
                if(canChange){
                    if(vListener!=null){
                        vListener.onChangeView(state,2);
                    }
                }

            }
        });

        NetworkChangeReceiver.setNetworkListener(new NetworkChangeReceiver.OnNetworkListener() {
            @Override
            public void onNetWorkConnected(final boolean state) {
                if(vListener!=null){
                    vListener.onChangeView(state,3);
                }

            }
        });
    }
}
