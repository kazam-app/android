package com.urbanity.android.redeem;

import com.urbanity.android.model.ItemRedeem;
import com.urbanity.android.model.ItemRedeemResponse;

/**
 * Created by DRM on 15/07/17.
 */

public interface RedeemView {

    void ShowProgress();
    void HideProgress();

    void onError(String error);
    void onSuccess(ItemRedeem item);
    void onSuccessRedeemExchange(ItemRedeemResponse itemRedeemResponse);
    void onErrorNetwork();

    void onErrorRedeemDate();
    void onErrorLogout();


}
