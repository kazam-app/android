package com.urbanity.android.legal;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseActivity;
import com.urbanity.android.legal.adapter.AdapterLegal;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.user.perfil.model.UserSettingsType;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.urbanity.android.analytics.EventsFB.SCREEN_SETTINGS_FAQ;
import static com.urbanity.android.analytics.EventsFB.SCREEN_SETTINGS_PP;
import static com.urbanity.android.analytics.EventsFB.SCREEN_SETTINGS_TOS;

/**
 * Created by DRM on 19/07/17.
 */

public class ActivityLegalFAQ extends BaseActivity {

    RecyclerView rv_legal_faqs;
    UserSettingsType type;
    Call<String[]> getTerms;
    AdapterLegal adapter;
    public static String TERM="TERMS";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal);




        rv_legal_faqs=(RecyclerView)findViewById(R.id.rv_legal_faqs);
        rv_legal_faqs.setLayoutManager(new LinearLayoutManager(this));

        final Bundle arg=getIntent().getExtras();
        if(arg!=null){

            type=(UserSettingsType)arg.getSerializable(TERM);
            switch (type){
                case TERMS:
                    setTitleToolbar("Terminos y Condiciones");
                    //TODO mandar a constants
                    FacebookAnalytics. FireBaseScreenTracking(this,SCREEN_SETTINGS_TOS);
                    getTerms  = RetrofitService.getUrbanService().getLegalTerms("terms");
                    break;

                case FAQS:
                    setTitleToolbar("Preguntas Frecuentes");
                    //TODO mandar a constants
                    FacebookAnalytics. FireBaseScreenTracking(this,SCREEN_SETTINGS_FAQ);
                    getTerms  = RetrofitService.getUrbanService().getLegalTerms("faq");
                    break;

                case PRIVS:
                    setTitleToolbar("Políticas de Privacidad");
                    //TODO mandar a constants
                    FacebookAnalytics. FireBaseScreenTracking(this,SCREEN_SETTINGS_PP);
                    getTerms  = RetrofitService.getUrbanService().getLegalTerms("policy");
                    break;
            }


            getTerms.enqueue(new Callback<String[]>() {
                @Override
                public void onResponse(Call<String[]> call, Response<String[]> response) {
                   if(response.body()!=null)
                   {
                       adapter=new AdapterLegal(response.body());
                       rv_legal_faqs.setAdapter(adapter);
                       adapter.notifyDataSetChanged();
                   }
                }

                @Override
                public void onFailure(Call<String[]> call, Throwable t) {
                    Log.e("TERMS",t.getMessage()+"");
                }
            });
        }


    }


}
