package com.urbanity.android.legal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.urbanity.android.R;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.trademarks.markType.TradeMarkType;

/**
 * Created by lenovo on 16/05/2017.
 */

public class AdapterLegal extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final int TERM =0;
    private String [] terms;


    public AdapterLegal(String [] terms){
        this.terms=terms;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View headerView;
        switch (viewType){

            case TERM:
                headerView=inflater.inflate(R.layout.row_legal,viewGroup,false);
                viewHolder=new ViewHolderItemMenu(headerView);
                break;

        }

        return viewHolder;
    }





    private class ViewHolderItemMenu extends RecyclerView.ViewHolder {
        TextView tx_term;

        private ViewHolderItemMenu(View itemView){
            super(itemView);

            tx_term =(TextView) itemView.findViewById(R.id.tx_term);

        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolderItemMenu aforeHolder;
      //  setAnimation(holder.itemView, position);

        switch (holder.getItemViewType()){

            case TERM:
                aforeHolder =(ViewHolderItemMenu) holder;
                configureViewHolderItemMenu(aforeHolder,position);
                break;
        }



    }

    private void configureViewHolderItemMenu(final ViewHolderItemMenu holder, final int position) {
        String term =terms[position];
        holder.tx_term.setText(term);
    }


    @Override
    public  int getItemViewType(int position) {

        if(terms[position] != null){
            return TERM;
        }
        return position;
    }

    @Override
    public int getItemCount() {
        return terms.length;
    }
}










