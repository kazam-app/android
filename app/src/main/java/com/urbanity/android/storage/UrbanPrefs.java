package com.urbanity.android.storage;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by DRM on 04/06/17.
 */

public class UrbanPrefs {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context mContext;
    // shared pref mode
    private final int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "URBANPREFS";
    private static UrbanPrefs _instance;

    public static synchronized UrbanPrefs getInstance(Context context) {
        if (_instance == null) {
            _instance = new UrbanPrefs(context);
        }
        return _instance;
    }
    private UrbanPrefs(Context context) {
        this.mContext = context;
        pref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    //******RunApp
    private static final String FIRSTRUN = "FIRSTRUN";
    public void setRunAppFirst(boolean firstRun){
        editor.putBoolean(FIRSTRUN, firstRun);
        editor.commit();
    }

    public  boolean isRunAppFirst(){
        return pref.getBoolean(FIRSTRUN,false);
    }




    //*****IsLoged

    private static final String ISLOGED = "ISLOGED";
    public void setIsLoged(boolean isLoged){
        editor.putBoolean(ISLOGED, isLoged);
        editor.commit();
    }

    public  boolean isLoged(){
        return pref.getBoolean(ISLOGED,false);
    }




    //*****IsLoged

    private static final String ISUPDATE = "ISUPDATE";
    public void setIsUpdate(boolean isUpdate){
        editor.putBoolean(ISUPDATE, isUpdate);
        editor.commit();
    }
    public  boolean isUpdate(){
        return pref.getBoolean(ISUPDATE,false);
    }


    private static final String NUMVERSION = "NUMVERSION";
    public void setNumVersion(String numVersion){
        editor.putString(NUMVERSION, numVersion);
        editor.commit();
    }
    public  String getNumVersion(){
        return pref.getString(NUMVERSION,"");
    }






    //*****CreatedAt

    private static final String CREATEDAT = "CREATEDAT";
    public void setCreatedAt(String createdAt){
        editor.putString(CREATEDAT, createdAt);
        editor.commit();
    }

    public  String getCreatedAt(){

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            Date date = formatter.parse(pref.getString(CREATEDAT,""));
            // Log.e("DATE Format", ""+formatter.format(date) ); //2017-08-15
            SimpleDateFormat formateador = new SimpleDateFormat("MMM yyyy", Locale.getDefault());
            return  formateador.format(date);


        } catch (ParseException e) {
            e.printStackTrace();
        }



        return "" ;
    }




    public  void ClearPrefs(){
        editor.clear().commit();
    }

}
