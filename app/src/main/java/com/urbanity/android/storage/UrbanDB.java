package com.urbanity.android.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.urbanity.android.model.ItemRedeemResponse;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.utils.UrbanUtils;

import java.util.ArrayList;

/**
 * Created by DRM on 06/06/17.
 */

public class UrbanDB extends SQLiteOpenHelper {


    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "URBAN_DB";
    private static final String TAG = "UrbanDB.class";
    public Context context;
    private static UrbanDB mInstance;


  
    //Create table USER

    private static final String TABLE_USER = "URBAN_USER";

    private static final String USER_URBAN_ID = "ID"; //GUEST , USER
    private static final String USER_NAME = "NAME";
    private static final String USER_LASTNAME = "USER_LASTNAME";
    private static final String USER_EMAIL = "USER_EMAIL";
    private static final String USER_GENDER = "USER_GENDER";
    private static final String USER_BIRTH_DATE = "USER_BIRTH_DATE";
    private static final String USER_URBAN_TOKEN = "USER_URBAN_TOKEN";
    private static final String USER_FB_ID = "USER_FB_ID";
    private static final String USER_FB_TOKEN = "USER_FB_TOKEN";
    private static final String USER_FB_EXPIRES_AT = "USER_FB_EXPIRES_AT";



    //Create table CARD REDEEM

    private static final String TABLE_CARD = "URBAN_CARD";

    private static final String CARD_ID = "CARD_ID"; //CARD_ID
    private static final String CARD_MERCHANT_ID = "CARD_MERCHANT_ID"; //CARD_ID
    private static final String CARD_IDENTITY = "CARD_IDENTITY";
    private static final String CARD_NAME = "CARD_NAME";
    private static final String CARD_PRIZE = "CARD_PRIZE";
    private static final String CARD_RULES = "CARD_RULES";
    private static final String CARD_TERMS = "CARD_TERMS";
    private static final String CARD_REDEEM_CODE = "CARD_REDEEM_CODE";
    private static final String CARD_EXPIRE = "CARD_EXPIRE";
    private static final String CARD_START_TIME = "CARD_START_TIME";
    private static final String CARD_END_TIME = "CARD_END_TIME";
    


    public static synchronized UrbanDB getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (mInstance == null) {
            mInstance = new UrbanDB(context.getApplicationContext());
        }
        return mInstance;
    }

    public UrbanDB(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    public void createTables(SQLiteDatabase db) {

        createTablesUrban(db);
        
    }

   

    public void createTablesUrban(SQLiteDatabase db){

        String SQL_CREATE_USER = "CREATE TABLE IF NOT EXISTS " + UrbanDB.TABLE_USER + "("
                + UrbanDB.USER_URBAN_ID + " TEXT PRIMARY KEY, "
                + UrbanDB.USER_NAME + " TEXT, "
                + UrbanDB.USER_LASTNAME + " TEXT, "
                + UrbanDB.USER_EMAIL + " TEXT, "
                + UrbanDB.USER_GENDER + " TEXT, "
                + UrbanDB.USER_BIRTH_DATE + " TEXT,"
                + UrbanDB.USER_URBAN_TOKEN + " TEXT, "
                + UrbanDB.USER_FB_ID + " TEXT, "
                + UrbanDB.USER_FB_TOKEN + " TEXT, "
                + UrbanDB.USER_FB_EXPIRES_AT + " TEXT );";


        String SQL_CREATE_CARD = "CREATE TABLE IF NOT EXISTS " + UrbanDB.TABLE_CARD + "("
                + UrbanDB.CARD_ID + " TEXT PRIMARY KEY, "
                + UrbanDB.CARD_MERCHANT_ID + " TEXT, "
                + UrbanDB.CARD_IDENTITY + " TEXT, "
                + UrbanDB.CARD_NAME + " TEXT, "
                + UrbanDB.CARD_PRIZE + " TEXT, "
                + UrbanDB.CARD_RULES + " TEXT,"
                + UrbanDB.CARD_TERMS + " TEXT, "
                + UrbanDB.CARD_REDEEM_CODE + " TEXT, "
                + UrbanDB.CARD_EXPIRE + " TEXT, "
                + UrbanDB.CARD_START_TIME + " TEXT, "
                + UrbanDB.CARD_END_TIME + " TEXT );";




        db.execSQL(SQL_CREATE_USER);
        db.execSQL(SQL_CREATE_CARD);
    }


    public void addUrbanUser(UrbanUser user) {

        SQLiteDatabase db = getWritableDatabase();
        if (user != null) {
            UrbanPrefs.getInstance(context).setCreatedAt(user.getCreated_at());
            String SQL_INSERT_USER = "INSERT OR REPLACE INTO " + UrbanDB.TABLE_USER + "("
                    + UrbanDB.USER_URBAN_ID + ", "
                    + UrbanDB.USER_NAME + ", "
                    + UrbanDB.USER_LASTNAME + ", "
                    + UrbanDB.USER_EMAIL + ","
                    + UrbanDB.USER_GENDER + ", "
                    + UrbanDB.USER_BIRTH_DATE + ", "
                    + UrbanDB.USER_URBAN_TOKEN + ", "
                    + UrbanDB.USER_FB_ID + ", "
                    + UrbanDB.USER_FB_TOKEN + ","
                    + UrbanDB.USER_FB_EXPIRES_AT
                    + ")  VALUES('"
                    + user.getUrbanId()
                    + "','" + user.getName()
                    + "','" + user.getLast_names()
                    + "','" + user.getEmail()
                    + "','" + user.getGender()
                    + "','" + UrbanUtils.getDate(user.getBirthdate())
                    + "','" +user.getUrbanToken()
                    + "','" + user.getFb_id()
                    + "','" + user.getFb_token()
                    + "','" + user.getFb_expires_at()
                    + "')";
            Log.e("UrbanDB",SQL_INSERT_USER);
            if (db != null) {
                try {
                    if (db.isOpen()) {
                        db.execSQL(SQL_INSERT_USER);
                        db.close();
                    }
                } catch (SQLiteException e) {
                    e.printStackTrace();
                }
            }
        }
    }



    public UrbanUser getUrbanUser() {
        SQLiteDatabase db = getReadableDatabase();
        UrbanUser user = null;

        String SQL_QUERY = "SELECT * FROM " + UrbanDB.TABLE_USER;
        Cursor c;

        if (db != null) {
            c = db.rawQuery(SQL_QUERY, null);
            if (c.moveToFirst()) {
                do {
                    user = new UrbanUser();
                    user.setUrbanId(c.getString(c.getColumnIndex(UrbanDB.USER_URBAN_ID)));
                    user.setName(c.getString(c.getColumnIndex(UrbanDB.USER_NAME)));
                    user.setLast_names(c.getString(c.getColumnIndex(UrbanDB.USER_LASTNAME)));
                    user.setEmail(c.getString(c.getColumnIndex(UrbanDB.USER_EMAIL)));
                    user.setGender(c.getString(c.getColumnIndex(UrbanDB.USER_GENDER)));
                    user.setBirthdate(c.getString(c.getColumnIndex(UrbanDB.USER_BIRTH_DATE)));
                    user.setUrbanToken(String.valueOf("Bearer "+  c.getString(c.getColumnIndex(UrbanDB.USER_URBAN_TOKEN))));
                    user.setFb_id(c.getString(c.getColumnIndex(UrbanDB.USER_FB_ID)));
                    user.setFb_token(c.getString(c.getColumnIndex(UrbanDB.USER_FB_TOKEN)));
                    user.setFb_expires_at(c.getString(c.getColumnIndex(UrbanDB.USER_FB_EXPIRES_AT)));


                } while (c.moveToNext());
                db.close();
            }
        }
        return user;
    }


    public void UpdateUser(UrbanUser user){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(USER_NAME,user.getName()); //These Fields should be your String values of actual column names
        cv.put(USER_LASTNAME,user.getLast_names());
        cv.put(USER_BIRTH_DATE, UrbanUtils.getDate(user.getBirthdate()));
        cv.put(USER_GENDER,user.getGender());
        db.update(UrbanDB.TABLE_USER, cv, USER_URBAN_ID+"="+user.getUrbanId(), null);
        db.close();

    }



    //*****CARD

    public void addUrbanCardRedeemed(String idCard, ItemRedeemResponse card,String merchant_id,  String startTime, String endTime) {
        SQLiteDatabase db = getWritableDatabase();
        if (card != null) {
            String SQL_INSERT_CARD = "INSERT OR REPLACE INTO " + UrbanDB.TABLE_CARD + "("
                    + UrbanDB.CARD_ID + ", "
                    + UrbanDB.CARD_MERCHANT_ID + ", "
                    + UrbanDB.CARD_IDENTITY + ", "
                    + UrbanDB.CARD_NAME + ","
                    + UrbanDB.CARD_PRIZE + ", "
                    + UrbanDB.CARD_RULES + ", "
                    + UrbanDB.CARD_TERMS + ", "
                    + UrbanDB.CARD_REDEEM_CODE + ", "
                    + UrbanDB.CARD_EXPIRE + ", "
                    + UrbanDB.CARD_START_TIME + ", "
                    + UrbanDB.CARD_END_TIME
                    + ")  VALUES('"
                    + idCard.trim()
                    + "','" + merchant_id
                    + "','" + card.getIdentity()
                    + "','" + card.getName()
                    + "','" + card.getPrize()
                    + "','" + card.getRules()
                    + "','" +card.getTerms()
                    + "','" + card.getRedeem_code()
                    + "','" + card.getExpires_at()
                    + "','" + startTime
                    + "','" + endTime
                    + "')";
            Log.e("UrbanDB",SQL_INSERT_CARD);
            if (db != null) {
                try {
                    if (db.isOpen()) {
                        db.execSQL(SQL_INSERT_CARD);
                        db.close();
                    }
                } catch (SQLiteException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public ArrayList<ItemRedeemResponse> getUrbanCards(String merchant_id) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<ItemRedeemResponse> cards=new ArrayList<>();
        ItemRedeemResponse card;
        String SQL_QUERY = "SELECT * FROM " + UrbanDB.TABLE_CARD+ " WHERE "+UrbanDB.CARD_MERCHANT_ID +" = '"+ merchant_id +"'";
        Cursor c;
        Log.e("UrbanDB",SQL_QUERY);
        if (db != null) {
            c = db.rawQuery(SQL_QUERY, null);
            if (c.moveToFirst()) {
                do {
                    card = new ItemRedeemResponse();

                    card.setIdCard(c.getString(c.getColumnIndex(UrbanDB.CARD_ID)));
                    card.setMerchant_id(c.getString(c.getColumnIndex(UrbanDB.CARD_MERCHANT_ID)));
                    card.setIdentity(Integer.parseInt(c.getString(c.getColumnIndex(UrbanDB.CARD_IDENTITY))));
                    card.setName(c.getString(c.getColumnIndex(UrbanDB.CARD_NAME)));
                    card.setPrize(c.getString(c.getColumnIndex(UrbanDB.CARD_PRIZE)));
                    card.setRules(c.getString(c.getColumnIndex(UrbanDB.CARD_RULES)));
                    card.setTerms(c.getString(c.getColumnIndex(UrbanDB.CARD_TERMS)));
                    card.setRedeem_code(c.getString(c.getColumnIndex(UrbanDB.CARD_REDEEM_CODE)));
                    card.setExpires_at(c.getString(c.getColumnIndex(UrbanDB.CARD_EXPIRE)));
                    card.setStartTime(c.getString(c.getColumnIndex(UrbanDB.CARD_START_TIME)));
                    card.setEndTime(c.getString(c.getColumnIndex(UrbanDB.CARD_END_TIME)));

                    cards.add(card);

                } while (c.moveToNext());
                db.close();
            }
        }
        Log.e("UrbanDB",cards.toString());
        return cards;
    }


    public ItemRedeemResponse getUrbanCard(String cardId) {
        SQLiteDatabase db = getReadableDatabase();
        ItemRedeemResponse card=null;
        String SQL_QUERY = "SELECT * FROM " + UrbanDB.TABLE_CARD +" WHERE "+UrbanDB.CARD_ID +"='"+ cardId +"'";
        Cursor c;
        Log.e("UrbanDB",SQL_QUERY);
        if (db != null) {
            c = db.rawQuery(SQL_QUERY, null);
            if (c.moveToFirst()) {
                do {
                    card = new ItemRedeemResponse();
                    card.setIdCard(c.getString(c.getColumnIndex(UrbanDB.CARD_ID)));
                    card.setMerchant_id(c.getString(c.getColumnIndex(UrbanDB.CARD_MERCHANT_ID)));
                    card.setIdentity(Integer.parseInt(c.getString(c.getColumnIndex(UrbanDB.CARD_IDENTITY))));
                    card.setName(c.getString(c.getColumnIndex(UrbanDB.CARD_NAME)));
                    card.setPrize(c.getString(c.getColumnIndex(UrbanDB.CARD_PRIZE)));
                    card.setRules(c.getString(c.getColumnIndex(UrbanDB.CARD_RULES)));
                    card.setTerms(c.getString(c.getColumnIndex(UrbanDB.CARD_TERMS)));
                    card.setRedeem_code(c.getString(c.getColumnIndex(UrbanDB.CARD_REDEEM_CODE)));
                    card.setExpires_at(c.getString(c.getColumnIndex(UrbanDB.CARD_EXPIRE)));
                    card.setStartTime(c.getString(c.getColumnIndex(UrbanDB.CARD_START_TIME)));
                    card.setEndTime(c.getString(c.getColumnIndex(UrbanDB.CARD_END_TIME)));


                } while (c.moveToNext());
                db.close();
            }
        }
        return card;
    }





    public void UpdateTimeInCard(String cardId, String timeMilliseconds){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(UrbanDB.CARD_START_TIME,timeMilliseconds); //These Fields should be your String values of actual column names
        db.update(UrbanDB.TABLE_CARD, cv, UrbanDB.CARD_ID+" = '"+cardId+"'", null);
        db.close();
    }






    public void CloseSession(){

        SQLiteDatabase db=getWritableDatabase();
        if(db!=null){
            Log.e(TAG,"Abrio base");
            try {

                try {
                    db.execSQL("DELETE FROM "+UrbanDB.TABLE_USER );
                    db.execSQL("DELETE FROM "+UrbanDB.TABLE_CARD );

                    Log.e(TAG,"SE HA BORRADO LA BD");
                } catch (SQLiteException e) {
                    Log.e(TAG,"error al limpiar BD");
                }

            } catch (SQLiteException e) {
                Log.e(TAG,e.getMessage());

            }
        }
        db.close();
    }


    public void DeleteCard(String cardId){

        SQLiteDatabase db=getWritableDatabase();
        if(db!=null){
            try {

                try {
                    db.execSQL("DELETE FROM "+UrbanDB.TABLE_CARD +" WHERE "+UrbanDB.CARD_ID +"='"+ cardId+"'");

                    Log.e(TAG,"Tarjeta eliminada");
                } catch (SQLiteException e) {
                    Log.e(TAG,"error al limpiar BD");
                }

            } catch (SQLiteException e) {
                Log.e(TAG,e.getMessage());

            }
        }
        db.close();

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}