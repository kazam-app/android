package com.urbanity.android.utils;

/**
 * Created by DRM on 04/06/17.
 */

public class UrbanConstants {

    public static final int REGISTER_FB=-1;
    public static final int REGISTER=0;
    public static final int LOGIN=1;
    public static final int FORGOT=2;

    public static final int ERRORLOGOUT=401;
}
