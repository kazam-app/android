package com.urbanity.android.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.urbanity.android.R;
import com.urbanity.android.model.ItemRedeemResponse;
import com.urbanity.android.storage.UrbanDB;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by DRM on 27/05/17.
 */

public class UrbanUtils {


    public static boolean CARDANIMATIONEXCHANEGE = false;

    public static ProgressDialog getDialogUrban(Context context,String tittle, String message){
        ProgressDialog dialog=new ProgressDialog(context);
        dialog.setIndeterminate(false);
        dialog.setCancelable(false);
        dialog.setMessage(message);
        if(tittle!=null)dialog.setTitle(tittle);
        return dialog;
    }

    public static void DialogSimpleOK(Context context, String title, String msg){
        AlertDialog.Builder dialog=new AlertDialog.Builder(context, R.style.MyAlertDialogStyle);
        dialog.setTitle(title);
        dialog.setMessage(msg);
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    public static boolean isValidEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public static String getDate(String dateFull){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date convertedCurrentDate = sdf.parse(dateFull);
            String date=sdf.format(convertedCurrentDate );
            return date;
        }catch (Exception e){

        }
        return dateFull;
    }


    public static void insertCardRedeemed(Context context, ItemRedeemResponse itemRedeemed, String merchant_id){
        CARDANIMATIONEXCHANEGE =true;

        Date now=new Date();
        String cardIdCreate=String.valueOf(now.getTime());
        Date endTime=new Date();
        UrbanDB.getInstance(context).addUrbanCardRedeemed(cardIdCreate, itemRedeemed , merchant_id,String.valueOf(now.getTime()),  String.valueOf(endTime.getTime() + ((6*10000) *5)));

    }


}
