package com.urbanity.android.utils;

import android.content.Context;

import com.urbanity.android.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by DRM on 16/05/17.
 */

public class UrbanUtilsDate {

    public static String setFormat(String oldDate) throws ParseException {
        SimpleDateFormat formatter, FORMATTER;
        formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date = formatter.parse(oldDate.substring(0, 24));
        FORMATTER = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");
        System.out.println("OldDate-->" + oldDate);
        System.out.println("NewDate-->" + FORMATTER.format(date));

        return FORMATTER.format(date);
    }

    public static Date payDateFormat(Date sDate) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sourceFormat, formatter;

        sourceFormat = formatter = new SimpleDateFormat(
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        TimeZone tz = calendar.getTimeZone();
        formatter.setTimeZone(tz);
        calendar.setTime(sDate);
        calendar.add(Calendar.HOUR, tz.getRawOffset() / 3600000);
        return calendar.getTime();
    }

    public static String textDate(String sDate) throws ParseException {

        SimpleDateFormat originalFormat, newFormat;
        originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date = originalFormat.parse(sDate);
        newFormat = new SimpleDateFormat("dd MMMM yyyy");

        return newFormat.format(date);
    }


    public static String relativeTime(Context context, String date) throws ParseException {
        long lStartTime = new Date().getTime();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date1 = f.parse(date);

        long diffInSeconds = (lStartTime - date1.getTime()) / 1000;

        long diff[] = new long[]{0, 0, 0, 0};
            /* sec */
        diff[3] = (diffInSeconds >= 60 ? diffInSeconds % 60 : diffInSeconds);
		    /* min */
        diff[2] = (diffInSeconds = (diffInSeconds / 60)) >= 60 ? diffInSeconds % 60 : diffInSeconds;
		    /* hours */
        diff[1] = (diffInSeconds = (diffInSeconds / 60)) >= 24 ? diffInSeconds % 24 : diffInSeconds;
		    /* days */
        diff[0] = (diffInSeconds = (diffInSeconds / 24));

        if (diff[0] == 1)
            return String.format(context.getString(R.string.time_days), "" + diff[0]);
        else if (diff[0] > 1)
            return String.format(context.getString(R.string.time_days), "" + diff[0]) + "s";
        else if (diff[1] == 1)
            return String.format(context.getString(R.string.time_hours), (int) diff[1]);
        else if (diff[1] > 1)
            return String.format(context.getString(R.string.time_hours), (int) diff[1]) + "s";
        else
            return String.format(context.getString(R.string.time_minutes), (int) diff[2]) + "s";
    }

    public static String convertSeconds(int num) {
        int hor, min;

        hor = num / 3600;
        min = (num - (3600 * hor)) / 60;

        if (hor < 1) {
            if (min < 1)
                return "";
            else if (min == 1) {
                return min + " minuto";
            } else if (min > 1) {
                return min + " minutos";
            }
        } else if (hor == 1) {
            if (min < 1)
                return hor + " hora ";
            else if (min == 1)
                return hor + " hora " + min + "minuto";
            else
                return hor + " hora " + min + "minutos";

        } else {
            if (min < 1)
                return hor + " horas ";
            else if (min == 1)
                return hor + " horas " + min + "minuto";
            else
                return hor + " horas " + min + "minutos";
        }

        return "" + num + " segundos";
    }

}
