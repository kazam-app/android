package com.urbanity.android.user.perfil.model;

/**
 * Created by DRM on 04/06/17.
 */

public class SettingCloseSession {

    private String tittleClose;
    private UserSettingsType type;

    public SettingCloseSession(String tittleClose, UserSettingsType type) {
        this.tittleClose = tittleClose;
        this.type=type;
    }

    public UserSettingsType getType() {
        return type;
    }

    public void setType(UserSettingsType type) {
        this.type = type;
    }

    public String getTittleClose() {
        return tittleClose;
    }

    public void setTittleClose(String tittleClose) {
        this.tittleClose = tittleClose;
    }
}
