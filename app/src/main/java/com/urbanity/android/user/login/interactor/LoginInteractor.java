package com.urbanity.android.user.login.interactor;

import android.content.Context;

import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.user.login.viewListener.OnLoginFinished;

/**
 * Created by DRM on 22/05/17.
 */

public interface LoginInteractor {
    void OnLogin(Context context, UrbanUser user, OnLoginFinished listener);
    void OnLoginFB(Context context, UrbanUser user, OnLoginFinished listener);
}
