package com.urbanity.android.user.login.presenter;

import android.content.Context;

import com.urbanity.android.model.UrbanUser;

/**
 * Created by DRM on 22/05/17.
 */

public interface LoginPresenter {
//    void validateRegister(Context context, String name, String lastname, String email, String password, String repasword);

    void validateLogin(Context context, String username,  String pasword);
    void validateLoginFB(Context context, UrbanUser user);

}
