package com.urbanity.android.user.register;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.urbanity.android.R;
import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.customsViews.customDialog.CustomDialog;
import com.urbanity.android.customsViews.customDialog.SimpleCustomDialog;
import com.urbanity.android.model.TypeDialog;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.onBoarding.OnBoardingActivity;
import com.urbanity.android.onBoarding.OnBoardingType;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.user.PerfilUserActivity;
import com.urbanity.android.user.register.presenter.RegisterPresenter;
import com.urbanity.android.user.register.presenter.RegisterPresenterImp;
import com.urbanity.android.user.register.registerViewInterface.RegisterView;
import com.urbanity.android.utils.UrbanUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.urbanity.android.analytics.EventsFB.SCREEN_ERROR_INVITE_CODE;
import static com.urbanity.android.analytics.EventsFB.SCREEN_INVITE_CODE;
import static com.urbanity.android.analytics.EventsFB.SCREEN_SUCCESS_INVITE_CODE;

/**
 * Created by DRM on 27/05/17.
 */

public class FragmentRegisterInsertCode extends BaseFragment implements RegisterView {

    ProgressDialog pDialog;
    RegisterPresenter presenter;
    UrbanUser user;
    EditText edCodeInvite;
    TextInputLayout tl_code;
    boolean isFB=false;

    public static FragmentRegisterInsertCode getInstance(UrbanUser user){
        FragmentRegisterInsertCode fragment=new FragmentRegisterInsertCode();
        Bundle arg=new Bundle();
        arg.putSerializable(PerfilUserActivity.USER,user);
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_insert_code,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        user=(UrbanUser)getArguments().getSerializable(PerfilUserActivity.USER);
        isFB=user.getFb_id().length()>0;
        edCodeInvite=(EditText)findViewById(R.id.edCodeInvite);
        tl_code=(TextInputLayout)findViewById(R.id.tl_code);
        pDialog= UrbanUtils.getDialogUrban(getActivity(),null,"Un momento por favor");
        presenter=new RegisterPresenterImp(this);
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_INVITE_CODE);

        findViewById(R.id.bt_finish_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edCodeInvite.getText().toString().isEmpty())
                    user.setInvite_code(edCodeInvite.getText().toString());
                    presenter.validateRegister(getActivity(),user);

            }
        });

        if (isFB){
            FacebookAnalytics.FBSimpleEvent(getActivity(), EventsFB.EVENT_SIGNUP_FB_INVITE_CODE);
        }else {
            FacebookAnalytics.FBSimpleEvent(getActivity(), EventsFB.EVENT_SIGNUP_EMAIL_INVITE_CODE);
        }

    }


    public static closeMainActivityListener mListener;

    public static void setListener(closeMainActivityListener listener){
        mListener=listener;
    }

    public interface closeMainActivityListener{
        void onCloseMainActivity();
    }



    @Override
    public void showProgress() {
        pDialog.show();
    }

    @Override
    public void hideProgress() {
        pDialog.dismiss();
    }

    @Override
    public void onEmptyField(UserFieldType type) {

    }


    @Override
    public void onSuccess(UrbanUser user) {

        HashMap<String,String>params=new HashMap<>();
        params.put(EventsFB.PARAM_GENDER,user.getGender().equals("1")?"male": "female");
        params.put(EventsFB.PARAM_DOB,user.getBirthdate());
        params.put(EventsFB.PARAM_AGE, String.valueOf(FragmentRegisterPersonal.YEARSOLD));



        if (isFB){
            String mDate=String.valueOf(user.getBirthdate());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");

            try {
                Calendar nowCalendar = Calendar.getInstance();
                Date newD = sdf.parse(mDate);
                long diff = nowCalendar.getTime().getTime() - newD.getTime();

                long  diffYears =   Math.round(diff / 1000 / 60 / 60 / 24 / 365);
                FragmentRegisterPersonal.YEARSOLD=diffYears;
                params.put(EventsFB.PARAM_AGE, String.valueOf(FragmentRegisterPersonal.YEARSOLD));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_SIGNUP_FB_SUCCESS,params);

        }else {

            FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_SIGNUP_EMAIL_SUCCESS,params);

        }

        UrbanDB.getInstance(getActivity()).addUrbanUser(user);
        UrbanPrefs.getInstance(getActivity()).setIsLoged(true);
        if(user.getMerchant()!=null){
            FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_SUCCESS_INVITE_CODE);
            SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), "¡Felicidades!", "¡Ya tienes tu primer sello en "+user.getMerchant().getName()+"! Solo debes terminar tu registro.","Terminar Registro",user.getMerchant().getImage_url(), TypeDialog.PENDING, new SimpleCustomDialog.OnButtonClickListener() {
                @Override
                public void onAcceptClickListener(Button button, AlertDialog dialog) {
                    dialog.dismiss();
                    getActivity().finish();
                    startActivity(new Intent(getActivity(), OnBoardingActivity.class).putExtra(OnBoardingActivity.BOARDINGTYPE, OnBoardingType.APPDESC));
                    getActivity().finish();
                    if(mListener!=null){
                        mListener.onCloseMainActivity();
                    }

                }
            });
            dialog.show();
        }else {
            startActivity(new Intent(getActivity(), OnBoardingActivity.class).putExtra(OnBoardingActivity.BOARDINGTYPE, OnBoardingType.APPDESC));

            getActivity().finish();
            if(mListener!=null){
                mListener.onCloseMainActivity();
            }
        }

    }

    @Override
    public void onError(String error) {
        AlertDialog.Builder dialog=new AlertDialog.Builder(getActivity(),R.style.MyAlertDialogStyle);
        dialog.setTitle("Error al registrarse");
        dialog.setMessage(error);
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onErrorInviteCode() {
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_ERROR_INVITE_CODE);
        tl_code.setError("Este código no pertenece a ninguna marca, intenta de nuevo.");
    }
}
