package com.urbanity.android.user.update.interactor;

import android.content.Context;

import com.urbanity.android.model.ErrorResponse;
import com.urbanity.android.model.UrbanResponseError;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.network.ErrorUtils;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.user.update.updateListener.OnUpdateFinishListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DRM on 06/07/17.
 */

public class UpdateInteractorImpl implements UpdateInteractor {
    @Override
    public void onUpdateDataUser(Context context, UrbanUser user, final OnUpdateFinishListener listener) {
        String token=UrbanDB.getInstance(context).getUrbanUser().getUrbanToken();

        Call<UrbanUser>updateUser= RetrofitService.getUrbanService().putUpdateUser(
                token,
                user.getName(),
                user.getLast_names(),
                Integer.parseInt(user.getGender()),
                user.getBirthdate());

        updateUser.enqueue(new Callback<UrbanUser>() {
            @Override
            public void onResponse(Call<UrbanUser> call, Response<UrbanUser> response) {
                if(response.body()!=null){
                    listener.onSuccessListener(response.body());
                }else {
                    UrbanResponseError error= ErrorUtils.parseError(response);
                    //No vienen  datos
                    listener.onErrorListener(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UrbanUser> call, Throwable t) {
                listener.onErrorListener("Ocurrio un erro al actualizar sus datos");
            }
        });


    }

    @Override
    public void onUpdatePassword(Context context, String oldPass, String newPass, String newRepass, final OnUpdateFinishListener listener) {
        String token=UrbanDB.getInstance(context).getUrbanUser().getUrbanToken();
        Call<Boolean>updatePassword=RetrofitService.getUrbanService().putUpdatePassword(token,oldPass,newPass,newRepass);
        updatePassword.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                   try{
                       UrbanResponseError error= ErrorUtils.parseError(response);
                       listener.onErrorListener(error.getMessage());
                   }catch (Exception e){
                       listener.onSuccessListener(null);
                   }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                listener.onErrorListener("Ocurrio un error al intentar actualizar sus contraseña");
            }
        });

    }
}
