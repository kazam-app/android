package com.urbanity.android.user.update;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseActivity;
import com.urbanity.android.user.changeMail.FragmentChangeMail;
import com.urbanity.android.user.forgotPassword.FragmentForgot;

/**
 * Created by DRM on 07/07/17.
 */

public class ActivityChangeMail extends BaseActivity {

    public static final int TYPEMAIL=1;
    public static final int TYPEPASS=2;
    int type=0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_mail);

        Bundle arg=getIntent().getExtras();
        if(arg!=null){
            type=arg.getInt("type");
            switch (type){
                case TYPEMAIL:
                    setTitleToolbar("Cambiar Correo");
                    getSupportFragmentManager().beginTransaction().replace(R.id.content_update_mail, FragmentChangeMail.getInstance()).commit();

                    break;


                case TYPEPASS:
                    setTitleToolbar("Cambiar Contraseña");
                    getSupportFragmentManager().beginTransaction().replace(R.id.content_update_mail, FragmentForgot.getInstance()).commit();

                    break;

            }

        }


    }
}
