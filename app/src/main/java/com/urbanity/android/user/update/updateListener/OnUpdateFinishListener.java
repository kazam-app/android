package com.urbanity.android.user.update.updateListener;

import com.urbanity.android.model.UrbanUser;

/**
 * Created by DRM on 06/07/17.
 */

public interface OnUpdateFinishListener {

    void onErrorListener(String error);
    void onSuccessListener(UrbanUser user);

}
