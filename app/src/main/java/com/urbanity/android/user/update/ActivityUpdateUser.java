package com.urbanity.android.user.update;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseActivity;
import com.urbanity.android.customsViews.customDialog.SimpleCustomDialog;
import com.urbanity.android.model.TypeDialog;
import com.urbanity.android.model.TypeFieldEmpty;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.user.register.FragmentRegisterPersonal;
import com.urbanity.android.user.update.presenter.UpdateuserPresenter;
import com.urbanity.android.user.update.presenter.UpdateuserPresenterImp;
import com.urbanity.android.user.update.updateListener.UpdateView;
import com.urbanity.android.utils.UrbanUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by DRM on 06/07/17.
 */

public class ActivityUpdateUser extends BaseActivity implements UpdateView {

    EditText input_ed_register_name,  input_ed_register_last_name,   input_ed_mail;
    TextView tx_ed_birthday;
    AppCompatRadioButton rb_ed_female, rb_ed_male;
    UrbanUser user;
    Button bt_update_data;
    UpdateuserPresenter presenter;
    ProgressDialog pDialog;

    public DatePickerDialog fromDatePickerDialog;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);
        setTitleToolbar("Perfil");
        user= UrbanDB.getInstance(this).getUrbanUser();
        presenter=new UpdateuserPresenterImp(this);
        pDialog= UrbanUtils.getDialogUrban(this,null,"Actualizando Datos");

        input_ed_register_name=(EditText)findViewById(R.id.input_ed_register_name);
        input_ed_register_name.setText(user.getName());

        input_ed_register_last_name=(EditText)findViewById(R.id.input_ed_register_last_name);
        input_ed_register_last_name.setText(user.getLast_names());

        bt_update_data=(Button)findViewById(R.id.bt_update_data);

        input_ed_mail    =(EditText)findViewById(R.id.input_ed_mail);
        if(user.getEmail()!=null){
            input_ed_mail.setText(user.getEmail());
            input_ed_mail.setEnabled(false);
        }

        tx_ed_birthday=(TextView)findViewById(R.id.tx_ed_birthday);
        tx_ed_birthday.setText(String.valueOf(user.getBirthdate()));
        tx_ed_birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendar();
            }
        });


        rb_ed_male=(AppCompatRadioButton)findViewById(R.id.rb_ed_male);
        rb_ed_female=(AppCompatRadioButton)findViewById(R.id.rb_ed_female);
        if(user.getGender().equals("1")){
            rb_ed_male.setChecked(true);
        }else{
            rb_ed_female .setChecked(true);
        }

        bt_update_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.setName(input_ed_register_name.getText().toString());
                user.setLast_names(input_ed_register_last_name.getText().toString());
                user.setGender(rb_ed_male.isChecked()?"1":"0");
                user.setBirthdate(tx_ed_birthday.getText().toString());
                presenter.validateDataUser(ActivityUpdateUser.this,user);
            }
        });
    }

    void showCalendar(){

        final Calendar nowCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(ActivityUpdateUser.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                String day="", month="";
                Calendar dialogDate = Calendar.getInstance();
                dialogDate.set(year, monthOfYear, dayOfMonth);

                long diff = nowCalendar.getTime().getTime() - dialogDate.getTime().getTime();
                long diffYears =   Math.round(diff / 1000 / 60 / 60 / 24 / 365);

                if(dayOfMonth<10){
                    day="0"+dayOfMonth;
                }else {
                    day=String.valueOf(dayOfMonth);
                }
                if(monthOfYear+1<10){
                    month=String.valueOf("0"+ (monthOfYear+1));
                }else {
                    month=String.valueOf(monthOfYear+1);
                }

                tx_ed_birthday.setText(String.valueOf(year)+"-" + month +"-"+ day);

            }
        },nowCalendar.get(Calendar.YEAR), nowCalendar.get(Calendar.MONTH), nowCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.show();
    }




    @Override
    public void showProgress() {
        pDialog.show();
    }

    @Override
    public void hideProgress() {
        pDialog.dismiss();
    }

    @Override
    public void OnSuccess(UrbanUser user) {

        HashMap<String,String> arg=new HashMap<>();

        String mDate=String.valueOf(user.getBirthdate());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");

        try {
            Calendar nowCalendar = Calendar.getInstance();
            Date newD = sdf.parse(mDate);
            long diff = nowCalendar.getTime().getTime() - newD.getTime();

            long  diffYears =   Math.round(diff / 1000 / 60 / 60 / 24 / 365);
            arg.put("user_age", String.valueOf(diffYears));
        } catch (ParseException e) {
            e.printStackTrace();
        }



        arg.put("user_id",user.getUrbanId());

        arg.put("user_gender",user.getGender().equals("1")?"Male": "Female");

        arg.put("user_birthdate", user.getBirthdate() );
        if(user.getFb_id().isEmpty()){
            arg.put("signup_type","Email");
        }else {
            arg.put("signup_type","Facebook");
        }
        FacebookAnalytics.FBEventsSendProperties(ActivityUpdateUser.this,arg);




        UrbanDB.getInstance(this).UpdateUser(user);
        SimpleCustomDialog dialog=new SimpleCustomDialog(ActivityUpdateUser.this, this.getResources().getString(R.string.app_name) , "Sus datos han sido actualziados", "Aceptar", TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
             dialog.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public void OnError(String error) {

    }

    @Override
    public void OnErrorConnection() {

    }

    @Override
    public void OnEmptyField(TypeFieldEmpty fieldEmpty) {
        switch (fieldEmpty){
            case NAME:
                UrbanUtils.DialogSimpleOK(ActivityUpdateUser.this,"Campo vacio","Ingresa tu nombre");
                break;

            case LASTNAME:
                UrbanUtils.DialogSimpleOK(ActivityUpdateUser.this,"Campo vacio","Ingresa tu apellido");
                break;
        }
    }
}
