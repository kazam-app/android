package com.urbanity.android.user.update.interactor;

import android.content.Context;

import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.user.update.updateListener.OnUpdateFinishListener;

/**
 * Created by DRM on 06/07/17.
 */

public interface UpdateInteractor {
    void onUpdateDataUser(Context context, UrbanUser user, OnUpdateFinishListener listener);
    void onUpdatePassword(Context context, String oldPass, String newPass, String newRepass, OnUpdateFinishListener listener);
}
