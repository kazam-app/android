package com.urbanity.android.user.perfil;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.urbanity.android.R;
import com.urbanity.android.UrbanApplication;
import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.contact.ContactActivity;
import com.urbanity.android.customsViews.customDialog.CustomDialog;
import com.urbanity.android.interaction.FragmentMainInteraction;
import com.urbanity.android.legal.ActivityLegalFAQ;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.user.notifications.ActivityNotifications;
import com.urbanity.android.user.perfil.adapter.AdapterUserData;
import com.urbanity.android.user.perfil.model.SettingCloseSession;
import com.urbanity.android.user.perfil.model.UserRegisterData;
import com.urbanity.android.user.perfil.model.UserSettingsTittle;
import com.urbanity.android.user.perfil.model.UserSettingsType;
import com.urbanity.android.user.perfil.model.UserSimpleSettings;
import com.urbanity.android.user.update.ActivityChangeMail;
import com.urbanity.android.user.update.ActivityUpdatePassword;
import com.urbanity.android.user.update.ActivityUpdateUser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DRM on 04/06/17.
 */

public class FragmentPerfilDetail extends BaseFragment{


    AdapterUserData adapter;
    RecyclerView rvUserData;


    public static FragmentPerfilDetail getInstance(){
        return new FragmentPerfilDetail();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_data_detail,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //TODO mandar a constants
        FacebookAnalytics. FireBaseScreenTracking(getActivity(),"Settings");

        rvUserData=(RecyclerView)findViewById(R.id.user_data);
        rvUserData.setLayoutManager(new LinearLayoutManager(getActivity()));

        findViewById(R.id.toolbarImg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                       FragmentMainInteraction.sawPerfil=2;
                        getActivity().onBackPressed();
            }
        });

        adapter = new AdapterUserData();

        adapter.setData(getActivity(), getSettings(UrbanPrefs.getInstance(getActivity()).isLoged()), new AdapterUserData.OnItemClickListener() {
            @Override
            public void onItemClick(UserSettingsType type) {
                switch (type){
                    case CLOSESESSION:
                        //TODO mandar a constants
                        FacebookAnalytics. FireBaseScreenTracking(getActivity(),"Settings - Log Out");


                        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
                        builder.setTitle("Cerrar Sesión");
                        builder.setMessage("¿Estas seguro que deseas cerrar tu sesión?");
                        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FacebookAnalytics.FBSimpleEvent(getActivity(), EventsFB.EVENT_LOGOUT);
                                dialog.dismiss();

                                UrbanApplication.getInstance().clearApplicationData();

                                UrbanDB.getInstance(getActivity()).CloseSession();

                                UrbanPrefs.getInstance(getActivity()).ClearPrefs();
                                UrbanPrefs.getInstance(getActivity()).setIsLoged(false);
                                UrbanPrefs.getInstance(getActivity()).setRunAppFirst(false);

                                getActivity().finish();
                                startActivity(new Intent(getActivity(), MainActivity.class));

                            }
                        });

                        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();


                        break;

                    case PERMISSIONNOTIF:
                        startActivity(new Intent(getActivity(),ActivityNotifications.class));
                        break;

                    case CHANGEPASSWORD:
                        validateSession(type);
                        break;

                    case FAQS:
                        startActivity(new Intent(getActivity(),ActivityLegalFAQ.class).putExtra(ActivityLegalFAQ.TERM, UserSettingsType.FAQS));
                        break;
                    case CONTACTS:
                        startActivity(new Intent(getActivity(),ContactActivity.class));
                        break;

                    case TERMS:
                        startActivity(new Intent(getActivity(),ActivityLegalFAQ.class).putExtra(ActivityLegalFAQ.TERM, UserSettingsType.TERMS));
                        break;

                    case PRIVS:
                        startActivity(new Intent(getActivity(),ActivityLegalFAQ.class).putExtra(ActivityLegalFAQ.TERM, UserSettingsType.PRIVS));
                        break;

                    case MAIL:
                        startActivity(new Intent(getActivity(),ActivityChangeMail.class).putExtra("type",ActivityChangeMail.TYPEMAIL));

                        break;

                    case NAME:
                    case LASTNAME:
                    case DOB:
                    case GENDER:

                        validateSession(type);
                        break;
                }
            }
        });
        rvUserData.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }




    public void validateSession(UserSettingsType type){


        if(UrbanPrefs.getInstance(getActivity()).isLoged()){

           switch (type){
               case CHANGEPASSWORD:
                   startActivity(new Intent(getActivity(),ActivityUpdatePassword.class));
                   break;
               default:
                   startActivity(new Intent(getActivity(),ActivityUpdateUser.class));
                   break;
           }
        }else {
//TODO mandar a constants
            FacebookAnalytics. FireBaseScreenTracking(getActivity(),"Settings - No Account [Register Pop Up]");
            CustomDialog dialog=new CustomDialog(getActivity(), "Regístrate y comienza a ganar", "Regístrate para comenzar a ganar sellos y premios de tus marcas favoritas.","Cancelar","¡Registrarme!", new CustomDialog.OnButtonClickListener() {
                @Override
                public void onAcceptClickListener(Button button, AlertDialog dialog) {
                    dialog.dismiss();

                    ((MainActivity)getActivity()).changeFragment();


                }

                @Override
                public void onCancelClickListener(Button button, AlertDialog dialog) {
                    dialog.dismiss();
                }
            });
            dialog.show();

        }
    }





    public ArrayList<Object> getSettings(boolean isLoged){
        final ArrayList<Object>items =new ArrayList<>();


        if(isLoged){
            FacebookAnalytics. FireBaseScreenTracking(getActivity(),"Settings - User Info");

            Call<UrbanUser> user = RetrofitService.getUrbanService().getUser(UrbanDB.getInstance(getActivity()).getUrbanUser().getUrbanToken());

            user.enqueue(new Callback<UrbanUser>() {
                @Override
                public void onResponse(Call<UrbanUser> call, Response<UrbanUser> response) {
                    if(response.body()!=null){

                        UrbanUser mUser= response.body();


                        items.add(new UserSettingsTittle("MI CUENTA"));
                        items.add(new UserRegisterData("Nombre",  mUser.getName(), UserSettingsType.NAME));
                        items.add(new UserRegisterData("Apellido", mUser.getLast_names(), UserSettingsType.LASTNAME));
                        items.add(new UserRegisterData("Fecha de nacimiento", mUser.getBirthdate(), UserSettingsType.DOB));
                        items.add(new UserRegisterData("Genero",mUser.getGender().equals("1")? "HOMBRE" : "MUJER" ,  UserSettingsType.GENDER));
                        items.add(new UserRegisterData("Correo", mUser.getEmail(), UserSettingsType.MAIL));

                        items.add(new UserSimpleSettings("Cambiar contraseña", UserSettingsType.CHANGEPASSWORD));
                        items.add(new UserSettingsTittle("NOTIFICACIONES"));
                        items.add(new UserSimpleSettings("Permisos de Notificaciones",UserSettingsType.PERMISSIONNOTIF));
                        items.add(new UserSettingsTittle("AYUDA"));
                        items.add(new UserSimpleSettings("Preguntas Frecuentes",UserSettingsType.FAQS));
                        items.add(new UserSimpleSettings("Contáctanos",UserSettingsType.CONTACTS));
                        items.add(new UserSettingsTittle("LEGAL"));
                        items.add(new UserSimpleSettings("Términos y Condiciones",UserSettingsType.TERMS));
                        items.add(new UserSimpleSettings("Política de Privacidad",UserSettingsType.PRIVS));
                        items.add(String.valueOf("emojiView"));
                        items.add(new SettingCloseSession("Cerrar Sesión",UserSettingsType.CLOSESESSION));

                        adapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<UrbanUser> call, Throwable t) {

                }
            });



        }
        else {

            items.add(new UserSettingsTittle("MI CUENTA"));
            items.add(new UserRegisterData("Nombre","", UserSettingsType.NAME));
            items.add(new UserRegisterData("Apellido", "", UserSettingsType.LASTNAME));
            items.add(new UserRegisterData("Fecha de nacimiento", "", UserSettingsType.DOB));
            items.add(new UserRegisterData("Genero", "" , UserSettingsType.GENDER));
            items.add(new UserRegisterData("Correo", "", UserSettingsType.MAIL));
            items.add(new UserSimpleSettings("Cambiar contraseña", UserSettingsType.CHANGEPASSWORD));
            items.add(new UserSettingsTittle("NOTIFICACIONES"));
            items.add(new UserSimpleSettings("Permisos de Notificaciones",UserSettingsType.PERMISSIONNOTIF));
            items.add(new UserSettingsTittle("AYUDA"));
            items.add(new UserSimpleSettings("Preguntas Frecuentes",UserSettingsType.FAQS));
            items.add(new UserSimpleSettings("Contáctanos",UserSettingsType.CONTACTS));
            items.add(new UserSettingsTittle("LEGAL"));
            items.add(new UserSimpleSettings("Términos y Condiciones",UserSettingsType.TERMS));
            items.add(new UserSimpleSettings("Política de Privacidad",UserSettingsType.PRIVS));
            items.add(String.valueOf("emojiView"));
            adapter.notifyDataSetChanged();

            //TODO mandar a constants
            FacebookAnalytics. FireBaseScreenTracking(getActivity(),"Settings - No Account");
        }

        return items;
    }
}
