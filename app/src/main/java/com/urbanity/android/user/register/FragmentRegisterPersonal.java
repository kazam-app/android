package com.urbanity.android.user.register;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.urbanity.android.R;
import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.user.PerfilUserActivity;
import com.urbanity.android.user.register.presenter.RegisterPresenter;
import com.urbanity.android.user.register.presenter.RegisterPresenterImp;
import com.urbanity.android.user.register.registerViewInterface.RegisterView;
import com.urbanity.android.utils.UrbanDevice;
import com.urbanity.android.utils.UrbanUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.urbanity.android.analytics.EventsFB.EVENT_SIGNUP_EMAIL_EXTRA_INFO;
import static com.urbanity.android.analytics.EventsFB.SCREEN_REGISTER2;
import static com.urbanity.android.analytics.EventsFB.SCREEN_REGISTER3;


/**
 * Created by DRM on 20/05/17.
 */

public class FragmentRegisterPersonal extends BaseFragment implements RegisterView {

   public DatePickerDialog fromDatePickerDialog;

    TextView tx_birtday;
    UrbanUser user;
    EditText inputRegisterLastName, inputRegisterName, edMail;
    AppCompatRadioButton rbMale;
    RegisterPresenter presenter;
    TextInputLayout input_mail;
    public static long YEARSOLD=0;

    Calendar nowCalendar;
    SimpleDateFormat formatter;
    long diffYears;


    public static FragmentRegisterPersonal getInstance(UrbanUser user){
        FragmentRegisterPersonal fragment=new FragmentRegisterPersonal();
        Bundle arg=new Bundle();
        arg.putSerializable(PerfilUserActivity.USER,user);
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_register_personal,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle("Registro");

        tx_birtday=(TextView) findViewById(R.id.tx_birtday);
        input_mail=(TextInputLayout)findViewById(R.id.input_mail);
        edMail=(EditText)findViewById(R.id.ed_mail);

        user=(UrbanUser)getArguments().getSerializable(PerfilUserActivity.USER);
        inputRegisterLastName=(EditText)findViewById(R.id.input_register_last_name);
        inputRegisterName=(EditText)findViewById(R.id.input_register_name);
        rbMale=(AppCompatRadioButton)findViewById(R.id.rb_male);
        rbMale.setActivated(true);


        if(user!=null){

            FacebookAnalytics.FBSimpleEvent(getActivity(),EVENT_SIGNUP_EMAIL_EXTRA_INFO);


            edMail.setText(user.getEmail());
            if( user.getBirthdate()!=null && !user.getBirthdate().isEmpty()){
                tx_birtday.setText(user.getBirthdate().replace("/","-"));

                String mDate=String.valueOf(tx_birtday.getText().toString());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");

                try {
                    Calendar nowCalendar = Calendar.getInstance();
                    Date newD = sdf.parse(mDate);
                    long diff = nowCalendar.getTime().getTime() - newD.getTime();

                    long  diffYears =   Math.round(diff / 1000 / 60 / 60 / 24 / 365);
                    YEARSOLD=diffYears;
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
            if( user.getName()!=null &&  !user.getName().isEmpty()){
                inputRegisterName.setText(user.getName());
                if(user.getEmail()!=null && !user.getEmail().isEmpty()){
                    input_mail.setVisibility(View.VISIBLE);
                }
            }else {
                FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_REGISTER3);
            }

            if(user.getLast_names()!=null  && !user.getLast_names().isEmpty()){
                inputRegisterLastName.setText(user.getLast_names());
            }

        }


        tx_birtday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalendar();
            }
        });
        presenter=new RegisterPresenterImp(this);

        findViewById(R.id.bt_register_create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.setEmail(edMail.getText().toString());
                if(tx_birtday.getText().toString().isEmpty()){
                    UrbanUtils.DialogSimpleOK(getActivity(),getResources().getString(R.string.errorRegisterMessage),getResources().getString(R.string.errorRegisterDOB));
                    return;
                }
                if(!rbMale.isChecked() && !((AppCompatRadioButton)findViewById(R.id.rb_female)).isChecked() ){
                    UrbanUtils.DialogSimpleOK(getActivity(),getResources().getString(R.string.errorRegisterMessage),getResources().getString(R.string.errorRegisterGender));
                    return;
                }

                presenter.validateUserData(getActivity(),user,inputRegisterName.getText().toString(),inputRegisterLastName.getText().toString(),tx_birtday.getText().toString(),rbMale.isChecked()?1:0);
            }
        });
    }


    void showCalendar(){

        final Calendar nowCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                String day="", month="";
                Calendar dialogDate = Calendar.getInstance();
                dialogDate.set(year, monthOfYear, dayOfMonth);

                long diff = nowCalendar.getTime().getTime() - dialogDate.getTime().getTime();
                long diffYears =   Math.round(diff / 1000 / 60 / 60 / 24 / 365);

                if(dayOfMonth<10){
                    day="0"+dayOfMonth;
                }else {
                    day=String.valueOf(dayOfMonth);
                }
                if(monthOfYear+1<10){
                    month=String.valueOf("0"+ (monthOfYear+1));
                }else {
                    month=String.valueOf(monthOfYear+1);
                }

                YEARSOLD = diffYears;

                tx_birtday.setText(String.valueOf(year)+"-" + month +"-"+ day);

            }
        },nowCalendar.get(Calendar.YEAR), nowCalendar.get(Calendar.MONTH), nowCalendar.get(Calendar.DAY_OF_MONTH));

        fromDatePickerDialog.show();
    }


    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onEmptyField(UserFieldType type) {
        switch (type){
            case NAME:
                inputRegisterName.setFocusableInTouchMode(true);
                inputRegisterName.requestFocus();
                inputRegisterName.setError(getResources().getString(R.string.errorInsertName));
                break;

            case LASTNAME:
                inputRegisterLastName.setFocusableInTouchMode(true);
                inputRegisterLastName.requestFocus();
                inputRegisterLastName.setError(getResources().getString(R.string.errorInsertLastName));
                break;


        }
    }

    @Override
    public void onSuccess(UrbanUser user) {



        HashMap<String,String>arg=new HashMap<>();
        arg.put("user_id",user.getUrbanId());

        arg.put("user_gender",user.getGender().equals("1")?"Male": "Female");
        arg.put("user_age", String.valueOf(FragmentRegisterPersonal.YEARSOLD));
        arg.put("user_birthdate", user.getBirthdate() );
        if(user.getFb_id().isEmpty()){
            arg.put("signup_type","Email");
        }else {
            arg.put("signup_type","Facebook");
        }
        FacebookAnalytics.FBEventsSendProperties(getActivity(),arg);


        if(user.getEmail().isEmpty()){
            user.setEmail(edMail.getText().toString());
        }
        user.setName(inputRegisterName.getText().toString());
        user.setLast_names(inputRegisterLastName.getText().toString());
        user.setGender(rbMale.isChecked()?"1":"0");
        user.setBirthdate(tx_birtday.getText().toString());

        //Validar si tiene la ubicacion activa
        if(!UrbanDevice.isLocationEnabled(getActivity())  || ! UrbanDevice.checkLocationPermission(getActivity() ) ){

            addFragmentToBackStack(FragmentActiveLocation.getInstance(user),"insertCode",R.id.content_user_perfil);

        }else{
            addFragmentToBackStack(FragmentRegisterInsertCode.getInstance(user),"insertCode",R.id.content_user_perfil);

        }

    }


    @Override
    public void onError(String error) {
        UrbanUtils.DialogSimpleOK(getActivity(),getResources().getString(R.string.errorRegisterMessage),error);
    }

    @Override
    public void onErrorInviteCode() {

    }
}
