package com.urbanity.android.user.login.viewListener;

import com.urbanity.android.model.UrbanUser;

/**
 * Created by DRM on 22/05/17.
 */

public interface LoginView {

    void onShowProgress();
    void onHideProgress();

    void onError(String error);
    void onSuccess(UrbanUser user);

    void onEmptyMail();
    void onEmptyPass();
}
