package com.urbanity.android.user.register;

/**
 * Created by DRM on 28/05/17.
 */

public enum  UserFieldType {
    MAIL,
    MAIL_FORMAT,
    PASS,
    REPASS,
    NAME,
    LASTNAME
}
