package com.urbanity.android.user.update.updateListener;

import com.urbanity.android.model.TypeFieldEmpty;
import com.urbanity.android.model.UrbanUser;

/**
 * Created by DRM on 06/07/17.
 */

public interface UpdateView {
    void showProgress();
    void hideProgress();


    void OnSuccess(UrbanUser user);
    void OnError(String error);
    void OnErrorConnection();

    void OnEmptyField(TypeFieldEmpty fieldEmpty);

}
