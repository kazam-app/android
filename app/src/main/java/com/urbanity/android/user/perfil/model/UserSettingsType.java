package com.urbanity.android.user.perfil.model;

/**
 * Created by DRM on 04/06/17.
 */

public enum UserSettingsType {
    NAME,
    LASTNAME,
    DOB,
    GENDER,
    MAIL,
    CHANGEPASSWORD,
    PERMISSIONNOTIF,
    FAQS,
    CONTACTS,
    TERMS,
    PRIVS,
    CLOSESESSION

}
