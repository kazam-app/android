package com.urbanity.android.user.perfil.model;

/**
 * Created by DRM on 04/06/17.
 */

public class UserSettingsTittle {
    private String tittle;


    public UserSettingsTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }
}
