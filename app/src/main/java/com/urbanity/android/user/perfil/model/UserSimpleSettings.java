package com.urbanity.android.user.perfil.model;

/**
 * Created by DRM on 04/06/17.
 */

public class UserSimpleSettings {
    private String tittle;
    UserSettingsType type;



    public UserSimpleSettings(String tittle, UserSettingsType type) {
        this.tittle = tittle;
        this.type = type;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public UserSettingsType getType() {
        return type;
    }

    public void setType(UserSettingsType type) {
        this.type = type;
    }
}
