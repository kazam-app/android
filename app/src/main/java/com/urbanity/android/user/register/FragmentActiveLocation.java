package com.urbanity.android.user.register;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.urbanity.android.R;
import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.customsViews.loadingView.AVLoadingIndicatorView;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.user.PerfilUserActivity;
import com.urbanity.android.utils.UrbanDevice;

import java.util.HashMap;

import static com.urbanity.android.analytics.EventsFB.SCREEN_LOCATION_ASK;

/**
 * Created by DRM on 07/06/17.
 */

public class FragmentActiveLocation extends BaseFragment {


    AVLoadingIndicatorView avi;
    UrbanUser user;
    boolean isFB=false;



    private static final int READ_LOCATION_PERMISSIONS_REQUEST = 1;




    public static FragmentActiveLocation getInstance(UrbanUser user){
        FragmentActiveLocation fragment=new FragmentActiveLocation();
        Bundle arg=new Bundle();
        arg.putSerializable(PerfilUserActivity.USER,user);
        fragment.setArguments(arg);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_active_location, container,false);
    }



    public boolean checkPermissions() {
        boolean resultP=true;
        // 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid
        // checking the build version since Context.checkSelfPermission(...) is only available
        // in Marshmallow
        // 2) Always check for permission (even if permission has already been granted)
        // since the user can revoke permissions at any time through Settings
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            resultP= false;
            // The permission is NOT already granted.
            // Check if the user has been asked about this permission already and denied
            // it. If so, we want to give more explanation about why the permission is needed.
            if (shouldShowRequestPermissionRationale(
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show our own UI to explain to the user why we need to read the contacts
                // before actually requesting the permission and showing the default UI
            }

            // Fire off an async request to actually get the permission
            // This will show the standard permission request dialog UI
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, READ_LOCATION_PERMISSIONS_REQUEST);
        }
        return resultP;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        user=(UrbanUser)getArguments().getSerializable(PerfilUserActivity.USER);
        isFB=user.getFb_id().length()>0;
        String indicator="BallScaleMultipleIndicator";
        avi= (AVLoadingIndicatorView) findViewById(R.id.avi);
        avi.setIndicator(indicator);
        avi.show();



        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_LOCATION_ASK);

        if (isFB){
            FacebookAnalytics.FBSimpleEvent(getActivity(), EventsFB.EVENT_SIGNUP_FB_LOCATION);
        }else {
            FacebookAnalytics.FBSimpleEvent(getActivity(), EventsFB.EVENT_SIGNUP_EMAIL_LOCATION);
        }


        findViewById(R.id.tx_skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (isFB){
                   FacebookAnalytics.FBEventsWhitSimpleParams(getActivity(), EventsFB.EVENT_SIGNUP_FB_LOCATION_PERMISSION,EventsFB.PARAM_VALUE_ACCEPTED);
               }else {
                   FacebookAnalytics.FBEventsWhitSimpleParams(getActivity(), EventsFB.EVENT_SIGNUP_EMAIL_LOCATION_PERMISSION,EventsFB.PARAM_VALUE_DENIED);
               }
                addFragmentToBackStack(FragmentRegisterInsertCode.getInstance(user),"insertCode",R.id.content_user_perfil);





            }
        });
//

        findViewById(R.id.bt_active_location).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isFB){
                    FacebookAnalytics.FBEventsWhitSimpleParams(getActivity(), EventsFB.EVENT_SIGNUP_FB_LOCATION_PERMISSION,EventsFB.PARAM_VALUE_ACCEPTED);
                }else {
                    FacebookAnalytics.FBEventsWhitSimpleParams(getActivity(), EventsFB.EVENT_SIGNUP_EMAIL_LOCATION_PERMISSION,EventsFB.PARAM_VALUE_ACCEPTED);
                }


                if(checkPermissions()){
                    validateLocationEnabled();
                }
            }
        });

    }

    public void validateLocationEnabled(){
        if(UrbanDevice.isLocationEnabled(getActivity())){
            addFragmentToBackStack(FragmentRegisterInsertCode.getInstance(user),"insertCode",R.id.content_user_perfil);
        }else{
            UrbanDevice.ActiveLocation(getActivity());
        }
    }










    // Callback with the request from calling requestPermissions(...)
    @Override
    public void onRequestPermissionsResult(int requestCode,  @NonNull String permissions[],  @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == READ_LOCATION_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                addFragmentToBackStack(FragmentRegisterInsertCode.getInstance(user),"insertCode",R.id.content_user_perfil);
            } else {
                // showRationale = false if user clicks Never Ask Again, otherwise true
                boolean showRationale = shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION);

                if (showRationale) {
                    // do something here to handle degraded mode
                } else {
                    Toast.makeText(getActivity(), "Permiso de Ubicación denegado", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
}

}
