package com.urbanity.android.user.forgotPassword;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.model.UrbanResponseError;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.network.ErrorUtils;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.utils.UrbanDevice;
import com.urbanity.android.utils.UrbanUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.urbanity.android.analytics.EventsFB.SCREEN_LOGIN_RETRIEVE;
import static com.urbanity.android.analytics.EventsFB.SCREEN_LOGIN_RETRIEVE_POP_POP;

/**
 * Created by DRM on 20/05/17.
 */

public class FragmentForgot extends BaseFragment {

    EditText ed_recovery_pass;
    UrbanUser user;
    public static FragmentForgot getInstance(){
        return new FragmentForgot();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forgot_password,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_LOGIN_RETRIEVE);
        ed_recovery_pass=(EditText)findViewById(R.id.ed_recovery_pass);

        if(UrbanPrefs.getInstance(getActivity()).isLoged()){
            user= UrbanDB.getInstance(getActivity()).getUrbanUser();
            ed_recovery_pass.setText(user.getEmail());
        }




        findViewById(R.id.bt_forgot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ed_recovery_pass.getText().toString().isEmpty()){
                    UrbanUtils.DialogSimpleOK(getActivity(),"Error","Ingresa tu contraseña");

                    return;
                }
                if(!UrbanUtils.isValidEmail(ed_recovery_pass.getText().toString())){
                    UrbanUtils.DialogSimpleOK(getActivity(),"Error","Ingresa un correo valido");

                    return;
                }

                Call<Boolean>recovery= RetrofitService.getUrbanService().postRecoveryPassword(ed_recovery_pass.getText().toString());
                recovery.enqueue(new Callback<Boolean>() {
                    @Override
                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                        if(response.body()){
                            showDialog();
                        }else {
                            UrbanResponseError error= ErrorUtils.parseError(response);
                            //No vienen  datos
                            UrbanUtils.DialogSimpleOK(getActivity(),"Error",error.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<Boolean> call, Throwable t) {
                        UrbanUtils.DialogSimpleOK(getActivity(),"Error","Ocurrio un error al intentar recuperar su contraseña");
                    }
                });
            }
        });
    }

    public void showDialog(){
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_LOGIN_RETRIEVE_POP_POP);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        builder.setTitle("Revisa tu correo");
        builder.setMessage("Te enviamos un correo para recuperar tu contraseña. Si no lo encuentras revisa en tu bandeja de Spam.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getActivity().onBackPressed();
            }
        });
     //   builder.setNegativeButton("Cancel", null);
        builder.show();
    }
}
