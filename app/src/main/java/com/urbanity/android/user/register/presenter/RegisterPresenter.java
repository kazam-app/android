package com.urbanity.android.user.register.presenter;

import android.content.Context;

import com.urbanity.android.model.UrbanUser;

/**
 * Created by DRM on 28/05/17.
 */

public interface RegisterPresenter {
    void validateUserData(Context context, String mail, String password, String rePassword);
    void validateUserData(Context context, UrbanUser user, String name, String lastname, String dob, int gender);
    void validateRegister(Context context,UrbanUser user);
}
