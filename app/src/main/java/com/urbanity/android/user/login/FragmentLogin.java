package com.urbanity.android.user.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.onBoarding.OnBoardingActivity;
import com.urbanity.android.onBoarding.OnBoardingType;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.user.PerfilUserActivity;
import com.urbanity.android.user.forgotPassword.FragmentForgot;
import com.urbanity.android.user.login.presenter.LoginPresenter;
import com.urbanity.android.user.login.presenter.LoginPresenterImp;
import com.urbanity.android.user.login.viewListener.LoginView;
import com.urbanity.android.user.register.FragmentRegisterPersonal;
import com.urbanity.android.utils.UrbanUtils;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.urbanity.android.analytics.EventsFB.SCREEN_LOGIN;

/**
 * Created by DRM on 20/05/17.
 */

public class FragmentLogin extends BaseFragment implements LoginView {

    LoginPresenter presenter;
    ProgressDialog pDialog;
    EditText inputUser, inputPasswor;


    LoginButton loginButton;


    public static FragmentLogin getInstance(){
        FragmentLogin fragment=new FragmentLogin();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle("Ingresar");
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_LOGIN);
        inputUser=(EditText)findViewById(R.id.input_user);
        inputPasswor=(EditText)findViewById(R.id.input_pass);

        pDialog= UrbanUtils.getDialogUrban(getActivity(),null,"Un momento por favor");
        presenter=new LoginPresenterImp(this);

        findViewById(R.id.bt_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.validateLogin(getActivity(),inputUser.getText().toString(),inputPasswor.getText().toString());
            }
        });



        findViewById(R.id.tx_forgot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragmentToBackStack(FragmentForgot.getInstance(),"olvidaPass",R.id.content_user_perfil);
            }
        });


        try {

            if(PerfilUserActivity.loginManager!=null){
                PerfilUserActivity.loginManager.logOut();
            }


        }catch (Exception ignore){}

        loginButton=(LoginButton)findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile, email"));

        loginButton.registerCallback(getActivity() instanceof MainActivity ?  MainActivity.callbackManager : PerfilUserActivity.callbackManager
                , new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult){
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        try {



                                            AccessToken token = AccessToken.getCurrentAccessToken();
                                            if (token != null) {
                                                UrbanUser user=new UrbanUser(object.getString("id"),token.getToken(),"18/08/2018");
                                                presenter.validateLoginFB(getActivity(),user);
                                            }

                                //            try{fbId = object.getString("id");}catch (JSONException e){e.printStackTrace();}
                                //            try{name = object.getString("first_name") !=null? object.getString("first_name") : "";}catch (JSONException e){e.printStackTrace();}
                                //            try{pictureFb = "https://graph.facebook.com/" + fbId + "/picture?type=large";}catch (Exception e){e.printStackTrace();}
                                //            try{lastname=object.getString("last_name");}catch (JSONException e){e.printStackTrace();}
                                //            try{email = object.getString("email")!=null ?  object.getString("email") : "";}catch (JSONException e){e.printStackTrace();}


                               //             PrefManager.getInstance(getActivity()).setFBIDV4(fbId);
                               //             presenter.validateLogin(getActivity(),"","",fbId,pictureFb);

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id ,first_name , email, gender, last_name");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }



                    @Override
                    public void onCancel() {
                        // Log.e("onCancel","onCancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                          Log.e("onError",""+error.getMessage());
                    }
                });

    }


    public static closeMainActivityListener mListener;

    public static void setListener(closeMainActivityListener listener){
        mListener=listener;
    }

    public interface closeMainActivityListener{
        void onCloseMainActivity();
    }



    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onShowProgress() {
        pDialog.show();
    }

    @Override
    public void onHideProgress() {
        pDialog.dismiss();
    }




    @Override
    public void onError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onSuccess(UrbanUser user) {


        HashMap<String,String> arg=new HashMap<>();

        String mDate=String.valueOf(user.getBirthdate());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");

        try {
            Calendar nowCalendar = Calendar.getInstance();
            Date newD = sdf.parse(mDate);
            long diff = nowCalendar.getTime().getTime() - newD.getTime();

            long  diffYears =   Math.round(diff / 1000 / 60 / 60 / 24 / 365);
            arg.put("user_age", String.valueOf(diffYears));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        arg.put("user_id",user.getUrbanId());
        arg.put("user_gender",user.getGender().equals("1")?"Male": "Female");
        arg.put("user_birthdate", user.getBirthdate() );
        if(user.getFb_id().isEmpty()){
            arg.put("signup_type","Email");
        }else {
            arg.put("signup_type","Facebook");
        }
        FacebookAnalytics.FBEventsSendProperties(getActivity(),arg);










        UrbanPrefs.getInstance(getActivity()).setIsLoged(true);
        UrbanDB.getInstance(getActivity()).addUrbanUser(user);
        if(mListener!=null){
            mListener.onCloseMainActivity();
        }
        startActivity(new Intent(getActivity(), OnBoardingActivity.class).putExtra(OnBoardingActivity.BOARDINGTYPE, OnBoardingType.APPDESC));
        getActivity().finish();
    }

    @Override
    public void onEmptyMail() {
        inputUser.setFocusableInTouchMode(true);
        inputUser.requestFocus();
        inputUser.setError("Ingresa tu correo");
    }

    @Override
    public void onEmptyPass() {
        inputPasswor.setFocusableInTouchMode(true);
        inputPasswor.requestFocus();
        inputPasswor.setError("Ingresa tu contraseña");
    }
}
