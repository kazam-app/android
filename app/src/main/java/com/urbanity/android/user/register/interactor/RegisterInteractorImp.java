package com.urbanity.android.user.register.interactor;

import android.content.Context;
import android.util.Log;

import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.model.UrbanResponseError;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.network.ErrorUtils;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.user.register.FragmentRegisterPersonal;
import com.urbanity.android.user.register.registerViewInterface.OnRegisterFinishListener;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DRM on 07/06/17.
 */

public class RegisterInteractorImp implements RegisterInteractor{
    @Override
    public void SendRegister(final Context context, final UrbanUser user, final OnRegisterFinishListener listener) {

        Call<UrbanUser> getRegister= RetrofitService.getUrbanService().getRegister(
                user.getName(),
                user.getLast_names(),
                user.getEmail(),
                user.getPassword(),
                user.getPassword(),
                Integer.parseInt(user.getGender()),
                user.getBirthdate(),
                user.getInvite_code(),
                user.getFb_id(),
                user.getFb_token(),
                "0000-00-00"
                //user.getFb_expires_at()
        );

        getRegister.enqueue(new Callback<UrbanUser>() {
            @Override
            public void onResponse(Call<UrbanUser> call, Response<UrbanUser> response) {

                if(response.code()==200){
                    listener.OnSuccessRegister(response.body());

                }else{
                   try{
                       UrbanResponseError error= ErrorUtils.parseError(response);
                       listener.OnErrorResponse(error.getMessage());
                   }catch (Exception e){
                       listener.OnErrorResponse("Error al registrar alguno de sus datos.");
                   }

                }
            }

            @Override
            public void onFailure(Call<UrbanUser> call, Throwable t) {
                listener.OnErrorResponse("Error al registrarse");
            }
        });

    }
}
