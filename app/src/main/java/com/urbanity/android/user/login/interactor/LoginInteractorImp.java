package com.urbanity.android.user.login.interactor;

import android.content.Context;
import android.util.Log;

import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.model.UrbanResponseError;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.network.ErrorUtils;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.user.login.viewListener.OnLoginFinished;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DRM on 22/05/17.
 */

public class LoginInteractorImp implements LoginInteractor {

    @Override
    public void OnLogin(final Context context, UrbanUser user, final OnLoginFinished listener) {

        Call<UrbanUser>getLogin= RetrofitService.getUrbanService().getLogin(user.getEmail(),user.getPassword());

        getLogin.enqueue(new Callback<UrbanUser>() {
            @Override
            public void onResponse(Call<UrbanUser> call, Response<UrbanUser> response) {
                if(response.body()!=null){
                    FacebookAnalytics.FBSimpleEvent(context,EventsFB.EVENT_LOGIN_EMAIL);
                    listener.OnSuccess(response.body());
                }else {
                    UrbanResponseError error= ErrorUtils.parseError(response);
                    //No vienen  datos
                    listener.OnError(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<UrbanUser> call, Throwable t) {
                listener.OnError("Error\nAlgo salio mal");
            }
        });




    }

    @Override
    public void OnLoginFB(final Context context, UrbanUser user, final OnLoginFinished listener) {

        Call<UrbanUser>getLoginFB=RetrofitService.getUrbanService().getLoginFB(user.getFb_id(),user.getFb_token(),user.getFb_expires_at());
        getLoginFB.enqueue(new Callback<UrbanUser>() {
            @Override
            public void onResponse(Call<UrbanUser> call, Response<UrbanUser> response) {
                if(response.body()!=null){
                    FacebookAnalytics.FBSimpleEvent(context,EventsFB.EVENT_LOGIN_FB);
                    listener.OnSuccess(response.body());
                }else {
                    listener.OnError("Error en inicio de sesion");
                }
            }

            @Override
            public void onFailure(Call<UrbanUser> call, Throwable t) {
                listener.OnError("Error\nAlgo salio mal");
            }
        });

    }
}
