package com.urbanity.android.user.changeMail;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.model.UrbanResponseError;
import com.urbanity.android.network.ErrorUtils;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.utils.UrbanUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DRM on 23/07/17.
 */

public class FragmentChangeMail extends BaseFragment {
    EditText ed_mail, ed_pass;

    public static FragmentChangeMail getInstance() {
        return new FragmentChangeMail();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_update_mail,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //TODO mandar a constants
        FacebookAnalytics. FireBaseScreenTracking(getActivity(),"Settings - Change Email");


        ed_mail=(EditText)findViewById(R.id.ed_mail);
        ed_pass=(EditText)findViewById(R.id.ed_pass);



        findViewById(R.id.bt_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO mandar a constants


                Call<Boolean>changeMail= RetrofitService.getUrbanService().changeMail(ed_mail.getText().toString(),ed_pass.getText().toString(), UrbanDB.getInstance(getActivity()).getUrbanUser().getUrbanToken());
                changeMail.enqueue(new Callback<Boolean>() {
                    @Override
                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                        if(response.body()!=null){
                           if(response.body()){
                               closeFragment();
                           }else {
                               UrbanUtils.DialogSimpleOK(getActivity(),"Confirma tu Correo","Algo salio mal");
                           }
                        }else {
                            UrbanResponseError error= ErrorUtils.parseError(response);
                            //No vienen  datos
                            UrbanUtils.DialogSimpleOK(getActivity(),"Confirma tu Correo",error.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<Boolean> call, Throwable t) {




                    }
                });


                FacebookAnalytics. FireBaseScreenTracking(getActivity(),"Settings - Change Email [Pop Up]");
            }
        });
    }


    public void closeFragment(){
        AlertDialog.Builder dialog=new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        dialog.setTitle("Confirma tu Correo");
        dialog.setMessage("Confirma tu correo con el link que mandamos a tu nueva dirección. Si tu correo no es confirmado en 24 horas, no se hará el cambio.");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getActivity().onBackPressed();

            }
        });

        dialog.show();
    }
}
