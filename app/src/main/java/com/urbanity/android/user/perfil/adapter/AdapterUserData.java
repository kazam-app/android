package com.urbanity.android.user.perfil.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.urbanity.android.R;
import com.urbanity.android.model.ItemMark;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.user.perfil.model.SettingCloseSession;
import com.urbanity.android.user.perfil.model.UserRegisterData;
import com.urbanity.android.user.perfil.model.UserSettingsTittle;
import com.urbanity.android.user.perfil.model.UserSettingsType;
import com.urbanity.android.user.perfil.model.UserSimpleSettings;

import java.util.List;

/**
 * Created by lenovo on 16/05/2017.
 */

public class AdapterUserData extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private Context context;
    private final int ITEMREGISTER=0, TITTLE=1, SIMPLEITEM=2, CLOSESESSION=3, EMOJIVIEW=4;
    List<Object> items;

    private int lastPosition = -1;





    public OnItemClickListener listener;
    public interface OnItemClickListener{
        void onItemClick(UserSettingsType type);
    }

    public void setData(Context context, List<Object> items, OnItemClickListener listener){
        this.context=context;
        this.items=items;
        this.listener=listener;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemUserRegisterView;
        View itemTittleView;
        View itemSimpleView;
        View itemCloseSession;
        View itemEmojiView;
        switch (viewType){

            case ITEMREGISTER:
                itemUserRegisterView=inflater.inflate(R.layout.row_settings_item_user_register,viewGroup,false);
                viewHolder=new ViewHolderItemUserRegister(itemUserRegisterView);
                break;

            case TITTLE:

                itemTittleView=inflater.inflate(R.layout.row_tittle_view,viewGroup,false);

                viewHolder=new ViewHolderItemTittle(itemTittleView);

                break;


            case SIMPLEITEM:
                itemSimpleView=inflater.inflate(R.layout.row_settings_simple_item,viewGroup,false);
                viewHolder=new ViewHolderSimpleItem(itemSimpleView);

                break;


            case CLOSESESSION:
                itemCloseSession=inflater.inflate(R.layout.row_close_session,viewGroup,false);
                viewHolder=new ViewHolderCloseSession(itemCloseSession);

                break;


            case EMOJIVIEW:
                itemEmojiView=inflater.inflate(R.layout.row_view_emojis,viewGroup,false);
                viewHolder=new ViewHolderEmojiView(itemEmojiView);
                break;

        }

        return viewHolder;
    }

    private class ViewHolderItemUserRegister extends RecyclerView.ViewHolder{

        TextView tx_row_label, tx_row_label_data;

        private ViewHolderItemUserRegister(View itemView){
            super(itemView);
            tx_row_label=(TextView)itemView.findViewById(R.id.tx_row_label);
            tx_row_label_data=(TextView)itemView.findViewById(R.id.tx_row_label_data);
        }
    }

    //*******TITTLE


    private class ViewHolderItemTittle extends RecyclerView.ViewHolder {
        TextView tx_label_title;

        private ViewHolderItemTittle(View itemView){
            super(itemView);
            tx_label_title=(TextView)itemView.findViewById(R.id.tx_label_title);
        }

    }




    //*******SIMPLE ITEM


    private class ViewHolderSimpleItem extends RecyclerView.ViewHolder{

        TextView tx_settings_simple_item;
        private ViewHolderSimpleItem(View itemView){
            super(itemView);
            tx_settings_simple_item=(TextView)itemView.findViewById(R.id.tx_settings_simple_item);
        }
    }


    //*******SIMPLE ITEM


    private class ViewHolderEmojiView extends RecyclerView.ViewHolder{
        private ViewHolderEmojiView(View itemView){
            super(itemView);
        }
    }



    private class ViewHolderCloseSession extends RecyclerView.ViewHolder{
        private ViewHolderCloseSession(View itemView){
            super(itemView);
        }
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolderItemUserRegister userRegister;
        ViewHolderItemTittle tittle;
        ViewHolderSimpleItem simpleItem;
        ViewHolderCloseSession closeSession;
        ViewHolderEmojiView emojiView;

      //  setAnimation(holder.itemView, position);

        switch (holder.getItemViewType()){

            case ITEMREGISTER:
                userRegister =(ViewHolderItemUserRegister) holder;
                configureViewHolderItemUserRegister(userRegister,position);
                break;



            case TITTLE:
                tittle =(ViewHolderItemTittle) holder;
                configureViewHolderTittle(tittle,position);
                break;



            case SIMPLEITEM:
                simpleItem =(ViewHolderSimpleItem) holder;
                configureViewHolderSimpleItem(simpleItem,position);
                break;

            case CLOSESESSION:
                closeSession =(ViewHolderCloseSession) holder;
                configureViewHolderCloseSession(closeSession,position);
                break;


            case EMOJIVIEW:
                emojiView =(ViewHolderEmojiView) holder;
                configureViewHolderEmojiView(emojiView,position);
                break;
        }
    }


    private void configureViewHolderItemUserRegister(final ViewHolderItemUserRegister holder, final int position) {
        final UserRegisterData user= (UserRegisterData) items.get(position);
        holder.tx_row_label.setText(user.getTittle());
        if(UrbanPrefs.getInstance(context).isLoged()){
            holder.tx_row_label_data.setText(user.getValue());

        }else {
            holder.tx_row_label_data.setText("");

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onItemClick(user.getType());
                }
            }
        });


    }

    //**** ConfigureTittle

    private void configureViewHolderTittle(final ViewHolderItemTittle holder, final int position) {
        UserSettingsTittle tittle= (UserSettingsTittle) items.get(position);
        holder.tx_label_title.setText(tittle.getTittle());
    }


    //**** Configure simple item

    private void configureViewHolderSimpleItem(final ViewHolderSimpleItem holder, final int position) {
        final UserSimpleSettings simpleSettings= (UserSimpleSettings) items.get(position);
        holder.tx_settings_simple_item.setText(simpleSettings.getTittle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onItemClick(simpleSettings.getType());
                }
            }
        });
    }


    private void configureViewHolderEmojiView(final ViewHolderEmojiView holder, final int position) {

    }



    //**** Configure Close session

    private void configureViewHolderCloseSession(final ViewHolderCloseSession holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onItemClick(UserSettingsType.CLOSESESSION);
                }
            }
        });


    }



    @Override
    public  int getItemViewType(int position) {

        if(items.get(position) instanceof UserRegisterData) {
            return ITEMREGISTER;
        }
        if(items.get(position) instanceof UserSettingsTittle){
            return TITTLE;
        }

        if(items.get(position) instanceof UserSimpleSettings){
            return SIMPLEITEM;
        }

        if(items.get(position) instanceof String){
            return EMOJIVIEW;
        }

        if(items.get(position) instanceof SettingCloseSession){
            return CLOSESESSION;
        }
        return position;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_up);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}










