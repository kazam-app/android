package com.urbanity.android.user.register.interactor;

import android.content.Context;

import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.user.register.registerViewInterface.OnRegisterFinishListener;

/**
 * Created by DRM on 07/06/17.
 */

public interface RegisterInteractor {

    void SendRegister(Context context, UrbanUser user, OnRegisterFinishListener listener);
}
