package com.urbanity.android.user.update;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.urbanity.android.R;
import com.urbanity.android.bases.BaseActivity;
import com.urbanity.android.customsViews.customDialog.SimpleCustomDialog;
import com.urbanity.android.model.TypeDialog;
import com.urbanity.android.model.TypeFieldEmpty;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.user.update.presenter.UpdateuserPresenter;
import com.urbanity.android.user.update.presenter.UpdateuserPresenterImp;
import com.urbanity.android.user.update.updateListener.UpdateView;
import com.urbanity.android.utils.UrbanUtils;

/**
 * Created by DRM on 07/07/17.
 */

public class ActivityUpdatePassword extends BaseActivity implements UpdateView{


    EditText ed_old_pass, ed_new_pass, ed_new_repass;
    Button bt_update_password;
    UpdateuserPresenter presenter;
    ProgressDialog pDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);
        setTitleToolbar("Cambiar Contraseña");

        ed_old_pass=(EditText)findViewById(R.id.ed_old_pass);
        ed_new_pass=(EditText)findViewById(R.id.ed_new_pass);
        ed_new_repass=(EditText)findViewById(R.id.ed_new_repass);
        bt_update_password=(Button)findViewById(R.id.bt_update_password);
        pDialog= UrbanUtils.getDialogUrban(this,null,"Actualizando contraseña");

        presenter=new UpdateuserPresenterImp(this);

        bt_update_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               presenter.validateUpdatePassword(ActivityUpdatePassword.this, ed_old_pass.getText().toString(), ed_new_pass.getText().toString(),ed_new_repass.getText().toString());
            }
        });

        findViewById(R.id.tx_forgot_pass).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityUpdatePassword.this,ActivityChangeMail.class).putExtra("type",ActivityChangeMail.TYPEPASS));
            }
        });

    }

    @Override
    public void showProgress() {
        pDialog.show();
    }

    @Override
    public void hideProgress() {
        pDialog.dismiss();
    }

    @Override
    public void OnSuccess(UrbanUser user) {

        SimpleCustomDialog dialog=new SimpleCustomDialog(ActivityUpdatePassword.this, this.getResources().getString(R.string.app_name), "Tu contraseña ha sido actualizada con éxito", "Aceptar", TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                dialog.dismiss();
                ActivityUpdatePassword.this.finish();
            }
        });
        dialog.show();

    }

    @Override
    public void OnError(String error) {
        UrbanUtils.DialogSimpleOK(ActivityUpdatePassword.this,"Error",error);
    }

    @Override
    public void OnErrorConnection() {

    }

    @Override
    public void OnEmptyField(TypeFieldEmpty fieldEmpty) {
        switch (fieldEmpty){
            case PASSWORD:
                UrbanUtils.DialogSimpleOK(ActivityUpdatePassword.this,"Campo vacio","Ingresa tu contraseña actual.");
                break;

            case NEWPASSWORD:
                UrbanUtils.DialogSimpleOK(ActivityUpdatePassword.this,"Campo vacio","Ingresa tu nueva contraseña.");

                break;

            case NEWREPASSWORD:
                UrbanUtils.DialogSimpleOK(ActivityUpdatePassword.this,"Campo vacio","Repite tu nueva contraseña.");

                break;

            case NOMATCHPASS:
                UrbanUtils.DialogSimpleOK(ActivityUpdatePassword.this,"Contraseña Invalida","La confirmación de la contraseña no coincide.");

                break;
        }
    }
}
