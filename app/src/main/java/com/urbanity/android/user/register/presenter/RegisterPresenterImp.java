package com.urbanity.android.user.register.presenter;

import android.content.Context;

import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.user.register.UserFieldType;
import com.urbanity.android.user.register.interactor.RegisterInteractor;
import com.urbanity.android.user.register.interactor.RegisterInteractorImp;
import com.urbanity.android.user.register.registerViewInterface.OnRegisterFinishListener;
import com.urbanity.android.user.register.registerViewInterface.RegisterView;
import com.urbanity.android.utils.UrbanDevice;
import com.urbanity.android.utils.UrbanUtils;

/**
 * Created by DRM on 28/05/17.
 */

public class RegisterPresenterImp implements RegisterPresenter , OnRegisterFinishListener {

    RegisterView view;
    RegisterInteractor interactor;
    public RegisterPresenterImp (RegisterView view){
        this.view=view;
        this.interactor=new RegisterInteractorImp();
    }

    @Override
    public void validateUserData(Context context, String mail, String password, String rePassword) {
        if(mail.isEmpty()){
            view.onEmptyField(UserFieldType.MAIL);
            return;
        }
        if(!UrbanUtils.isValidEmail(mail)){
            view.onEmptyField(UserFieldType.MAIL_FORMAT);
            return;
        }
        if(password.isEmpty()){
            view.onEmptyField(UserFieldType.PASS);
            return;
        }
        if(rePassword.isEmpty()){
            view.onEmptyField(UserFieldType.REPASS);
            return;
        }
        if(!rePassword.equals(password)){
            view.onError("Las contraseñas no coinciden");
        return;
        }

        view.onSuccess(new UrbanUser(mail,password));
    }

    @Override
    public void validateUserData(Context context, UrbanUser user, String name, String lastname, String dob, int gender) {
        if(name.isEmpty()){
            view.onEmptyField(UserFieldType.NAME);
            return;
        }
        if(lastname.isEmpty()){
            view.onEmptyField(UserFieldType.LASTNAME);
            return;
        }
        user.setName(name);
        user.setLast_names(lastname);
        user.setGender(String.valueOf(gender));
        user.setBirthdate(dob);
        view.onSuccess(user);
    }

    @Override
    public void validateRegister(Context context, UrbanUser user) {
        view.showProgress();
        interactor.SendRegister(context,user,this);
    }

    @Override
    public void OnErrorResponse(String error) {
        view.hideProgress();
        if(error.equals(String.valueOf("invalid_invite_code"))){
            view.onErrorInviteCode();
        }else {
            view.onError(error);
        }
    }


    @Override
    public void OnSuccessRegister(UrbanUser user) {
        view.hideProgress();
        view.onSuccess(user);
    }
}
