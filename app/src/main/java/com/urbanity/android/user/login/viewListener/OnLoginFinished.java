package com.urbanity.android.user.login.viewListener;

import com.urbanity.android.model.UrbanUser;

/**
 * Created by DRM on 22/05/17.
 */

public interface OnLoginFinished {
    void OnError(String error);
    void OnSuccess(UrbanUser user);
}
