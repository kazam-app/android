package com.urbanity.android.user.update.presenter;

import android.content.Context;

import com.urbanity.android.model.UrbanUser;

/**
 * Created by DRM on 06/07/17.
 */

public interface UpdateuserPresenter {

    void validateDataUser(Context context,UrbanUser user);
    void validateUpdatePassword(Context context,String oldPass, String newPAss, String newRePass);
}
