package com.urbanity.android.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.urbanity.android.R;
import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseActivity;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.user.login.FragmentLogin;
import com.urbanity.android.user.register.FragmentRegister;
import com.urbanity.android.user.register.FragmentRegisterPersonal;
import com.urbanity.android.utils.UrbanConstants;

/**
 * Created by DRM on 20/05/17.
 */



public class PerfilUserActivity extends BaseActivity {


    Toolbar mToolbar;
    public  static CallbackManager callbackManager;
    public static LoginManager loginManager;
    public static String TYPE="type";
    public static String USER="user";
    public int mType=-1;
    UrbanUser user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_perfil);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle("Registro");

        Bundle arg=getIntent().getExtras();
        if(arg!=null){
            mType=arg.getInt(TYPE);
            user= (UrbanUser) arg.getSerializable(USER);
        }
        callbackManager = CallbackManager.Factory.create();
        loginManager=LoginManager.getInstance();


        //Validar si es la primera vez que ingresa

        switch (mType){
            case UrbanConstants.REGISTER:
                FacebookAnalytics.FBSimpleEvent(PerfilUserActivity.this, EventsFB.EVENT_SIGNUP_EMAIL_CREDENTAILS);
                getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down).replace(R.id.content_user_perfil, FragmentRegister.getInstance()).commit();

                break;

            case UrbanConstants.LOGIN:
                getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down).replace(R.id.content_user_perfil, FragmentLogin.getInstance()).commit();
                break;

            case UrbanConstants.REGISTER_FB:

                FacebookAnalytics.FBSimpleEvent(PerfilUserActivity.this, EventsFB.EVENT_SIGNUP_FB_EXTRA_INFO);
                getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up, R.anim.slide_in_down, R.anim.slide_out_down).replace(R.id.content_user_perfil, FragmentRegisterPersonal.getInstance(user)).commit();
                break;


        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
