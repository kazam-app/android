package com.urbanity.android.user.register.registerViewInterface;

import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.user.register.UserFieldType;

/**
 * Created by DRM on 28/05/17.
 */

public interface RegisterView {

    void showProgress();
    void hideProgress();

    void onEmptyField(UserFieldType type);
    void onSuccess(UrbanUser user);
    void onError(String error);
    void onErrorInviteCode();
}
