package com.urbanity.android.user.register;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.urbanity.android.R;
import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.user.PerfilUserActivity;
import com.urbanity.android.user.register.presenter.RegisterPresenter;
import com.urbanity.android.user.register.presenter.RegisterPresenterImp;
import com.urbanity.android.user.register.registerViewInterface.RegisterView;

import static com.urbanity.android.analytics.EventsFB.SCREEN_REGISTER1;
import static com.urbanity.android.analytics.EventsFB.SCREEN_REGISTER2;

/**
 * Created by DRM on 20/05/17.
 */

public class FragmentRegister extends BaseFragment implements RegisterView {


    EditText inputRegisterMail, inputRegisterRepass ,inputRegisterPass ;
    RegisterPresenter presenter;
    public static FragmentRegister getInstance(){
        FragmentRegister fragment=new FragmentRegister();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle("Registro");
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_REGISTER2);


        inputRegisterMail=(EditText)findViewById(R.id.input_register_mail);
        inputRegisterPass=(EditText)findViewById(R.id.input_register_pass);
        inputRegisterRepass=(EditText)findViewById(R.id.input_register_repass);

        presenter=new RegisterPresenterImp(this);
        findViewById(R.id.bt_register_create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.validateUserData(getActivity(),inputRegisterMail.getText().toString(), inputRegisterPass.getText().toString(), inputRegisterRepass.getText().toString());
            }
        });
    }



    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onEmptyField(UserFieldType type) {
        switch (type){
            case MAIL:
                inputRegisterMail.setFocusableInTouchMode(true);
                inputRegisterMail.requestFocus();
                inputRegisterMail.setError("Ingresa tu correo");

                break;

            case PASS:
                inputRegisterPass.setFocusableInTouchMode(true);
                inputRegisterPass.requestFocus();
                inputRegisterPass.setError("Ingresa tu contraseña");
                break;

            case REPASS:
                inputRegisterRepass.setFocusableInTouchMode(true);
                inputRegisterRepass.requestFocus();
                inputRegisterRepass.setError("Repite tu contraseña");

                break;

            case MAIL_FORMAT:
                inputRegisterMail.setFocusableInTouchMode(true);
                inputRegisterMail.requestFocus();
                inputRegisterMail.setError("El formato del correo es incorrecto");
                break;
        }
    }

    @Override
    public void onSuccess(UrbanUser user) {
        user.setPassword(inputRegisterPass.getText().toString());
        user.setRepassword(inputRegisterPass.getText().toString());
        addFragmentToBackStack(FragmentRegisterPersonal.getInstance(user),"registerPersonal",R.id.content_user_perfil);
    }

    @Override
    public void onError(String error) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        builder.setMessage(error);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onErrorInviteCode() {

    }
}
