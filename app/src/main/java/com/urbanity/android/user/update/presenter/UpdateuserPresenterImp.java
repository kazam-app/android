package com.urbanity.android.user.update.presenter;

import android.content.Context;

import com.urbanity.android.model.TypeFieldEmpty;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.user.update.interactor.UpdateInteractor;
import com.urbanity.android.user.update.interactor.UpdateInteractorImpl;
import com.urbanity.android.user.update.updateListener.OnUpdateFinishListener;
import com.urbanity.android.user.update.updateListener.UpdateView;
import com.urbanity.android.utils.UrbanDevice;

/**
 * Created by DRM on 06/07/17.
 */

public class UpdateuserPresenterImp implements UpdateuserPresenter,OnUpdateFinishListener {


    UpdateView updateView;
    UpdateInteractor interactor;
    public UpdateuserPresenterImp(UpdateView updateView){
        this.updateView=updateView;
        this.interactor=new UpdateInteractorImpl();

    }

    @Override
    public void validateDataUser(Context context, UrbanUser user) {
        if(UrbanDevice.isConnected(context)){
            if(user.getName().isEmpty()){
                updateView.OnEmptyField(TypeFieldEmpty.NAME);

                return;
            }
            if(user.getLast_names().isEmpty()){
                updateView.OnEmptyField(TypeFieldEmpty.LASTNAME);
                return;
            }
            interactor.onUpdateDataUser(context, user,this);
            updateView.showProgress();

        }else {
            updateView.OnErrorConnection();
        }
    }

    @Override
    public void validateUpdatePassword(Context context, String oldPass, String newPAss, String newRePass) {
        if(oldPass.isEmpty()){
            updateView.OnEmptyField(TypeFieldEmpty.PASSWORD);
            return;
        }
        if(newPAss.isEmpty()){
            updateView.OnEmptyField(TypeFieldEmpty.NEWPASSWORD);
            return;
        }

        if(newRePass.isEmpty()){
            updateView.OnEmptyField(TypeFieldEmpty.NEWREPASSWORD);
            return;
        }
        if(!newPAss.equals(newRePass)){
            updateView.OnEmptyField(TypeFieldEmpty.NOMATCHPASS);
            return;
        }
        updateView.showProgress();
        interactor.onUpdatePassword(context,oldPass,newPAss, newRePass, this);
    }

    @Override
    public void onErrorListener(String error) {
        updateView.hideProgress();
        updateView.OnError(error);
    }

    @Override
    public void onSuccessListener(UrbanUser user) {
        updateView.hideProgress();
        updateView.OnSuccess(user);
    }
}
