package com.urbanity.android.user.perfil.model;

/**
 * Created by DRM on 04/06/17.
 */

public class UserRegisterData {
    private String tittle;
    protected String value;
    private UserSettingsType type;


    public UserRegisterData(String tittle, String value, UserSettingsType type) {
        this.tittle = tittle;
        this.value = value;
        this.type = type;
    }




    public String getTittle() {
        return tittle;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public UserSettingsType getType() {
        return type;
    }

    public void setType(UserSettingsType type) {
        this.type = type;
    }
}
