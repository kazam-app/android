package com.urbanity.android.user.register.registerViewInterface;

import com.urbanity.android.model.UrbanUser;

/**
 * Created by DRM on 07/06/17.
 */

public interface OnRegisterFinishListener {
    void OnErrorResponse(String error);
    void OnSuccessRegister(UrbanUser user);
}
