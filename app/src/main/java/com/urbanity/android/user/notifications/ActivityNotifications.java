package com.urbanity.android.user.notifications;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseActivity;

/**
 * Created by DRM on 26/07/17.
 */

public class ActivityNotifications extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        setTitleToolbar("Permisos de Notificaciones");

        //TODO mandar a constants
        FacebookAnalytics. FireBaseScreenTracking(this,"Settings - Notification Permissions");


    }
}
