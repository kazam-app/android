package com.urbanity.android.user.login.presenter;

import android.content.Context;

import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.user.login.interactor.LoginInteractor;
import com.urbanity.android.user.login.interactor.LoginInteractorImp;
import com.urbanity.android.user.login.viewListener.LoginView;
import com.urbanity.android.user.login.viewListener.OnLoginFinished;
import com.urbanity.android.utils.UrbanDevice;

/**
 * Created by DRM on 22/05/17.
 */

public class LoginPresenterImp implements LoginPresenter, OnLoginFinished {

    LoginInteractor interactor;
    LoginView loginView;

    public LoginPresenterImp(LoginView loginView){
        this.interactor=new LoginInteractorImp();
        this.loginView=loginView;
        }



    @Override
    public void validateLogin(Context context, String mail, String pass) {
        if(mail.isEmpty()){
            loginView.onEmptyMail();
            return;
        }
        if(pass.isEmpty()){
            loginView.onEmptyPass();
            return;
        }

        if(UrbanDevice.isConnected(context)){
            loginView.onShowProgress();
            interactor.OnLogin(context, new UrbanUser(mail,pass),this);
        }
    }

    @Override
    public void validateLoginFB(Context context, UrbanUser user) {
        if(UrbanDevice.isConnected(context)){
            loginView.onShowProgress();
            interactor.OnLoginFB(context,user,this);
        }
    }

    @Override
    public void OnError(String error) {
        loginView.onHideProgress();
        loginView.onError(error);
    }

    @Override
    public void OnSuccess(UrbanUser user) {
        loginView.onHideProgress();
        loginView.onSuccess(user);
    }
}