package com.urbanity.android.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.notifications.NotificationsManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.urbanity.android.ActivityUpdateApp;
import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseActivity;
import com.urbanity.android.model.ItemValidateVersion;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.onBoarding.OnBoardingActivity;
import com.urbanity.android.onBoarding.OnBoardingType;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.utils.UrbanDevice;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.urbanity.android.analytics.EventsFB.SCREEN_SPLASH;
/**
 * Created by DRM on 27/05/17.
 */

public class SplashActivity  extends BaseActivity{

    ImageView imgSplash;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        imgSplash=(ImageView)findViewById(R.id.img_splash);
        FacebookAnalytics.FireBaseScreenTracking(this,SCREEN_SPLASH);


        if(UrbanDevice.isConnected(this)){

            String token =  FirebaseInstanceId.getInstance().getToken();

            if(token!=null){
                try{
                    AppEventsLogger.setPushNotificationsRegistrationId(token);
                }catch (Exception e){

                }
            }

           //TODO Checar logica
            if(!UrbanPrefs.getInstance(SplashActivity.this).getNumVersion(). equals(UrbanDevice.getVersionApp(SplashActivity.this))){
                validateVersion();
                UrbanPrefs.getInstance(SplashActivity.this).setNumVersion(UrbanDevice.getVersionApp(SplashActivity.this));
            }else {
                launchNextActivity();
            }

        }else {
            launchNextActivity();
        }

    }



    public void launchNextActivity(){
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                SplashActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AppEventsLogger.setPushNotificationsRegistrationId(FirebaseInstanceId.getInstance().getToken());


                       if(UrbanPrefs.getInstance(SplashActivity.this).isRunAppFirst()){
                            startActivity(new Intent(SplashActivity.this, MainActivity.class));
                            SplashActivity.this.finish();
                            return;
                        }
                        UrbanPrefs.getInstance(SplashActivity.this).setRunAppFirst(true);
                        startActivity(new Intent(SplashActivity.this, OnBoardingActivity.class).putExtra(OnBoardingActivity.BOARDINGTYPE, OnBoardingType.MAIN));
                        SplashActivity.this.finish();

                    }
                });

            }
        };
        Timer timer = new Timer();
        timer.schedule(task, 1000);
    }




    void validateVersion(){
        Call<ItemValidateVersion> validateVersion= RetrofitService.getUrbanService().getValidateVersion(UrbanDevice.getVersionApp(SplashActivity.this),"android");
        validateVersion.enqueue(new Callback<ItemValidateVersion>() {
            @Override
            public void onResponse(Call<ItemValidateVersion> call, Response<ItemValidateVersion> response) {
                if(response.body()!=null){

                    if(response.body().isUpdate()){
                        startActivity(new Intent(SplashActivity.this, ActivityUpdateApp.class));
                        SplashActivity.this.finish();
                    } else {
                        launchNextActivity();
                    }


                }else {
                    launchNextActivity();
                }
            }

            @Override
            public void onFailure(Call<ItemValidateVersion> call, Throwable t) {
                Log.e("Error",t.getMessage()+"");
            }
        });
    }

}
