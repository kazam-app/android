package com.urbanity.android.customsViews.cristalViewPager;

import android.view.View;

/**
 * Created by DRM on 27/05/17.
 */

public class ParallaxPageTransformer extends BaseTransformer {

    private static final float FACTOR = 1.4f;

    public ParallaxPageTransformer(CrystalViewPager crystalViewPager) {
        super(crystalViewPager);
    }

    @Override
    protected void onTransform(View view, float position, int pageWidth, int pageHeight) {
        if (position >= 0) {
            view.setTranslationX(-pageWidth / FACTOR * position);
        }
    }
}
