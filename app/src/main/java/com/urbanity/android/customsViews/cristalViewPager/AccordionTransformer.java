package com.urbanity.android.customsViews.cristalViewPager;

import android.view.View;

/**
 * Created by DRM on 20/05/17.
 */

public class AccordionTransformer extends BaseTransformer {


    public AccordionTransformer(CrystalViewPager crystalViewPager) {
        super(crystalViewPager);
    }

    @Override
    protected void onTransform(View view, float position, int pageWidth, int pageHeight) {
        view.setTranslationX(pageWidth / 2 * -position);
        view.setScaleX(1 - Math.abs(position));
    }
}
