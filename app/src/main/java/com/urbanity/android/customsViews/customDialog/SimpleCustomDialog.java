package com.urbanity.android.customsViews.customDialog;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.urbanity.android.R;
import com.urbanity.android.model.TypeDialog;

/**
 * Created by DRM on 03/10/16.
 */
public class SimpleCustomDialog extends AlertDialog implements View.OnClickListener{

    String title;
    String message;
    OnButtonClickListener listener;
    TypeDialog type;

    String  titleAcept;
    Context context;
    String urlImg=null;




    public SimpleCustomDialog(Context context, String tiitle, String message, String titleAcept,TypeDialog type, OnButtonClickListener lister){
        super(context);
        this.title = tiitle;
        this.listener = lister;
        this.titleAcept=titleAcept;
        this.message=message;
        this.context=context;
        this.type=type;
    }

    public SimpleCustomDialog(Context context, String tiitle, String message, String titleAcept,String urlImg, TypeDialog type, OnButtonClickListener lister){
        super(context);
        this.title = tiitle;
        this.listener = lister;
        this.titleAcept=titleAcept;
        this.message=message;
        this.context=context;
        this.type=type;
        this.urlImg=urlImg;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
        setContentView(R.layout.custom_alert_dialog);


        findViewById(R.id.accept).setOnClickListener(this);
        findViewById(R.id.cancel).setOnClickListener(this);
        try{((TextView)findViewById(R.id.title)).setText(title);}catch (Exception e){e.printStackTrace();}
        try{((TextView)findViewById(R.id.message)).setText(message);}catch (Exception e){e.printStackTrace();}
        try{(findViewById(R.id.cancel)).setVisibility(View.GONE);}catch (Exception e){e.printStackTrace();}




        if(titleAcept!=null){
            ((Button)findViewById(R.id.accept)).setText(titleAcept);
        }

        ImageView circleImageView=(ImageView)findViewById(R.id.img_dialog);
        if(urlImg!=null)
            Picasso.with(context).load(urlImg).into(circleImageView);



        switch (type){
            case OK:
//               circleImageView.setBackgroundResource(R.drawable.shape);
                break;

            case CANCEL:
  //              circleImageView.setBackgroundResource(R.drawable.ic_msg_ok);
                break;
        }


        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onClick(View view) {
        if (listener!=null){
            if (view.getId() == R.id.accept)
                listener.onAcceptClickListener((Button) view, this);
        }else {
            dismiss();
        }
    }

    public interface OnButtonClickListener{
         void onAcceptClickListener(Button button, AlertDialog dialog);
    }


}
