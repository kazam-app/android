package com.urbanity.android.customsViews.customDialog;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.urbanity.android.R;

/**
 * Created by DRM on 03/10/16.
 */
public class CustomDialog extends AlertDialog implements View.OnClickListener{

    String title;
    String message;
    OnButtonClickListener listener;
    View view;

    String titleCancel, titleAcept;
    Context context;



    public CustomDialog(Context context, String tiitle,String message, String titleCancel,String titleAcept, OnButtonClickListener lister){
        super(context);
        this.title = tiitle;
        this.listener = lister;
        this.titleAcept=titleAcept;
        this.titleCancel=titleCancel;
        this.message=message;
        this.context=context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
        setContentView(R.layout.custom_alert_dialog);


        findViewById(R.id.accept).setOnClickListener(this);
        findViewById(R.id.cancel).setOnClickListener(this);
        try{((TextView)findViewById(R.id.title)).setText(title);}catch (Exception e){e.printStackTrace();}
        try{((TextView)findViewById(R.id.message)).setText(message);}catch (Exception e){e.printStackTrace();}

        if (view!=null){
            ((RelativeLayout) findViewById(R.id.custom_view_container)).addView(view);
        }



        if(titleAcept!=null){
            ((Button)findViewById(R.id.accept)).setText(titleAcept);
            ((Button)findViewById(R.id.cancel)).setText(titleCancel);
        }

        ImageView circleImageView=(ImageView)findViewById(R.id.img_dialog);




        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onClick(View view) {
        if (listener!=null){
            if (view.getId() == R.id.accept)
                listener.onAcceptClickListener((Button) view, this);
            else
                listener.onCancelClickListener((Button) view, this);
        }else {
            dismiss();
        }
    }

    public interface OnButtonClickListener{
         void onAcceptClickListener(Button button, AlertDialog dialog);
         void onCancelClickListener(Button button, AlertDialog dialog);
    }


}
