package com.urbanity.android.network;

import com.urbanity.android.model.UrbanResponseError;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by lenovo on 13/06/2017.
 */

public class ErrorUtils {

    public static UrbanResponseError parseError(Response<?> response) {

        Converter<ResponseBody, UrbanResponseError> converter = RetrofitService.getRetrofit().responseBodyConverter(UrbanResponseError.class, new Annotation[0]);
        UrbanResponseError error;
        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new UrbanResponseError();
        }

        return error;
    }
}
