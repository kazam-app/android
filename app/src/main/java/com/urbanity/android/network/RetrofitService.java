package com.urbanity.android.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by lenovo on 16/05/2017.
 */

public class RetrofitService {

    public static EndPointService getUrbanService(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);


        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
// add your other interceptors …
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
// add logging as last interceptor
        httpClient.addInterceptor(logging);

        Retrofit retrofit =new Retrofit.Builder()
                .baseUrl("https://api.kazamapp.mx")
                //.baseUrl("http://api.urbanity.xyz:3500")
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(EndPointService.class);
    }



    public static Retrofit getRetrofit(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);


        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
// add your other interceptors …
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
// add logging as last interceptor
        httpClient.addInterceptor(logging);




        Retrofit retrofit =new Retrofit.Builder()
                .baseUrl("http://50.112.50.215:3500")
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }


}

