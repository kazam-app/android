package com.urbanity.android.network;

/**
 * Created by lenovo on 16/05/2017.
 */

import com.urbanity.android.model.ItemMerchant;
import com.urbanity.android.model.ItemNews;
import com.urbanity.android.model.ItemRedeem;
import com.urbanity.android.model.ItemRedeemResponse;
import com.urbanity.android.model.ItemRegion;
import com.urbanity.android.model.ItemValidateVersion;
import com.urbanity.android.model.MerchantNear;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.model.PunchCard;
import com.urbanity.android.model.UrbanUser;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by DRM on 15/10/16.
 */
public interface EndPointService {


    String GET_NEWS =  "/news/android";

    String POST_LOGIN =  "/v1/login";

    String POST_REGITER =  "/v1/sign_up";

    String GET_MERCHANTS =  "/v1/merchants";

    String GET_NEAR_MERCHANTS = "v1/shops/near";

    String GET_MERCHANTS_BY_ID = "/v1/merchants/{merchant_id}/shops";

    String PUT_UPDATE_USER = "/v1/profile";

    String PUT_UPDATE_PASSWORD = "/v1/profile/password";

    String POST_RECOVERY_PASSWORD= "/v1/recovery";

    String GET_BEACON_MERCHANT= "/v1/merchants/beacon";
    String GET_QR_MERCHANT= "/v1/merchants/qr/{uid}";
    String GET_PUNCH_CARD= "/v1/merchants/{merchant_id}/punch_cards";

    String POST_PUNCH_BEACON ="/v1/merchants/{merchant_id}/punch/{card_id}";

    String POST_PUNCH_QR = "/v1/merchants/{merchant_id}/punch/{card_id}";

    String POST_REDEEM_EXCHANGE_BEACON="/v1/merchants/{merchant_id}/redeem/{card_id}";


    String GET_ALL_PUNCH_CARD="/v1/punch_cards";


    String GET_PUNCH_CARD_READY="/v1/punch_cards/ready";


    String GET_LEGAL="/v1/{path_legal}";

    String GET_VALIDATE_VERSION="/v1/version";
    String GET_REGIONS =  "/v1/beacons/regions";

    String UPDATEMAIL = "/v1/profile/email";



    @GET(GET_LEGAL)
    Call<String[]>getLegalTerms(@Path("path_legal") String id
    );


    @GET(GET_NEWS)
    Call<ArrayList<ItemNews>> getNews();

    @FormUrlEncoded
    @POST(POST_LOGIN)
    Call<UrbanUser> getLogin(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST(POST_LOGIN)
    Call<UrbanUser> getLoginFB(@Field("fb_id") String fb_id, @Field("fb_token") String fb_token, @Field("fb_expires_at") String fb_expires_at);


    @POST(POST_REGITER)
    Call<UrbanUser> getRegister(@Query("user[name]") String name,
                             @Query("user[last_names]") String last_names,
                             @Query("user[email]") String email,
                             @Query("user[password]") String password,
                             @Query("user[password_confirmation]") String password_confirmation,
                             @Query("user[gender]") int gender,
                             @Query("user[birthdate]") String date,
                             @Query("user[invite_code]") String invite_code,
                             @Query("user[fb_id]") String fb_id,
                             @Query("user[fb_token]") String fb_token,
                             @Query("user[fb_expires_at]") String FBdateExpired
    );



    @PUT(PUT_UPDATE_USER)
    Call<UrbanUser> putUpdateUser(@Header("Authorization")String header,
                                  @Query("user[name]") String name,
                                  @Query("user[last_names]") String last_names,
                                  @Query("user[gender]") int gender,
                                  @Query("user[birthdate]") String date
    );

    @PUT(PUT_UPDATE_PASSWORD)
    Call<Boolean> putUpdatePassword(@Header("Authorization")String header,
                                  @Query("old_password") String old_password,
                                  @Query("password") String password,
                                  @Query("password_confirmation") String password_confirmation
    );


    @GET(GET_VALIDATE_VERSION)
    Call<ItemValidateVersion> getValidateVersion(@Query("version")String version, @Query("platform")String platform
    );




    @GET(GET_MERCHANTS)
    Call<ArrayList<Merchants>>getMerchantsList();


    @GET(GET_MERCHANTS_BY_ID)
    Call<ArrayList<ItemMerchant>>getMerchantByID(@Header("Authorization")String header,
                                                 @Path("merchant_id") String merchant_id,

                                                 @Query("lat")double lan,
                                                 @Query("lng")double lng
                                                 );


    @GET(GET_NEAR_MERCHANTS)
    Call<ArrayList<MerchantNear>>getNearMerchantsList(
                                                  @Query("lat")double lan,
                                                  @Query("lng")double lng);

    @FormUrlEncoded
    @POST(POST_RECOVERY_PASSWORD)
    Call<Boolean> postRecoveryPassword(@Field("email") String email);



    @FormUrlEncoded
    @POST(POST_PUNCH_BEACON)
    Call<ItemRedeem>getPunchBeacon(@Header("Authorization")String header,
                                   @Path("merchant_id") String merchant_id,
                                   @Path("card_id") String card_id,
                                   @Field("uid") String uid,
                                   @Field("major") String major,
                                   @Field("minor") String minor
    );


    @FormUrlEncoded
    @POST(POST_REDEEM_EXCHANGE_BEACON)
    Call<ItemRedeemResponse>postRedeemExchangeBeacon(@Header("Authorization")String header,
                                             @Path("merchant_id") String merchant_id,
                                             @Path("card_id") String card_id,
                                             @Field("uid") String uid,
                                             @Field("major") String major,
                                             @Field("minor") String minor
    );

    @FormUrlEncoded
    @POST(POST_REDEEM_EXCHANGE_BEACON)
    Call<ItemRedeemResponse>postRedeemExchangeQRBeacon(@Header("Authorization")String header,
                                                     @Path("merchant_id") String merchant_id,
                                                     @Path("card_id") String card_id,
                                                     @Field("token") String uid
    );












    @FormUrlEncoded
    @POST(POST_PUNCH_QR)
    Call<ItemRedeem>getPunchQR(@Header("Authorization")String header,
                                   @Path("merchant_id") String merchant_id,
                                   @Path("card_id") String card_id,
                                   @Field("token") String token
    );






    @GET(GET_ALL_PUNCH_CARD)
    Call<ArrayList<PunchCard>>getAllPunchCard(@Header("Authorization")String header
    );

    @GET(GET_PUNCH_CARD_READY)
    Call<ArrayList<PunchCard>>getPunchCardReady(@Header("Authorization")String header
    );






    @GET(GET_PUNCH_CARD)
    Call<ArrayList<PunchCard>>getPunchCards(@Header("Authorization")String header,
                                                   @Path("merchant_id") String id
                                                  );





    @GET(GET_BEACON_MERCHANT)
    Call<Merchants>getBeaconMerchant(
            @Header("Authorization")String header,
            @Query("uid")String uid,
            @Query("major")String major);

    @GET(GET_QR_MERCHANT)
    Call<Merchants>getQRMerchant(@Header("Authorization")String header,
                                    @Path("uid") String id
    );



  @GET(GET_REGIONS)
    Call<ArrayList<ItemRegion>> getRegions(@Header("Authorization")String header
  );


  @PUT(UPDATEMAIL)
    Call<Boolean>changeMail(
          @Query("user[email]") String newMail,
          @Query("user[password]") String password,
          @Header("Authorization")String header

  );


 @GET(PUT_UPDATE_USER)
    Call<UrbanUser>getUser(@Header("Authorization")String header);

}

