package com.urbanity.android.analytics;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.HashMap;

import static com.urbanity.android.analytics.EventsFB.PARAM_STATUS;

/**
 * Created by drm on 06/09/17.
 */

public class FacebookAnalytics {


    public static void FBSimpleEvent(Context context, String event){
        AppEventsLogger.newLogger(context).logEvent(event);
        FirebaseAnalytics.getInstance(context).logEvent(event,new Bundle());
    }


    public static void FBEventsSendProperties(Context context, HashMap<String,String>params){
        Bundle arg= new Bundle();
        for ( String key : params.keySet() ) {
            arg.putString(key,params.get(key));
            Log.e("Params"," -Key:" + key + "-Value :"+ params.get(key));
            FirebaseAnalytics.getInstance(context).setUserProperty(key, params.get(key));
        }
        AppEventsLogger.updateUserProperties(arg, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {

            }
        });






    }



    public static void FBEventsWhitParams(Context context, String event, HashMap<String,String>params){
        AppEventsLogger logger = AppEventsLogger.newLogger(context);
        Bundle arg= new Bundle();

        for ( String key : params.keySet() ) {
            arg.putString(key,params.get(key));
            Log.e("Params","Event:"+event +   " -Key:" + key + "-Value :"+ params.get(key));
        }
        logger.logEvent(event,arg);
        //FirebaseAnalytics
        FirebaseAnalytics.getInstance(context).logEvent(event,arg);
    }

    public static void FBEventsWhitSimpleParams(Context context, String event,String value) {
        AppEventsLogger logger = AppEventsLogger.newLogger(context);
        Bundle arg = new Bundle();
        arg.putString(PARAM_STATUS, value);
        logger.logEvent(event, arg);

        //FirebaseAnalytics
        FirebaseAnalytics.getInstance(context).logEvent(event,arg);
    }









    public static void FireBaseScreenTracking(Activity context, String screenName ){
        FirebaseAnalytics.getInstance(context).setCurrentScreen(context, screenName, null /* class override */);
    }


}
