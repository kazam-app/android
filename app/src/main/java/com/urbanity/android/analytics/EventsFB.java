package com.urbanity.android.analytics;

import java.lang.ref.SoftReference;

/**
 * Created by drm on 11/09/17.
 */

public class EventsFB {
    //Events

    public static String EVENT_APP_OPEN="app_open";
    public static String EVENT_APP_CLOSE="app_close";

    public static String EVENT_SIGNUP_FB_START="signup_fb_start";
    public static String EVENT_SIGNUP_FB_EXTRA_INFO="signup_fb_extra_info";
    public static String EVENT_SIGNUP_FB_LOCATION="signup_fb_location";
    public static String EVENT_SIGNUP_FB_LOCATION_PERMISSION="signup_fb_location_permission";
//    public static String EVENT_SIGNUP_FB_NOTIFICATION="signup_fb_notification";
//    public static String EVENT_SIGNUP_FB_NOTIFICATION_PERMISSION="signup_fb_notification_permission";
    public static String EVENT_SIGNUP_FB_INVITE_CODE="signup_fb_invite_code";
    public static String EVENT_SIGNUP_FB_SUCCESS="signup_fb_success";


    public static String EVENT_SIGNUP_EMAIL_START="signup_email_start";
    public static String EVENT_SIGNUP_EMAIL_CREDENTAILS="signup_email_credentials";
    public static String EVENT_SIGNUP_EMAIL_EXTRA_INFO="signup_email_extra_info";
    public static String EVENT_SIGNUP_EMAIL_LOCATION="signup_email_location";
    public static String EVENT_SIGNUP_EMAIL_LOCATION_PERMISSION="signup_email_location_permission";
    //public static String EVENT_SIGNUP_EMAIL_NOTIFICATION="signup_email_notification";
   // public static String EVENT_SIGNUP_EMAIL_NOTIFICATION_PERMISSION="signup_email_notification_permission";
    public static String EVENT_SIGNUP_EMAIL_INVITE_CODE="signup_email_invite_code";
    public static String EVENT_SIGNUP_EMAIL_SUCCESS="signup_email_success";


    public static String EVENT_LOGIN_FB = "login_fb";
    public static String EVENT_LOGIN_EMAIL= "login_email";
    public static String EVENT_LOGOUT= "logout";

    public static String EVENT_MERCHANT_DETECTED = "merchant_detected";

    public static String EVENT_PUNCH_LIMIT_REACHED = "punch_limit_reached";


    public static String EVENT_PUNCHCARD_TRY_PUNCH = "punchcard_try_punch";
    public static String EVENT_PUNCHCARD_PUNCHED="punchcard_punched";
    public static String EVENT_PUNCHCARD_TRY_REDEEM = "punchcard_try_redeem";
    public static String EVENT_PUNCHCARD_REDEEMED = "punchcard_redeemed";


    public static String EVENT_MERCHANT_CATEGORY = "category";


    public static String PARAM_VALIDATION_TYPE= "validation_type";

    public static String PARAM_PUNCHCARD_USER_ID= "user_id";
    public static String PARAM_PUNCHCARD_MERCHANT_NAME="merchant_name";
    public static String PARAM_PUNCHCARD_MERCHANT_ID= "merchant_id";
    public static String PARAM_PUNCHCARD_ID="punchcard_id";
    public static String PARAM_PUNCHCARD_CATEGORY = "category_name";
    public static String PARAM_SCREEN_SOURCE ="screen_source";


    public static String PARAM_STATUS="status"; //accepted, denied
    public static String PARAM_GENDER="user_gender"; //male, female
    public static String PARAM_DOB="birthdate"; //birthdate
    public static String PARAM_AGE="user_age"; //birthdate

    public static String PARAM_VALUE_ACCEPTED="accepted";
    public static String PARAM_VALUE_DENIED="denied";





    public static String SCREEN_SPLASH="Splash Screen";
    public static String SCREEN_UPDATE="Update App";
    public static String SCREEN_REGISTER1="Signup - Email Credentials";
    public static String SCREEN_REGISTER2="Signup - Extra Info [Email]";
    public static String SCREEN_REGISTER3="Signup - Extra Info [FB]";
    public static String SCREEN_LOCATION_ASK="Signup - Location Ask";
    public static String SCREEN_INVITE_CODE="Signup - Invite Code";
    public static String SCREEN_ERROR_INVITE_CODE="Signup - Invite Code [Invalid Code]";
    public static String SCREEN_SUCCESS_INVITE_CODE="Signup - Invite Code [Valid Code]";
    public static String SCREEN_LOGIN="Log In";
    public static String SCREEN_LOGIN_RETRIEVE="Log In - Retrieve Password";
    public static String SCREEN_LOGIN_RETRIEVE_POP_POP="Log In - Retrieve Password [Pop Up]";

    public static String SCREEN_CAMERA ="Interaction Screen - Camera";
    public static String SCREEN_MERCHANT_DETECTED ="Interaction Screen - Merchant Detected";
    public static String SCREEN_MERCHANT_DETECTED_PUNCH_ERROR =   "Interaction Screen - Merchant Detected [Punch Error]";
    public static String SCREEN_INTERACTION_NO_LOCATION  ="Interaction Screen - Standard [No Location]";
    public static String SCREEN_VALIDATION_BLUETOOTH ="Validation Screen - Bluetooth";
    public static String SCREEN_VALIDATION_CAMERA="Validation Screen - Camera";
    public static String SCREEN_VALIDATION_BLUETOOTH_ERROR ="Interaction Screen - Merchant Detected [Redeem Error]";
    public static String SCREEN_SETTINGS_TOS ="Settings - ToS";
    public static String SCREEN_SETTINGS_FAQ ="Settings - FAQ";
    public static String SCREEN_SETTINGS_PP=  "Settings - PP";
    public static String SCREEN_MERCHANT_PROFILE_NO_CARD = "Merchant Profile - Punch Cards [No Card]";
    public static String SCREEN_MERCHANT_LIST_NO_NEARBY_STORES ="Merchant List - Near Me [No Nearby Stores]";






    public static String EVENT_WELCOME ="mo_welcome";
    public static String EVENT_ENTER_STORE ="mo_enter_store";
    public static String EVENT_PUNCH_CARD = "mo_punch_card";
    public static String EVENT_GET_PRIZES = "mo_get_prizes";


    public static String EVENT_HOW_WORK  ="io_how_it_works";
    public static String EVENT_STAMP_CARD="io_stamp_cards";
    public static String EVENT_IO_VALIDATE="io_validate";
    public static String EVENT_IO_GET_PRIZE= "io_get_prize";


    public static String EVENT_SCREEN_WELCOME="Main Onboarding - Welcome";
    public static String EVENT_SCREEN_ENTER_STORE="Main Onboarding - Enter Store";
    public static String EVENT_SCREEN_YOUR_CARDS="Main Onboarding - Punch Your Cards";
    public static String EVENT_SCREEN_PRIZE="Main Onboarding - Get Prizes";


    public static String EVENT_SCREEN_HOW_WORK  ="Interaction Onboarding - How it Works";
    public static String EVENT_SCREEN_STAMP_CARD="Interaction Onboarding - Stamp Your Card";
    public static String EVENT_SCREEN_IO_VALIDATE="Interaction Onboarding - Validate";
    public static String EVENT_SCREEN_IO_GET_PRIZE= "Interaction Onboarding - Get Your Prize";





}
