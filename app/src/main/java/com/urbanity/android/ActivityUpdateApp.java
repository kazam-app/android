package com.urbanity.android;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseActivity;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.onBoarding.OnBoardingActivity;
import com.urbanity.android.onBoarding.OnBoardingType;
import com.urbanity.android.storage.UrbanPrefs;

import static com.urbanity.android.analytics.EventsFB.SCREEN_UPDATE;

/**
 * Created by DRM on 26/07/17.
 */

public class ActivityUpdateApp extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_app);
        FacebookAnalytics.FireBaseScreenTracking(this,SCREEN_UPDATE);


        findViewById(R.id.bt_update_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.urbanity.android")));
            }
        });

        findViewById(R.id.img_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchNextActivity();
                ActivityUpdateApp.this.finish();
            }
        });

        findViewById(R.id.tx_latter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchNextActivity();
                ActivityUpdateApp.this.finish();
            }
        });
        
    }
    
    void launchNextActivity(){
        if(UrbanPrefs.getInstance(ActivityUpdateApp.this).isRunAppFirst()){
            startActivity(new Intent(ActivityUpdateApp.this, MainActivity.class));
            ActivityUpdateApp.this.finish();
            return;
        }
        UrbanPrefs.getInstance(ActivityUpdateApp.this).setRunAppFirst(true);
        startActivity(new Intent(ActivityUpdateApp.this, OnBoardingActivity.class).putExtra(OnBoardingActivity.BOARDINGTYPE, OnBoardingType.MAIN));
        finish();
    }
}
