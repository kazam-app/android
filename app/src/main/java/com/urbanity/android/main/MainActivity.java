package com.urbanity.android.main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.facebook.notifications.NotificationsManager;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.urbanity.android.R;
import com.urbanity.android.UrbanApplication;
import com.urbanity.android.customsViews.customDialog.SimpleCustomDialog;
import com.urbanity.android.interaction.FragmentMainInteraction;
import com.urbanity.android.interaction.beacon.search.BeaconView;
import com.urbanity.android.interaction.beacon.search.presenter.BeaconPresenter;
import com.urbanity.android.interaction.beacon.search.presenter.BeaconPresenterImp;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.model.TypeDialog;
import com.urbanity.android.reward.RewardActivity;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.trademarks.TrademarkActivity;
import com.urbanity.android.user.login.FragmentLogin;
import com.urbanity.android.user.register.FragmentRegisterInsertCode;
import com.urbanity.android.utils.UrbanUtils;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity implements FragmentLogin.closeMainActivityListener, FragmentRegisterInsertCode.closeMainActivityListener , BeaconView {


    ImageView imgUrban;

    ImageView imgMarks, imgRewards;
    TextView tx_news, tx_perfil;
    
    
    
    
    ProgressDialog pDialog;
    BeaconPresenter presenter;
    public static boolean showNews=false;

    public static boolean canChangeFragment=true;
    public  static CallbackManager callbackManager;
    public static LoginManager loginManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NotificationsManager.presentCardFromNotification(this);

        pDialog= UrbanUtils.getDialogUrban(this,null,"Un momento por favor");
        presenter=new BeaconPresenterImp(this);

         imgUrban=(ImageView)findViewById(R.id.img_main_urban);

        imgMarks=(ImageView)findViewById(R.id.imgMarks);
        imgRewards=(ImageView)findViewById(R.id.imgRewards);
        tx_news=(TextView )findViewById(R.id.tx_news);
        tx_perfil=(TextView )findViewById(R.id.tx_perfil);


         callbackManager = CallbackManager.Factory.create();
         loginManager=LoginManager.getInstance();

         FragmentLogin.setListener(this);
         FragmentRegisterInsertCode.setListener(this);

         changeFragment();

        findViewById(R.id.ly_perfil).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,RewardActivity.class));
            }
        });

        imgUrban.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(FragmentMainInteraction.sawPerfil==1){
                    FragmentMainInteraction.sawPerfil=0;
                    getSupportFragmentManager().beginTransaction().replace(R.id.content_main, FragmentMainInteraction.getInstance()).commit();
                }
            }
        });

        findViewById(R.id.ly_news).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canChangeFragment=false;
                startActivity(new Intent(MainActivity.this,TrademarkActivity.class));


            }
        });

            TrademarkActivity.setListener(new TrademarkActivity.ListenerEvent() {

                @Override
                public void changeFragmentEventMain() {
                    changeFragment();
                }

            });

            RewardActivity.setListener(new RewardActivity.ListenerEvent() {

                @Override
                public void changeFragmentEventMain() {
                    changeFragment();
                }

            });
       }


    @Override
    protected void onResume() {
        super.onResume();
        if(FragmentMainInteraction.sawPerfil==2){
            FragmentMainInteraction.sawPerfil=0;
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, FragmentMainInteraction.getInstance()).commit();
        }
    }

    public void changeFragment(){

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        canChangeFragment=true;
                        removeAllFragments();
                        try{
                            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, FragmentMainInteraction.getInstance()).commit();
                        }catch (Exception e){}
                    }
                });
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, 100);



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);


        //Instance of FragmentInteractionStates
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "No se encontraron resultados", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data
                try {
                    presenter.onValidateQR(MainActivity.this,result.getContents());
                } catch (Exception e) {
                    e.printStackTrace();
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }


    }
    @Override
    protected void onPause() {
        super.onPause();
        //removeAllFragments();
    }
    @Override
    public void onCloseMainActivity() {
        MainActivity.this.finish();
    }
    public void removeAllFragments(){
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    //****************
    @Override
    public void ShowProgress() {
        pDialog.show();
    }

    @Override
    public void HideProgress() {
        pDialog.dismiss();
    }

    @Override
    public void onErrorNetwork() {

        SimpleCustomDialog dialog=new SimpleCustomDialog(MainActivity.this, this.getResources().getString(R.string.app_name), "Se encontro Conexión a internet", "OK", TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void Retry() {
        Log.e("Retry","Retry");
    }

    @Override
    public void onError(String error) {
        SimpleCustomDialog dialog=new SimpleCustomDialog(MainActivity.this, this.getResources().getString(R.string.app_name), error, "OK", TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onSuccess(Merchants response) {
        if(mListener!=null){
            mListener.onQRListenerSuccess(response);
        }
    }

    @Override
    public void onSessionExpired() {
        SimpleCustomDialog dialog=new SimpleCustomDialog(MainActivity.this, getResources().getString(R.string.error_session), getResources().getString(R.string.error_session_message), getResources().getString(R.string.buttonAccept), TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                UrbanApplication.getInstance().clearApplicationData();
                UrbanDB.getInstance(MainActivity.this).CloseSession();
                UrbanPrefs.getInstance(MainActivity.this).ClearPrefs();
                UrbanPrefs.getInstance(MainActivity.this).setIsLoged(false);
                UrbanPrefs.getInstance(MainActivity.this).setRunAppFirst(false);
                startActivity(new Intent(MainActivity.this, MainActivity.class));
                MainActivity.this.finish();

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    public static  OnQRListener mListener;
    public static void setListenerQR(OnQRListener listener){
        mListener = listener;
    }
    public interface OnQRListener{
        void onQRListenerSuccess(Merchants response);
    }

}
