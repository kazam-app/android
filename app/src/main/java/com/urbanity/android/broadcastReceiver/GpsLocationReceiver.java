package com.urbanity.android.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
//import android.location.Location;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.urbanity.android.utils.UrbanDevice;

/**
 * Created by DRM on 17/07/17.
 */

public class GpsLocationReceiver extends BroadcastReceiver implements LocationListener {

@Override
public void onReceive(Context context, Intent intent){


        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")){

            if(UrbanDevice.isLocationEnabled(context)){
                if(mListener!=null){
                    mListener.onLocationEnabled(true);
                }
                Log.e("LOCATION","ON ");
            }else
                Log.e("LOCATION","OFF ");
            if(mListener!=null){
                mListener.onLocationEnabled(false);
            }
            }
        }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("LOCATION","CHANGE ");
    }

    public static OnLocationListener mListener;
    public static void setNetworkListener(OnLocationListener listener){
        mListener=listener;
    }
    public interface OnLocationListener{
        void onLocationEnabled(boolean state);
    }
}
