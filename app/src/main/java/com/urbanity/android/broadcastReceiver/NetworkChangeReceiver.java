package com.urbanity.android.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
/**
 * Created by DRM on 25/09/15.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {

        int status = NetworkUtil.getConnectivityStatusString(context);
        Log.e("Sulod reciever", "Sulod sa network reciever");
        if (!"android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            if(status==NetworkUtil.NETWORK_STATUS_NOT_CONNECTED){
                Log.e("NetworkChangeReceiver", "No Conectado ");
                if(mListener!=null){
                    mListener.onNetWorkConnected(false);
                }
            }else{
                Log.e("NetworkChangeReceiver", "Conectado ");
                if(mListener!=null){
                    mListener.onNetWorkConnected(true);
                }
            }
        }
    }


    public static OnNetworkListener mListener;
    public static void setNetworkListener(OnNetworkListener listener){
        mListener=listener;
    }
    public interface OnNetworkListener{
        void onNetWorkConnected(boolean state);
    }
}