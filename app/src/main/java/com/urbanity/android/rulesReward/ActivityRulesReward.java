package com.urbanity.android.rulesReward;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseActivity;

/**
 * Created by DRM on 26/07/17.
 */

public class ActivityRulesReward extends BaseActivity {

    String date,rules, cond;
    TextView tx_date_expire, tx_rules,tx_cond;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules_reward);

        //TODO mandar a contants
        FacebookAnalytics.FireBaseScreenTracking(this,"Punch Cards - Terms and Conditions");


        setTitleToolbar("Vencimiento y Condiciones");
        tx_date_expire=(TextView)findViewById(R.id.tx_date_expire);
        tx_rules=(TextView)findViewById(R.id.tx_rules);
        tx_cond=(TextView)findViewById(R.id.tx_cond);
        Bundle arg=getIntent().getExtras();
        if(arg!=null){
            date=arg.getString("date");
            rules=arg.getString("rules");
            cond=arg.getString("term");
            tx_date_expire.setText(date);
            tx_rules.setText(rules);
            tx_cond.setText(cond);

    }



    }
}
