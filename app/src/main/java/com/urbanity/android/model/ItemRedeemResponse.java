package com.urbanity.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by DRM on 16/07/17.
 */

public class ItemRedeemResponse  implements Serializable{

    @SerializedName("merchant_id")
    private String merchant_id;

    @SerializedName("identity")
    private int identity;

    @SerializedName("name")
    private String name;

    @SerializedName("prize")
    private String prize;

    @SerializedName("rules")
    private String rules;

    @SerializedName("terms")
    private String terms;

    @SerializedName("punch_limit")
    private int punch_limit;

    @SerializedName("redeem_code")
    private String redeem_code;

    @SerializedName("expires_at")
    private String expires_at;

    @SerializedName("validation_date")
    String validation_date;



    private String idCard;
    private String startTime;
    private String endTime;


    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }



    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public int getIdentity() {
        return identity;
    }

    public void setIdentity(int identity) {
        this.identity = identity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public int getPunch_limit() {
        return punch_limit;
    }

    public void setPunch_limit(int punch_limit) {
        this.punch_limit = punch_limit;
    }

    public String getRedeem_code() {
        return redeem_code;
    }

    public void setRedeem_code(String redeem_code) {
        this.redeem_code = redeem_code;
    }

    public String getExpires_at() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            Date date = formatter.parse(expires_at);
            // Log.e("DATE Format", ""+formatter.format(date) ); //2017-08-15
            SimpleDateFormat formateador = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", Locale.getDefault());
            return  formateador.format(date);


        } catch (ParseException e) {
            e.printStackTrace();
        }



        return expires_at;
    }


    public String getValidation_date() {
        return validation_date;
    }

    public void setValidation_date(String validation_date) {
        this.validation_date = validation_date;
    }

    public void setExpires_at(String expires_at) {
        this.expires_at = expires_at;
    }
}
