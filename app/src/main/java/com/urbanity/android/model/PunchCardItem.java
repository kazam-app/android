package com.urbanity.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DRM on 13/07/17.
 */

public class PunchCardItem {

    @SerializedName("id")
    private int id;

    @SerializedName("prize")
    private String prize;

    @SerializedName("punch_count")
    private int punch_count;

    @SerializedName("punch_limit")
    private int punch_limit;

    @SerializedName("terms")
    private String terms;

    @SerializedName("rules")
    private String rules;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }

    public int getPunch_count() {
        return punch_count;
    }

    public void setPunch_count(int punch_count) {
        this.punch_count = punch_count;
    }

    public int getPunch_limit() {
        return punch_limit;
    }

    public void setPunch_limit(int punch_limit) {
        this.punch_limit = punch_limit;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }
}
