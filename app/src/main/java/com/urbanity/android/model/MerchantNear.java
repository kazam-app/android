package com.urbanity.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DRM on 12/06/17.
 */

public class MerchantNear {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("merchant_name")
    private String merchant_name;

    @SerializedName("merchant_image_url")
    private String merchant_image_url;

    @SerializedName("shop_cluster")
    private String shop_cluster;

    @SerializedName("category")
    private String category;

    @SerializedName("distance")
    private String distance;

    @SerializedName("merchant_budget_rating")
    private String merchant_budget_rating;

    @SerializedName("merchant_id")
    private String merchant_id;

    @SerializedName("merchant_color")
    private String merchant_color;

    public String getMerchant_color() {
        return merchant_color;
    }

    public void setMerchant_color(String merchant_color) {
        this.merchant_color = merchant_color;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getMerchant_budget_rating() {
        return merchant_budget_rating;
    }

    public void setMerchant_budget_rating(String merchant_budget_rating) {
        this.merchant_budget_rating = merchant_budget_rating;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public String getMerchant_image_url() {
        return merchant_image_url;
    }

    public void setMerchant_image_url(String merchant_image_url) {
        this.merchant_image_url = merchant_image_url;
    }

    public String getShop_cluster() {
        return shop_cluster;
    }

    public void setShop_cluster(String shop_cluster) {
        this.shop_cluster = shop_cluster;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
