package com.urbanity.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lenovo on 13/06/2017.
 */

public class UrbanResponseError {

    @SerializedName("code")
    private String code;

    @SerializedName("message")
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
