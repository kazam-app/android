package com.urbanity.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DRM on 14/07/17.
 */

public class ItemRedeem implements Serializable {

    @SerializedName("id")
    private String id;


    @SerializedName("identity")
    private int identity;

    @SerializedName("punch_count")
    private int punch_count;

    @SerializedName("punch_limit")
    private int punch_limit;

    @SerializedName("validation_date")
    String validation_date;

    public String getValidation_date() {
        return validation_date;
    }

    public void setValidation_date(String validation_date) {
        this.validation_date = validation_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIdentity() {
        return identity;
    }

    public void setIdentity(int identity) {
        this.identity = identity;
    }

    public int getPunch_count() {
        return punch_count;
    }

    public void setPunch_count(int punch_count) {
        this.punch_count = punch_count;
    }

    public int getPunch_limit() {
        return punch_limit;
    }

    public void setPunch_limit(int punch_limit) {
        this.punch_limit = punch_limit;
    }
}
