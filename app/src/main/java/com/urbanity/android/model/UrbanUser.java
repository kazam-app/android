package com.urbanity.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lenovo on 06/06/2017.
 */

public class UrbanUser implements Serializable {

    @SerializedName("id")
    private String UrbanId;
    @SerializedName("name")
    private String name;
    @SerializedName("last_names")
    private String last_names;
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;
    @SerializedName("password_confirmation")
    private String repassword;


    @SerializedName("gender")
    private String gender;
    @SerializedName("birthdate")
    private String birthdate;
    @SerializedName("invite_code")
    private String invite_code="";


    @SerializedName("fb_id")
    private String fb_id="";
    @SerializedName("fb_token")
    private String fb_token="";
    @SerializedName("fb_expires_at")
    private String fb_expires_at="";

    @SerializedName("token")
    private String UrbanToken;

    @SerializedName("merchant")
    private MerchantInvite merchant;

    @SerializedName("created_at")
    private String created_at;


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUrbanToken() {
        return UrbanToken;
    }

    public void setUrbanToken(String urbanToken) {
        UrbanToken = urbanToken;
    }

    public MerchantInvite getMerchant() {
        return merchant;
    }

    public void setMerchant(MerchantInvite merchant) {
        this.merchant = merchant;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_names() {
        return last_names;
    }

    public void setLast_names(String last_names) {
        this.last_names = last_names;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepassword() {
        return repassword;
    }

    public void setRepassword(String repassword) {
        this.repassword = repassword;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getInvite_code() {
        return invite_code;
    }

    public void setInvite_code(String invite_code) {
        this.invite_code = invite_code;
    }

    public String getFb_id() {
        return fb_id;
    }

    public void setFb_id(String fb_id) {
        this.fb_id = fb_id;
    }

    public String getFb_token() {
        return fb_token;
    }

    public void setFb_token(String fb_token) {
        this.fb_token = fb_token;
    }

    public String getFb_expires_at() {
        return fb_expires_at;
    }

    public void setFb_expires_at(String fb_expires_at) {
        this.fb_expires_at = fb_expires_at;
    }

    public String getUrbanId() {
        return UrbanId;
    }

    public void setUrbanId(String urbanId) {
        UrbanId = urbanId;
    }

    //Constructor vacio
    public UrbanUser(){}


    //Constructor of login method
    public UrbanUser(String mail, String password){
        this.email=mail;
        this.password=password;
    }

    //Constructor of login method
    public UrbanUser(String fbId,String fb_token,String fb_expires_at){
        this.fb_id=fbId;
        this.fb_token=fb_token;
        this.fb_expires_at=fb_expires_at;
    }


    public class MerchantInvite{
        @SerializedName("id")
        private String id;

        @SerializedName("name")
        private String name;

        @SerializedName("image_url")
        private String image_url;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }
    }


}
