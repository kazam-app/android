package com.urbanity.android.model;

import java.io.Serializable;

/**
 * Created by drm on 15/11/17.
 */

public class ItemRegion implements Serializable {

    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
