package com.urbanity.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DRM on 24/05/17.
 */

public class ItemMark implements Serializable {

    @SerializedName("name")
    private String name;

    @SerializedName("address")
    private String address;

    @SerializedName("type")
    private String type;

    @SerializedName("price")
    private String price;

    @SerializedName("distance")
    private String distance;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
