package com.urbanity.android.model;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by lenovo on 15/06/2017.
 */

public class PunchCard implements Serializable {

    @SerializedName("id")
    private String id;

    @SerializedName("identity")
    private int identity;

    @SerializedName("prize")
    private String prize;

    @SerializedName("punch_count")
    private int punch_count;

    @SerializedName("punch_limit")
    private int punch_limit;

    @SerializedName("terms")
    private String terms;

    @SerializedName("rules")
    private String rules;


    //PUNCH READY
    @SerializedName("merchant_name")
    private String merchant_name;

    @SerializedName("merchant_image_url")
    private String merchant_image_url;


    //PUNCH ALL
    @SerializedName("merchant_color")
    private String merchant_color;

    @SerializedName("merchant_id")
    private String merchant_id;

    @SerializedName("category")
    private String category;

    @SerializedName("expires_at")
    private String expires_at;

    @SerializedName("validation_date")
    String validation_date;

    public String getValidation_date() {
        return validation_date;
    }

    public void setValidation_date(String validation_date) {
        this.validation_date = validation_date;
    }

    public String getExpires_at() {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
        try {
            Date date = formatter.parse(expires_at);
           // Log.e("DATE Format", ""+formatter.format(date) ); //2017-08-15
            SimpleDateFormat formateador = new SimpleDateFormat( "dd 'de' MMMM 'de' yyyy", Locale.getDefault());

            return  formateador.format(date);


        } catch (ParseException e) {
            e.printStackTrace();
        }



        return expires_at;
    }

    public void setExpires_at(String expires_at) {
        this.expires_at = expires_at;
    }

    public String getMerchant_color() {
        return merchant_color;
    }

    public void setMerchant_color(String merchant_color) {
        this.merchant_color = merchant_color;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public String getMerchant_image_url() {
        return merchant_image_url;
    }

    public void setMerchant_image_url(String merchant_image_url) {
        this.merchant_image_url = merchant_image_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIdentity() {
        return identity;
    }

    public void setIdentity(int identity) {
        this.identity = identity;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }

    public int getPunch_count() {
        return punch_count;
    }

    public void setPunch_count(int punch_count) {
        this.punch_count = punch_count;
    }

    public int getPunch_limit() {
        return punch_limit;
    }

    public void setPunch_limit(int punch_limit) {
        this.punch_limit = punch_limit;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }
}
