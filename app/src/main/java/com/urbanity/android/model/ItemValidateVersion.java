package com.urbanity.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DRM on 26/07/17.
 */

public class ItemValidateVersion implements Serializable {
    @SerializedName("update")
    boolean update;

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }
}
