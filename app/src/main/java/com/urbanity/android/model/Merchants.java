package com.urbanity.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lenovo on 09/06/2017.
 */

public class Merchants implements Serializable {

    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("image_url")
    private String image_url;
    @SerializedName("category")
    private String category;
    @SerializedName("budget_rating")
    private String budget_rating;
    @SerializedName("color")
    private String color;

    /*
    * @SerializedName("id")
    private String id;


    @SerializedName("name")
    private String name;

    @SerializedName("image_url")
    private String image_url;

    @SerializedName("category")
    private String category;

    @SerializedName("budget_rating")
    private String budget_rating;

    @SerializedName("color")
    private String color;*/


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBudget_rating() {
        return budget_rating;
    }

    public void setBudget_rating(String budget_rating) {
        this.budget_rating = budget_rating;
    }
}
