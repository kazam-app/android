package com.urbanity.android.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DRM on 19/07/17.
 */

public class ItemMerchant {

    @SerializedName("id")
    private int id;

    @SerializedName("distance")
    private String distance;

    @SerializedName("name")
    private String name;

    @SerializedName("merchant_image_url")
    private String merchant_image_url;

    @SerializedName("merchant_budget_rating")
    private int merchant_budget_rating;

    @SerializedName("shop_cluster")
    private String shop_cluster;

    @SerializedName("category")
    private String category;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMerchant_image_url() {
        return merchant_image_url;
    }

    public void setMerchant_image_url(String merchant_image_url) {
        this.merchant_image_url = merchant_image_url;
    }

    public int getMerchant_budget_rating() {
        return merchant_budget_rating;
    }

    public void setMerchant_budget_rating(int merchant_budget_rating) {
        this.merchant_budget_rating = merchant_budget_rating;
    }

    public String getShop_cluster() {
        return shop_cluster;
    }

    public void setShop_cluster(String shop_cluster) {
        this.shop_cluster = shop_cluster;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
