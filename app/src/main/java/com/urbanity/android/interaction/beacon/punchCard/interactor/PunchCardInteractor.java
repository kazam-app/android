package com.urbanity.android.interaction.beacon.punchCard.interactor;

import android.content.Context;

import com.urbanity.android.interaction.beacon.punchCard.PunchCardFinishListener;
import com.urbanity.android.interaction.beacon.search.OnBeaconFinishListener;

/**
 * Created by DRM on 13/07/17.
 */

public interface PunchCardInteractor {
    void OnSearchPunchCardMerchant(Context context, String merchant_id, PunchCardFinishListener listener);
}
