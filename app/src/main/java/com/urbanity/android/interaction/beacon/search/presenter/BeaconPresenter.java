package com.urbanity.android.interaction.beacon.search.presenter;

import android.content.Context;

/**
 * Created by DRM on 12/07/17.
 */

public interface BeaconPresenter {
    void onValidateBeacon(Context context, String uid,String major);
    void onValidateQR(Context context, String uid);
}
