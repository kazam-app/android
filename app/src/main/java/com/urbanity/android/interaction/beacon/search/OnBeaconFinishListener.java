package com.urbanity.android.interaction.beacon.search;

import com.urbanity.android.model.Merchants;

/**
 * Created by DRM on 12/07/17.
 */

public interface OnBeaconFinishListener {

    void onError(String error);
    void onSuccess(Merchants response);
    void onRetry();

    void onSessionExpired();
}
