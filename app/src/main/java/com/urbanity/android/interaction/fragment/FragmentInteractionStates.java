package com.urbanity.android.interaction.fragment;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.zxing.integration.android.IntentIntegrator;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.device.BeaconRegion;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory;
import com.kontakt.sdk.android.ble.manager.listeners.EddystoneListener;
import com.kontakt.sdk.android.ble.manager.listeners.IBeaconListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.EddystoneSpaceListener;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;
import com.kontakt.sdk.android.common.profile.IEddystoneDevice;
import com.kontakt.sdk.android.common.profile.IEddystoneNamespace;
import com.urbanity.android.R;
import com.urbanity.android.UrbanApplication;
import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.customsViews.customDialog.SimpleCustomDialog;
import com.urbanity.android.interaction.beacon.search.BeaconView;
import com.urbanity.android.interaction.beacon.search.presenter.BeaconPresenter;
import com.urbanity.android.interaction.beacon.search.presenter.BeaconPresenterImp;
import com.urbanity.android.model.ItemRegion;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.model.TypeDialog;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.urbanity.android.analytics.EventsFB.SCREEN_CAMERA;
import static com.urbanity.android.analytics.EventsFB.SCREEN_MERCHANT_DETECTED_PUNCH_ERROR;

/**
 * Created by DRM on 04/06/17.
 */

public class FragmentInteractionStates extends BaseFragment implements  BeaconView{

    LinearLayout insertViews;
    private IntentIntegrator qrScan;
    private final String TAG = "BeaconReferenceApp";
    BeaconPresenter presenter;



    private ProximityManager proximityManager;

    boolean isReady=true;
    boolean canChange=true;

    public static ArrayList<ItemRegion>allRegions;


    public static FragmentInteractionStates getInstance(){
        return new FragmentInteractionStates();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_interaction_states,container,false);
    }






    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        insertViews=(LinearLayout)findViewById(R.id.insert_views);
//TODO mandar a constants
        FacebookAnalytics.FBSimpleEvent(getActivity(), "Interaction Screen - Standard");
        presenter=new BeaconPresenterImp(this);
        qrScan = new IntentIntegrator(getActivity());
        findViewById(R.id.bt_scan_code).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO mandar a contants
                FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_CAMERA);
                qrScan.initiateScan();
            }
        });
        MainActivity.setListenerQR(new MainActivity.OnQRListener() {
            @Override
            public void onQRListenerSuccess(final Merchants response) {

                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                HashMap<String,String> params=new HashMap<>();
                                params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_NAME,response.getName());
                                params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_ID,response.getId());
                                params.put("source","camera");//Bluetooth/camera


                                //TODO mandar a contants
                                FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_MERCHANT_DETECTED,params);

                                if(canChange){
                                    replaceFragment(FragmentSuccessResponse.getInstance(response),R.id.content_interaction);
                                }



                            }
                        });
                    }
                };
                Timer timer = new Timer();
                timer.schedule(task, 80);

            }
        });



    }


    @Override
    public void onStart() {
        super.onStart();
        proximityManager = ProximityManagerFactory.create(getActivity());
        proximityManager.configuration()
                .monitoringEnabled(true)
                .monitoringSyncInterval(10)
                .deviceUpdateCallbackInterval(TimeUnit.SECONDS.toMillis(2));


        if(allRegions==null){
            allRegions = new ArrayList<>();
            Log.e("Inicializa", "All regions");
        }

        if(allRegions.size()==0){
            Call<ArrayList<ItemRegion>> getRegions= RetrofitService.getUrbanService().getRegions(UrbanDB.getInstance(getActivity()).getUrbanUser().getUrbanToken());
            getRegions.enqueue(new Callback<ArrayList<ItemRegion>>() {
                @Override
                public void onResponse(Call<ArrayList<ItemRegion>> call, Response<ArrayList<ItemRegion>> response) {
                    if(response.body()!=null){

                        if(response.body().size()>0){
                            allRegions = response.body();

                            ArrayList<IBeaconRegion>regions=new ArrayList<>();
                            for (int i=0; i<response.body().size(); i++){
                                IBeaconRegion region  = new BeaconRegion.Builder()
                                        .identifier("My region")
                                        .secureProximity(UUID.fromString(response.body().get(i).getUid()))
                                        .build();

                                regions.add(region);
                                Log.e("Agrega Region :", response.body().get(i).getUid());
                            }

                            proximityManager.spaces().iBeaconRegions(regions);


                        }
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<ItemRegion>> call, Throwable t) {

                }
            });
        }




        proximityManager.setIBeaconListener(new IBeaconListener() {
            @Override
            public void onIBeaconDiscovered(IBeaconDevice ibeacon, IBeaconRegion region) {
                Log.e("Sample", "IBeacon DATA" );
                Log.e("Sample", "IBeacon discovered Name: " + ibeacon.getName());
                Log.e("Sample", "IBeacon discovered Major: " + ibeacon.getMajor());
                Log.e("Sample", "IBeacon discovered Minor: " + ibeacon.getMinor());
                Log.e("Sample", "IBeacon discovered Proximity: " + ibeacon.getProximity());
                Log.e("Sample", "IBeacon discovered UniqueId: " + ibeacon.getUniqueId());
                Log.e("Sample", "IBeacon discovered ProximityUUID: " + ibeacon.getProximityUUID());
                Log.e("Sample", "IBeacon discovered Distance: " + ibeacon.getDistance());
            }

            @Override
            public void onIBeaconsUpdated( List<IBeaconDevice> ibeacon, IBeaconRegion region) {
                if(ibeacon.size()>0){
                    for (int i=0; i<ibeacon.size(); i++){
                        if(ibeacon.get(i).getDistance() < 3.3){
                            if(isReady){
                                isReady=false;
                                stopBeaconUnBind();
                                showBeacon(ibeacon.get(i));
                            }
                        }
                    }
                }
            }

            @Override
            public void onIBeaconLost(IBeaconDevice iBeacon, IBeaconRegion region) {

            }
        });


        proximityManager.setSpaceListener(new EddystoneSpaceListener() {
            @Override
            public void onNamespaceEntered(IEddystoneNamespace namespace) {
                //IBeacon region has been entered
            }

            @Override
            public void onNamespaceAbandoned(IEddystoneNamespace namespace) {
                //IBeacon region has been abandoned
            }
        });



        proximityManager.setEddystoneListener(new EddystoneListener() {
            @Override
            public void onEddystoneDiscovered(IEddystoneDevice eddystone, IEddystoneNamespace namespace) {
                //Profile discovered
            }

            @Override
            public void onEddystonesUpdated(List<IEddystoneDevice> eddystones, IEddystoneNamespace namespace) {
                //Profiles updated
            }

            @Override
            public void onEddystoneLost(IEddystoneDevice eddystone, IEddystoneNamespace namespace) {
                //Profile lost
            }
        });


    }
    @Override
    public void onResume() {
        super.onResume();
        canChange=true;
        startScanning();

    }



    @Override
    public void onDestroy() {
        super.onDestroy();
       if(proximityManager!=null){
           proximityManager.disconnect();
           proximityManager = null;
       }
    }

    @Override
    public void onStop() {
        super.onStop();
        proximityManager.stopScanning();
    }




    @Override
    public void onPause() {
        super.onPause();
        canChange=false;
        stopBeaconUnBind();

    }

    private void startScanning() {
        proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                if(proximityManager!=null){
                   if(proximityManager.isConnected()){
                       proximityManager.startScanning();
                   }
                }
            }
        });
    }


    public void stopBeaconUnBind(){
        if(proximityManager!=null){
            proximityManager.stopScanning();
        }

    }


    public void showBeacon(final IBeaconDevice beacon){

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    presenter.onValidateBeacon(getActivity(),String.valueOf(beacon.getProximityUUID()),String.valueOf(beacon.getMajor()));
                }

            });

    }





    @Override
    public void ShowProgress() {
        Log.e("Progress","Show");
    }

    @Override
    public void HideProgress() {
        Log.e("Progress","Dismiss");
    }

    @Override
    public void onErrorNetwork() {

        SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), getResources().getString(R.string.app_name), "No se encontro Conexión a internet", "OK", TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void Retry() {
        isReady=true;
        if(getActivity()!=null){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    TimerTask task = new TimerTask() {
                        @Override
                        public void run() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    startScanning();

                                }
                            });
                        }
                    };
                    Timer timer = new Timer();
                    timer.schedule(task, 80);



                }
            });
        }
    }

    @Override
    public void onError(String error) {
        if(canChange){
            SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), getResources().getString(R.string.app_name), error,  getResources().getString(R.string.buttonAccept), TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
                @Override
                public void onAcceptClickListener(Button button, AlertDialog dialog) {
                    dialog.dismiss();
                }
            });
            dialog.show();

            //TODO mandar a contants
            FacebookAnalytics.FireBaseScreenTracking(getActivity(), SCREEN_MERCHANT_DETECTED_PUNCH_ERROR );

        }
          }

    @Override
    public void onSuccess(Merchants response) {
        if(response!=null){
            if(FragmentSuccessResponse.getInstance(response)!=null){
                HashMap<String,String>params=new HashMap<>();

                params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_NAME,response.getName());
                params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_ID,response.getId());
                params.put("source","bluetooth");//Bluetooth/camera


                //TODO mandar a contants

                   FacebookAnalytics.FBEventsWhitParams(getActivity(),EventsFB.EVENT_MERCHANT_DETECTED,params);
                if(canChange){
                    replaceFragment(FragmentSuccessResponse.getInstance(response),R.id.content_interaction);
                }


            }
        }
    }

    @Override
    public void onSessionExpired() {
        SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), getResources().getString(R.string.error_session), getResources().getString(R.string.error_session_message), getResources().getString(R.string.buttonAccept), TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                UrbanApplication.getInstance().clearApplicationData();
                UrbanDB.getInstance(getActivity()).CloseSession();
                UrbanPrefs.getInstance(getActivity()).ClearPrefs();
                UrbanPrefs.getInstance(getActivity()).setIsLoged(false);
                UrbanPrefs.getInstance(getActivity()).setRunAppFirst(false);
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();

                dialog.dismiss();
            }
        });
        dialog.show();
    }
}


