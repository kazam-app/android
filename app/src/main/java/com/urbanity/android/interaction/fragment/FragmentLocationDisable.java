package com.urbanity.android.interaction.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.interaction.FragmentMainInteraction;
import com.urbanity.android.utils.UrbanDevice;
import com.urbanity.android.utils.UrbanUtils;

import static com.urbanity.android.analytics.EventsFB.SCREEN_CAMERA;
import static com.urbanity.android.analytics.EventsFB.SCREEN_INTERACTION_NO_LOCATION;

/**
 * Created by DRM on 02/07/17.
 */

public class FragmentLocationDisable extends BaseFragment {

    private IntentIntegrator qrScan;


    private static final int READ_LOCATION_PERMISSIONS_REQUEST = 1;


    public static FragmentLocationDisable getInstance(){
        return  new FragmentLocationDisable();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_interaction_location,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        qrScan = new IntentIntegrator(getActivity());
        //TODO mandar a constants
        FacebookAnalytics.FBSimpleEvent(getActivity(),  SCREEN_INTERACTION_NO_LOCATION);

        findViewById(R.id.bt_scan_code).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO mandar a contants
                FacebookAnalytics.FireBaseScreenTracking(getActivity(), SCREEN_CAMERA);
                qrScan.initiateScan();
                //Validate Bluetooth...
            }
        });

        findViewById(R.id.bt_active_location).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkPermissions()){
                    validateLocationEnabled();
                }

            }
        });
    }



    public boolean checkPermissions() {
        boolean resultP=true;
        // 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid
        // checking the build version since Context.checkSelfPermission(...) is only available
        // in Marshmallow
        // 2) Always check for permission (even if permission has already been granted)
        // since the user can revoke permissions at any time through Settings
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            resultP= false;
            // The permission is NOT already granted.
            // Check if the user has been asked about this permission already and denied
            // it. If so, we want to give more explanation about why the permission is needed.
            if (shouldShowRequestPermissionRationale( Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show our own UI to explain to the user why we need to read the contacts
                // before actually requesting the permission and showing the default UI
            }

            // Fire off an async request to actually get the permission
            // This will show the standard permission request dialog UI
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, READ_LOCATION_PERMISSIONS_REQUEST);
        }
        return resultP;
    }

    // Callback with the request from calling requestPermissions(...)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],  @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request

        if (requestCode == READ_LOCATION_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 &&  grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                validateLocationEnabled();

            } else {
                // showRationale = false if user clicks Never Ask Again, otherwise true
                boolean showRationale = shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION);

                if (showRationale) {
                    // do something here to handle degraded mode
                } else {
                    Toast.makeText(getActivity(), "Location permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    void validateLocationEnabled(){
        if(!UrbanDevice.isLocationEnabled(getActivity())){
            UrbanDevice.ActiveLocation(getActivity());
        }else {

           if(mListener!=null){
               mListener.onLocationAccepted();
           }


        }
    }

    public static LocationAcceptedListener mListener;

    public static void setListener(LocationAcceptedListener listener){
        mListener= listener;

    }

        public interface LocationAcceptedListener{
        void onLocationAccepted();
    }
}
