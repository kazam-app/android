package com.urbanity.android.interaction.beacon.search.presenter;

import android.content.Context;

import com.urbanity.android.interaction.beacon.search.BeaconView;
import com.urbanity.android.interaction.beacon.search.OnBeaconFinishListener;
import com.urbanity.android.interaction.beacon.search.interactor.BeaconInteractor;
import com.urbanity.android.interaction.beacon.search.interactor.BeaconInteractorImp;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.utils.UrbanDevice;

/**
 * Created by DRM on 12/07/17.
 */

public class BeaconPresenterImp implements BeaconPresenter,OnBeaconFinishListener {

    BeaconView view;
    BeaconInteractor interactor;

    public BeaconPresenterImp(BeaconView view){
        this.view=view;
        this.interactor=new BeaconInteractorImp();

    }

    @Override
    public void onValidateBeacon(Context context, String uid, String major) {
        if(!UrbanDevice.isConnected(context)){
            view.onErrorNetwork();
            return;
        }
        view.ShowProgress();
        interactor.onSendBeacon(context,uid,major,this);

    }

    @Override
    public void onValidateQR(Context context, String uid) {
        if(!UrbanDevice.isConnected(context)){
            view.onErrorNetwork();
            return;
        }
        view.ShowProgress();
        interactor.onSendQR(context,uid,this);
    }

    @Override
    public void onError(String error) {
        view.HideProgress();
        view.onError(error);
    }

    @Override
    public void onSuccess(Merchants response) {
        view.HideProgress();
        view.onSuccess(response);
    }

    @Override
    public void onRetry() {
        view.HideProgress();
        view.Retry();
    }

    @Override
    public void onSessionExpired() {
        view.HideProgress();
        view.onSessionExpired();

    }
}
