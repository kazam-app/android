package com.urbanity.android.interaction.beacon.punchCard.presenter;

import android.content.Context;

/**
 * Created by DRM on 13/07/17.
 */

public interface PunchCardPresenter {
    void validatePunchCardMerchant(Context context, String merchant_id);
}
