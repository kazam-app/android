package com.urbanity.android.interaction.beacon.punchCard.presenter;

import android.content.Context;

import com.urbanity.android.interaction.beacon.punchCard.PunchCardFinishListener;
import com.urbanity.android.interaction.beacon.punchCard.PunchCardView;
import com.urbanity.android.interaction.beacon.punchCard.interactor.PunchCardInteractor;
import com.urbanity.android.interaction.beacon.punchCard.interactor.PunchCardInteractorImp;
import com.urbanity.android.model.PunchCard;
import com.urbanity.android.utils.UrbanDevice;

import java.util.ArrayList;

/**
 * Created by DRM on 13/07/17.
 */

public class PunchCardPresenterImp implements PunchCardPresenter,PunchCardFinishListener {


    PunchCardView punchCardView;
    PunchCardInteractor interactor;
    public PunchCardPresenterImp(PunchCardView punchCardView){
        this.punchCardView=punchCardView;
        this.interactor=new PunchCardInteractorImp();
    }


    @Override
    public void validatePunchCardMerchant(Context context, String merchant_id) {
            if(UrbanDevice.isConnected(context)){
                interactor.OnSearchPunchCardMerchant(context,merchant_id,this);
                punchCardView.ShowProgress();
            }else {
                punchCardView.onErrorNetwork();
            }
    }

    @Override
    public void onError(String error) {
        punchCardView.HideProgress();
        punchCardView.onError(error);
    }

    @Override
    public void onSuccess(ArrayList<PunchCard> items) {
        punchCardView.HideProgress();
        punchCardView.onSuccess(items);
    }

    @Override
    public void onEmpty() {
        punchCardView.HideProgress();
        punchCardView.onEmptyPunchCard();
    }

    @Override
    public void onSessionExpired() {
        punchCardView.HideProgress();
        punchCardView.onSessionExpired();
    }
}
