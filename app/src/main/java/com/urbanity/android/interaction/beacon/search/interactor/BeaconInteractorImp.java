package com.urbanity.android.interaction.beacon.search.interactor;

import android.content.Context;

import com.urbanity.android.interaction.beacon.search.OnBeaconFinishListener;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.model.UrbanResponseError;
import com.urbanity.android.network.ErrorUtils;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.utils.UrbanConstants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DRM on 12/07/17.
 */

public class BeaconInteractorImp implements BeaconInteractor {


    @Override
    public void onSendBeacon(Context context, String uid, String major, final OnBeaconFinishListener listener) {

        Call<Merchants>getBeaconMerchant= RetrofitService.getUrbanService().getBeaconMerchant(UrbanDB.getInstance(context).getUrbanUser().getUrbanToken(),uid,major);
        getBeaconMerchant.enqueue(new Callback<Merchants>() {
            @Override
            public void onResponse(Call<Merchants> call, Response<Merchants> response) {

                if(response.code()== UrbanConstants.ERRORLOGOUT){
                    listener.onSessionExpired();
                    return;
                }

               if(response.code()==404){
                   listener.onRetry();
               }
               else
                if(response.body()!=null){
                    listener.onSuccess(response.body());
                }else {
                    UrbanResponseError error= ErrorUtils.parseError(response);
                    //No vienen  datos
                    listener.onError(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<Merchants> call, Throwable t) {
                listener.onError("No fue posible encontrar datos");
            }
        });


    }

    @Override
    public void onSendQR(Context context, String uid,final OnBeaconFinishListener listener) {
        Call<Merchants>getQRResponse=RetrofitService.getUrbanService().getQRMerchant(UrbanDB.getInstance(context).getUrbanUser().getUrbanToken(),uid);
        getQRResponse.enqueue(new Callback<Merchants>() {
            @Override
            public void onResponse(Call<Merchants> call, Response<Merchants> response) {


                if(response.code()== UrbanConstants.ERRORLOGOUT){
                    listener.onSessionExpired();
                    return;
                }


                if(response.body()!=null){
                    listener.onSuccess(response.body());
                }else {
                    UrbanResponseError error= ErrorUtils.parseError(response);
                    //No vienen  datos
                    listener.onError(error.getMessage());

                }
            }

            @Override
            public void onFailure(Call<Merchants> call, Throwable t) {
                listener.onError("No fue posible encontrar datos");
            }
        });
    }
}
