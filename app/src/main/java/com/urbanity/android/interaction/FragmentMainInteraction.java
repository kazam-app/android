package com.urbanity.android.interaction;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.urbanity.android.R;
import com.urbanity.android.UrbanApplication;
import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.broadcastReceiver.GpsLocationReceiver;
import com.urbanity.android.broadcastReceiver.NetworkChangeReceiver;
import com.urbanity.android.interaction.fragment.FragmentBluetoothDisable;
import com.urbanity.android.interaction.fragment.FragmentConnectionDisable;
import com.urbanity.android.interaction.fragment.FragmentInteractionRegister;
import com.urbanity.android.interaction.fragment.FragmentInteractionStates;
import com.urbanity.android.interaction.fragment.FragmentLocationDisable;
import com.urbanity.android.interaction.fragment.FragmentSuccessResponse;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.onBoarding.OnBoardingActivity;
import com.urbanity.android.onBoarding.OnBoardingType;
import com.urbanity.android.reward.RewardActivity;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.trademarks.TrademarkActivity;
import com.urbanity.android.user.perfil.FragmentPerfilDetail;
import com.urbanity.android.utils.UrbanDevice;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by DRM on 04/06/17.
 */

public class FragmentMainInteraction extends BaseFragment implements FragmentLocationDisable.LocationAcceptedListener {

    TextView txNameUser, txOldUser;
    boolean canChange=true;

    public static int sawPerfil=0;


    public static FragmentMainInteraction getInstance(){
        return new FragmentMainInteraction();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_interaction,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        txNameUser=(TextView)findViewById(R.id.tx_name_user);
        txOldUser =(TextView)findViewById(R.id.tx_oldUser);

        findViewById(R.id.img_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sawPerfil= 1;
                addFragmentToBackStackNoAnimation(FragmentPerfilDetail.getInstance(),"Perfil",R.id.content_main);
            }
        });


        findViewById(R.id.img_help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), OnBoardingActivity.class).putExtra(OnBoardingActivity.BOARDINGTYPE, OnBoardingType.APPDESC));
                getActivity().finish();
            }
        });

        //Valdar si esta loegeado
        if(UrbanPrefs.getInstance(getActivity()).isLoged()){
            txNameUser.setText(String.valueOf(UrbanDB.getInstance(getActivity()).getUrbanUser().getName() + "  "+ UrbanDB.getInstance(getActivity()).getUrbanUser().getLast_names()));
            txOldUser.setVisibility(View.VISIBLE);
            txOldUser.setText(String.valueOf("Miembro desde: "+UrbanPrefs.getInstance(getActivity()).getCreatedAt()));
            //Validar blutooth
            if(canChange)
             changeStatesApplication();


        }else {
            txOldUser.setVisibility(View.INVISIBLE);
            getChildFragmentManager().beginTransaction().replace(R.id.content_interaction, FragmentInteractionRegister.getInstance()).commit();
        }










            //*************CHANGE STATES
            UrbanApplication.setNetworkListener(new UrbanApplication.OnBluetoothListener() {
                @Override
                public void onBluetoothEnabled(boolean state) {

                   // if(canChange)
                    changeStatesApplication();


                }
            });

            GpsLocationReceiver.setNetworkListener(new GpsLocationReceiver.OnLocationListener() {
                @Override
                public void onLocationEnabled(boolean state) {

                    TimerTask task = new TimerTask() {
                        @Override
                        public void run() {

                            try{
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(canChange)
                                            changeStatesApplication();
                                    }
                                });
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }
                    };
                    Timer timer = new Timer();
                    timer.schedule(task, 600);

                }
            });

            NetworkChangeReceiver.setNetworkListener(new NetworkChangeReceiver.OnNetworkListener() {
                @Override
                public void onNetWorkConnected(boolean state) {

              if(canChange)
                    changeStatesApplication();


                }
            });

        FragmentLocationDisable.setListener(this);


        MainActivity.setListenerQR(new MainActivity.OnQRListener() {
            @Override
            public void onQRListenerSuccess(final Merchants response) {
                //*****CAMBIO NUEVO

                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                HashMap<String,String> params=new HashMap<>();
                                params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_NAME,response.getName());
                                params.put(EventsFB.PARAM_PUNCHCARD_MERCHANT_ID,response.getId());
                                params.put("source","camera");//Bluetooth/camera
                                FacebookAnalytics.FBEventsWhitParams(getActivity(), EventsFB.EVENT_MERCHANT_DETECTED,params);
                                getChildFragmentManager().beginTransaction().replace(R.id.content_interaction, FragmentSuccessResponse.getInstance(response)).commit();


                            }
                        });
                    }
                };
                Timer timer = new Timer();
                timer.schedule(task, 80);

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        canChange=true;
    }

    @Override
    public void onPause() {
        super.onPause();
        canChange=false;
    }

    public void changeStatesApplication(){

        if(MainActivity.canChangeFragment){
            if(UrbanPrefs.getInstance(getActivity()).isLoged()){
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                        if (mBluetoothAdapter != null) {
                            // Device does not support Bluetooth
                            if (!mBluetoothAdapter.isEnabled()) {
                                // Bluetooth is not enable :)
                                getChildFragmentManager().beginTransaction().replace(R.id.content_interaction, FragmentBluetoothDisable.getInstance()).commit();
                                return;
                            }
                        }

                        //Validar ubicacion

                        if(!UrbanDevice.isLocationEnabled(getActivity())  || ! UrbanDevice.checkLocationPermission(getActivity() )){
                            getChildFragmentManager().beginTransaction().replace(R.id.content_interaction, FragmentLocationDisable.getInstance()).commit();
                            return;
                        }

                        if(!UrbanDevice.isConnected(getActivity())){
                            getChildFragmentManager().beginTransaction().replace(R.id.content_interaction, FragmentConnectionDisable.getInstance(0)).commit();
                            return;
                        }
                            try{
                                getChildFragmentManager().beginTransaction().replace(R.id.content_interaction, FragmentInteractionStates.getInstance()).commit();

                            }catch (Exception e){
                                e.printStackTrace();
                            }

                    }
                });

            }else {

                getChildFragmentManager().beginTransaction().replace(R.id.content_interaction, FragmentInteractionRegister.getInstance()).commit();

            }

        }



    }

    /**
    *
    *
    * */


    @Override
    public void onLocationAccepted() {
        changeStatesApplication();
    }
}
