package com.urbanity.android.interaction.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.urbanity.android.R;
import com.urbanity.android.analytics.EventsFB;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.legal.ActivityLegalFAQ;
import com.urbanity.android.model.UrbanUser;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.user.PerfilUserActivity;
import com.urbanity.android.user.perfil.model.UserSettingsType;
import com.urbanity.android.utils.UrbanConstants;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by DRM on 04/06/17.
 */

public class FragmentInteractionRegister extends BaseFragment {

    LoginButton btFacebookLogin;

    UrbanUser user;
    public static FragmentInteractionRegister getInstance(){
        return new FragmentInteractionRegister();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_interaction_register,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        user=new UrbanUser();
        //TODO mandar a constants
        FacebookAnalytics.FBSimpleEvent(getActivity(), "Interaction Screen - No Account");

        //   String textHTML="Al crear tu cuenta estás aceptando nuestros <a href=\"http://www.google.com\">Términos y Condiciones</a> y <a href=\"http://www.google.com\">Política de Privacidad.</a>";
        /*if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            txTerm.setText(Html.fromHtml(textHTML, Html.FROM_HTML_MODE_COMPACT));
        }else {
            txTerm.setText(Html.fromHtml(textHTML));
        }
        txTerm.setMovementMethod(LinkMovementMethod.getInstance());
        */


        findViewById(R.id.tx_terms).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),ActivityLegalFAQ.class).putExtra(ActivityLegalFAQ.TERM, UserSettingsType.TERMS));
            }
        });

        findViewById(R.id.tx_pri).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),ActivityLegalFAQ.class).putExtra(ActivityLegalFAQ.TERM, UserSettingsType.PRIVS));
            }
        });




        findViewById(R.id.bt_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FacebookAnalytics.FBSimpleEvent(getActivity(), EventsFB.EVENT_SIGNUP_EMAIL_START);
                startActivity(new Intent(getActivity(), PerfilUserActivity.class).putExtra(PerfilUserActivity.TYPE, UrbanConstants.REGISTER));
            }
        });

        findViewById(R.id.tx_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), PerfilUserActivity.class).putExtra(PerfilUserActivity.TYPE,UrbanConstants.LOGIN));
            }
        });



        btFacebookLogin=(LoginButton)findViewById(R.id.bt_facebook_login);

        btFacebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FacebookAnalytics.FBSimpleEvent(getActivity(), EventsFB.EVENT_SIGNUP_FB_START);
            }
        });


        try {

            if(MainActivity.loginManager!=null){
                MainActivity.loginManager.logOut();
            }


        }catch (Exception ignore){}



        btFacebookLogin.setReadPermissions(Arrays.asList("public_profile, email"));

        btFacebookLogin.registerCallback(getActivity() instanceof MainActivity ?  MainActivity.callbackManager : PerfilUserActivity.callbackManager
                , new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult){
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        try {

                                            user.setFb_id(object.getString("id"));
                                            user.setName(object.getString("first_name"));
                                            user.setLast_names(object.getString("last_name"));

                                            final String OLD_FORMAT = "MM/dd/yyyy";
                                            final String NEW_FORMAT = "yyyy/MM/dd";
                                          //  String oldDateString = object.getString("birthday");
                                            String newDateString;

                                            try{

                                                SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
                                               // Date d = sdf.parse(oldDateString);
                                                sdf.applyPattern(NEW_FORMAT);
                                             //   newDateString = sdf.format(d);

                                                user.setBirthdate("");//user.setBirthdate(newDateString);
                                            }catch (Exception e){

                                            }


                                            user.setGender(object.getString("gender"));
                                            user.setEmail(object.getString("email")!=null ?  object.getString("email") : "");


                                            AccessToken token = AccessToken.getCurrentAccessToken();
                                            if (token != null) {
                                                user.setFb_expires_at(String.valueOf(token.getExpires()));
                                                user.setFb_token(token.getToken());
                                            }


                                            startActivity(new Intent(getActivity(), PerfilUserActivity.class).putExtra(PerfilUserActivity.TYPE, UrbanConstants.REGISTER_FB).putExtra(PerfilUserActivity.USER,user));





                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id ,first_name , email, gender,last_name");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }



                    @Override
                    public void onCancel() {
                         Log.e("onCancel","onCancel");
                    }

                    @Override
                    public void onError(FacebookException error) {
                         Log.e("onError",""+error.getMessage());
                    }
                });

    }
}
