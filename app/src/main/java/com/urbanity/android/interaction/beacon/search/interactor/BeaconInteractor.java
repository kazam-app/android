package com.urbanity.android.interaction.beacon.search.interactor;

import android.content.Context;

import com.urbanity.android.interaction.beacon.search.OnBeaconFinishListener;

/**
 * Created by DRM on 12/07/17.
 */

public interface BeaconInteractor {

    void onSendBeacon(Context context, String uid, String major, OnBeaconFinishListener listener);
    void onSendQR(Context context, String uid, OnBeaconFinishListener listener);

}
