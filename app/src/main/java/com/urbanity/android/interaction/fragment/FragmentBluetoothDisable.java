package com.urbanity.android.interaction.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.zxing.integration.android.IntentIntegrator;
import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;

import static com.urbanity.android.analytics.EventsFB.SCREEN_CAMERA;

/**
 * Created by DRM on 02/07/17.
 */



public class FragmentBluetoothDisable extends BaseFragment {

    private IntentIntegrator qrScan;


    public static FragmentBluetoothDisable getInstance(){
        return new FragmentBluetoothDisable();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_interaction_bluetooth,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        qrScan = new IntentIntegrator(getActivity());
        //TODO mandar a constants
        FacebookAnalytics.FBSimpleEvent(getActivity(), "Interaction Screen - Standard [No Bluetooth]");

        findViewById(R.id.bt_scan_code).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//TODO mandar a contants

                FacebookAnalytics.FireBaseScreenTracking(getActivity(),  SCREEN_CAMERA);
                qrScan.initiateScan();
                //Validate Bluetooth...
            }
        });

    }

}
