package com.urbanity.android.interaction.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.urbanity.android.R;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;

/**
 * Created by DRM on 02/07/17.
 */

public class FragmentConnectionDisable extends BaseFragment {

    TextView txRetry;
    public static FragmentConnectionDisable getInstance(int key){
        FragmentConnectionDisable fragment=new FragmentConnectionDisable();
        Bundle arg=new Bundle();
        arg.putInt("key",key);
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.view_interaction_no_internet,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //TODO mandar a constants
        FacebookAnalytics.FBSimpleEvent(getActivity(), "Interaction Screen - Standard [No Internet]");

        txRetry=(TextView)findViewById(R.id.tx_retry);
        if(getArguments().getInt("key")==1){
            txRetry.setVisibility(View.VISIBLE);
        }else {
            txRetry.setVisibility(View.GONE);
        }
        txRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getArguments().getInt("key")==1){
                    getActivity().onBackPressed();
                }
            }
        });
    }
}
