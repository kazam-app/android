package com.urbanity.android.interaction.beacon.search;

import com.urbanity.android.model.Merchants;

/**
 * Created by DRM on 12/07/17.
 */

public interface BeaconView {

    void ShowProgress();
    void HideProgress();
    void onErrorNetwork();

    void Retry();
    void onError(String error);
    void onSuccess(Merchants response);

    void onSessionExpired();



}
