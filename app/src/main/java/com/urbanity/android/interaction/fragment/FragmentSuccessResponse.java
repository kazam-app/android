package com.urbanity.android.interaction.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.urbanity.android.R;
import com.urbanity.android.UrbanApplication;
import com.urbanity.android.analytics.FacebookAnalytics;
import com.urbanity.android.bases.BaseFragment;
import com.urbanity.android.customsViews.cristalViewPager.CrystalViewPager;
import com.urbanity.android.customsViews.customDialog.SimpleCustomDialog;
import com.urbanity.android.interaction.beacon.punchCard.PunchCardView;
import com.urbanity.android.interaction.beacon.punchCard.presenter.PunchCardPresenter;
import com.urbanity.android.interaction.beacon.punchCard.presenter.PunchCardPresenterImp;
import com.urbanity.android.model.ItemRedeemResponse;
import com.urbanity.android.model.Merchants;
import com.urbanity.android.model.PunchCard;
import com.urbanity.android.main.MainActivity;
import com.urbanity.android.model.TypeDialog;
import com.urbanity.android.notify.FragmentEmptyPunchCard;
import com.urbanity.android.onBoarding.FragmentAdapter;
import com.urbanity.android.onBoarding.OnBoardingActivity;
import com.urbanity.android.onBoarding.OnBoardingType;
import com.urbanity.android.redeem.fragment.FragmentPunchCardExchange;
import com.urbanity.android.redeem.fragment.FragmentRedeemBluetooth;
import com.urbanity.android.redeem.fragment.FragmentScanQR;
import com.urbanity.android.splash.SplashActivity;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.storage.UrbanPrefs;
import com.urbanity.android.trademarks.perfil.fragment.CardMarkFragment;
import com.urbanity.android.utils.UrbanDevice;
import com.urbanity.android.utils.UrbanUtils;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.urbanity.android.analytics.EventsFB.SCREEN_MERCHANT_DETECTED;
import static com.urbanity.android.trademarks.perfil.fragment.CardMarkFragment.POSITIONCARD;
import static com.urbanity.android.utils.UrbanUtils.CARDANIMATIONEXCHANEGE;

/**
 * Created by DRM on 12/07/17.
 */

public class FragmentSuccessResponse extends BaseFragment implements PunchCardView, FragmentRedeemBluetooth.OnClearAdapterListener{

    public static String MERCHANT="MERCHANT";
    LinearLayout header_beacon_response;
    Animation animHeaderIn, animHeaderOut;
    ImageView img_perfil_mark;
    Merchants beacon;
    TextView tx_mark_name, tx_mark_category;
    CrystalViewPager viewPager;
    FragmentAdapter adapter;
    CirclePageIndicator indicator;
    PunchCardPresenter presenter;
    ProgressDialog pDialog;
    TextView tx_mark_price_on, tx_mark_price_off;
    LinearLayout contentPagerCards;
    ImageView img_close_cards;
    LinearLayout content_all_cars;


    public static FragmentSuccessResponse getInstance(Merchants response){
        FragmentSuccessResponse fragment=new FragmentSuccessResponse();
        Bundle args=new Bundle();
        args.putSerializable(MERCHANT,response);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_success_beacon_response,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FacebookAnalytics.FireBaseScreenTracking(getActivity(),SCREEN_MERCHANT_DETECTED);
        beacon=(Merchants) getArguments().getSerializable(MERCHANT);
        animHeaderIn= AnimationUtils.loadAnimation(getActivity(),R.anim.anim_header_merchat_in);
        animHeaderOut=AnimationUtils.loadAnimation(getActivity(),R.anim.anim_header_merchat_out);

        header_beacon_response=(LinearLayout)findViewById(R.id.header_beacon_response);
        header_beacon_response.startAnimation(animHeaderIn);
        img_perfil_mark=(ImageView)findViewById(R.id.img_perfil_mark);
        tx_mark_name=(TextView)findViewById(R.id.tx_mark_name);
        tx_mark_name.setText(beacon.getName());
        tx_mark_category=(TextView)findViewById(R.id.tx_mark_category);
        tx_mark_category.setText(beacon.getCategory());
        tx_mark_price_on=(TextView)findViewById(R.id.tx_mark_price_on);
        tx_mark_price_off=(TextView)findViewById(R.id.tx_mark_price_off);
        contentPagerCards=(LinearLayout)findViewById(R.id.content_pager);
        content_all_cars=(LinearLayout) findViewById(R.id.content_all_cars);

        img_close_cards=(ImageView)findViewById(R.id.img_close_cards);


           img_close_cards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                content_all_cars.startAnimation(animHeaderOut);
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((MainActivity)getActivity()).changeFragment();

                            }
                        });

                    }
                };
                Timer timer = new Timer();
                timer.schedule(task, 350);

            }
        });

        switch (Integer.parseInt(beacon.getBudget_rating())){
            case 0:
                tx_mark_price_on.setText("");
                tx_mark_price_off.setText("$$$$$");
                break;

            case 1:
                tx_mark_price_on.setText("$");
                tx_mark_price_off.setText("$$$$");
             break;

            case 2:
                tx_mark_price_on.setText("$$");
                tx_mark_price_off.setText("$$$");
                break;

            case 3:
                tx_mark_price_on.setText("$$$");
                tx_mark_price_off.setText("$$");
                break;

            case 4:
                tx_mark_price_on.setText("$$$$");
                tx_mark_price_off.setText("$");
                break;

            case 5:
                tx_mark_price_on.setText("$$$$$");
                tx_mark_price_off.setText("");
                break;
        }
        presenter=new PunchCardPresenterImp(this);
        pDialog= UrbanUtils.getDialogUrban(getActivity(),null,"Buscando PunchCards");
        FragmentRedeemBluetooth.setListener(this);
        Picasso.with(getActivity()).load(beacon.getImage_url()).placeholder(getActivity().getResources().getDrawable(R.drawable.ic_mark)).error(getActivity().getResources().getDrawable(R.drawable.ic_mark)).into(img_perfil_mark);

        findCards();


        FragmentScanQR.setListenerRefress(new FragmentScanQR.OnRefresCardsListener() {
            @Override
            public void OnRefresh() {
                ClearAdapterCard();
                findCards();
            }
        });

        CardMarkFragment.setListenerSize(new CardMarkFragment.ChangeSizeListener() {

            @Override
            public void onFullCard(final int position) {
                TimerTask task1 = new TimerTask() {
                    @Override
                    public void run() {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                viewPager.setCurrentItem(position+1);

                            }
                        });

                    }
                };
                Timer timer1 = new Timer();
                timer1.schedule(task1, 1000);
            }
        });
    }






    public void findCards(){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!MainActivity.showNews){
                    Drawable background =header_beacon_response.getBackground();
                    if(background instanceof GradientDrawable){
                        GradientDrawable shapeDrawable = (GradientDrawable) background;
                        if(beacon.getColor()!=null){
                            try{shapeDrawable.setColor(Color.parseColor(beacon.getColor()));}catch (Exception e){e.printStackTrace();}
                        }
                    }


                    presenter.validatePunchCardMerchant(getActivity(),beacon.getId());
                    viewPager= (CrystalViewPager) findViewById(R.id.viewpager);
                    adapter= new FragmentAdapter(getChildFragmentManager());
                    indicator=(CirclePageIndicator)findViewById(R.id.indicator);
                }
            }
        });
    }

    @Override
    public void ShowProgress() {
        pDialog.show();
    }

    @Override
    public void HideProgress() {
       if(pDialog!=null && pDialog.isShowing()){
           pDialog.dismiss();
       }
    }

    @Override
    public void onEmptyPunchCard() {
        adapter.addFragment(FragmentEmptyPunchCard.getInstance(String.valueOf(beacon.getName())));
        if(viewPager!=null){
            viewPager.setAdapter(adapter);
            indicator.setViewPager(viewPager);
            adapter.notifyDataSetChanged();
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccess(ArrayList<PunchCard> items) {
        for (int i=0; i<items.size(); i++){
            adapter.addFragment(CardMarkFragment.getInstance(items.get(i),beacon, "interaction" , i, items.get(i).getPunch_count()));
        }
        adapter.notifyDataSetChanged();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

            try{
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Validar si hay recompenzas en cache
                        if(UrbanDB.getInstance(getActivity()).getUrbanCards(beacon.getId()).size()>0){
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    for(ItemRedeemResponse item: UrbanDB.getInstance(getActivity()).getUrbanCards(beacon.getId())){
                                        adapter.addFragment(FragmentPunchCardExchange.getInstance(item));
                                    }
                                    if(adapter!=null){
                                        adapter.notifyDataSetChanged();
                                    }

                                }
                            });

                        }

                        FragmentPunchCardExchange.setListener(new FragmentPunchCardExchange.OnDeleteCardListener() {
                            @Override
                            public void DeleteCard(final Fragment fragment) {
                                try{
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            ClearAdapter();
                                        }
                                    });
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }

            }
        };
        Timer timer = new Timer();
        timer.schedule(task, 200);


        if(viewPager!=null){
            viewPager.setAdapter(adapter);
            indicator.setViewPager(viewPager);
            viewPager.setOffscreenPageLimit(adapter.getCount());
            adapter.notifyDataSetChanged();

            viewPager.setCurrentItem(POSITIONCARD);




            if(CARDANIMATIONEXCHANEGE){
                TimerTask task1 = new TimerTask() {
                    @Override
                    public void run() {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                viewPager.setCurrentItem(viewPager.getChildCount());
                                CARDANIMATIONEXCHANEGE =false;
                            }
                        });

                    }
                };
                Timer timer1 = new Timer();
                timer1.schedule(task1, 1000);
            }

        }

    }



    public void ClearAdapterCard(){
        List<Fragment> fragments=null;
        try{
            fragments = getChildFragmentManager().getFragments();

        }catch (Exception e){
            e.printStackTrace();
        }


        if (fragments != null) {
            FragmentTransaction ft = getChildFragmentManager().beginTransaction();
            for (Fragment f : fragments) {
                //You can perform additional check to remove some (not all) fragments:
                if (f instanceof CardMarkFragment) {
                    ft.remove(f);
                }
            }
            ft.commitAllowingStateLoss();
        }
    }



    @Override
    public void onError(String error) {
        UrbanUtils.DialogSimpleOK(getActivity(),getActivity().getResources().getString(R.string.app_name),error);
    }

    @Override
    public void onErrorNetwork() {
        UrbanUtils.DialogSimpleOK(getActivity(),getActivity().getResources().getString(R.string.app_name),"No cuenta con con conexión a internet");
    }

    @Override
    public void onSessionExpired() {
        SimpleCustomDialog dialog=new SimpleCustomDialog(getActivity(), getResources().getString(R.string.error_session), getResources().getString(R.string.error_session_message), getResources().getString(R.string.buttonAccept), TypeDialog.OK, new SimpleCustomDialog.OnButtonClickListener() {
            @Override
            public void onAcceptClickListener(Button button, AlertDialog dialog) {
                UrbanApplication.getInstance().clearApplicationData();
                UrbanDB.getInstance(getActivity()).CloseSession();
                UrbanPrefs.getInstance(getActivity()).ClearPrefs();
                UrbanPrefs.getInstance(getActivity()).setIsLoged(false);
                UrbanPrefs.getInstance(getActivity()).setRunAppFirst(false);
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();

                dialog.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public void ClearAdapter() {
       getActivity().runOnUiThread(new Runnable() {
           @Override
           public void run() {


               try{
                   TimerTask task = new TimerTask() {
                       @Override
                       public void run() {
                           if(getActivity()!=null){
                               getActivity().runOnUiThread(new Runnable() {
                                   @Override
                                   public void run() {

                                       replaceFragment(FragmentSuccessResponse.getInstance(beacon),R.id.content_interaction);

                                   }
                               });
                           }

                       }
                   };
                   Timer timer = new Timer();
                   timer.schedule(task, 200);
               }catch (Exception e){
                   e.printStackTrace();
               }


           }
       });
    }
}
