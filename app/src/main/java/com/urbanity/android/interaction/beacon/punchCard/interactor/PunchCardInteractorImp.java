package com.urbanity.android.interaction.beacon.punchCard.interactor;

import android.content.Context;
import android.widget.Toast;

import com.urbanity.android.interaction.beacon.punchCard.PunchCardFinishListener;
import com.urbanity.android.interaction.beacon.search.OnBeaconFinishListener;
import com.urbanity.android.model.PunchCard;
import com.urbanity.android.model.UrbanResponseError;
import com.urbanity.android.network.ErrorUtils;
import com.urbanity.android.network.RetrofitService;
import com.urbanity.android.storage.UrbanDB;
import com.urbanity.android.utils.UrbanConstants;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DRM on 13/07/17.
 */

public class PunchCardInteractorImp implements PunchCardInteractor {
    @Override
    public void OnSearchPunchCardMerchant(Context context,String merchant_id, final PunchCardFinishListener listener) {
        if(UrbanDB.getInstance(context).getUrbanUser()!=null){
            Call<ArrayList<PunchCard>> getPunchCards= RetrofitService.getUrbanService().getPunchCards(UrbanDB.getInstance(context).getUrbanUser().getUrbanToken(),merchant_id);
            getPunchCards.enqueue(new Callback<ArrayList<PunchCard>>() {
                @Override
                public void onResponse(Call<ArrayList<PunchCard>> call, Response<ArrayList<PunchCard>> response) {
                    if(response.code()== UrbanConstants.ERRORLOGOUT){
                        listener.onSessionExpired();
                        return;
                    }
                    if(response.body()!=null){
                        if(response.body().size()>0){
                            listener.onSuccess(response.body());
                        }else {
                            listener.onEmpty();
                        }
                    }else {
                        UrbanResponseError error= ErrorUtils.parseError(response);
                        //No vienen  datos
                        listener.onError(error.getMessage());
                    }
                }
                @Override
                public void onFailure(Call<ArrayList<PunchCard>> call, Throwable t) {
                    listener.onError("Ocurrio un error al intentar recuperar los datos");
                }
            });
        }


    }
}
