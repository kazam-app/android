package com.urbanity.android.interaction.beacon.punchCard;

import com.urbanity.android.model.PunchCard;

import java.util.ArrayList;

/**
 * Created by DRM on 13/07/17.
 */

public interface PunchCardView {

    void ShowProgress();
    void HideProgress();

    void onEmptyPunchCard();
    void onSuccess(ArrayList<PunchCard>items);
    void onError(String error);
    void onErrorNetwork();
    void onSessionExpired();



}
