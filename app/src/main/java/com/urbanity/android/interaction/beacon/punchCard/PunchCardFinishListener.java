package com.urbanity.android.interaction.beacon.punchCard;

import com.urbanity.android.model.PunchCard;

import java.util.ArrayList;

/**
 * Created by DRM on 13/07/17.
 */

public interface PunchCardFinishListener {
    void onError(String error);
    void onSuccess(ArrayList<PunchCard> items);
    void onEmpty();
    void onSessionExpired();
}
